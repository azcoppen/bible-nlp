@extends ('layouts.themes.mouldify.master')

@section ('sharing')
  @include ('layouts.themes.mouldify.partials.sharing', [
    'title' => 'Cross References : ' . $book->title.' '.$chapter->number.', '.($verses->count() > 1 ? 'Verses ' . $verses->min('number') .'-'. $verses->max('number') : 'Verse ' . $verses->first()->number).' | RocketBible.com',
    'info'  => 'Cross References: '.$verses->pluck('text.en')->implode (' '),
  ])
@stop

@section ('content')

  <div class="page-heading">
      <h1 class="title"><span class="font-weight-light">Cross References
          / </span> {{$book->title}} {{$chapter->number}}
          <span class="font-weight-light text-secondary"> {{ $verses->count() > 1 ? $verses->min('number') .'-'. $verses->max('number') : $verses->first()->number }} </span>
      </h1>
      <h2 class="ml-3">
        <i class="zmdi zmdi-long-arrow-right zmdi-hc-fw"></i>
        {!! fix_utf8_apos($chapter->summaries->first()->text['en']) !!}
      </h2>
  </div>

  <div class="row">
    <div class="col-lg-12 col-sm-12">

      <blockquote class="blockquote lead text-muted mb-3">
        @foreach ($verses AS $verse)
            <a rel="{{$verse->number}}" class="para-verse" title="" href="{{route ('verses.show', [$book->slug, $chapter->number, $verse->number])}}">
              {!! highlight ( quotations ($verse->text['en']), $chapter->unique_entities->pluck('name')->all()) !!}
            </a>
        @endforeach
      </blockquote>

      <hr />

      <article class="gx-card mt-3">
        @if ( isset($cross_refs) && count ($cross_refs) )

        @foreach ($cross_refs AS $book_num => $book_group)
          <h3 class="font-weight-semibold clearfix">
            {{$book_group->first()->first()->book_title}}
          </h3>

          <p class="text-muted">{{$BOOKS->where('order', $book_num)->first()->summary}}</p>


          <ul class="list-unstyled ml-1 clearfix">

          @foreach ( $book_group AS $chapter_group )
            <li class="mt-3" style="margin-bottom: 30px;">
            <h3>
              <i class="zmdi zmdi-long-arrow-right zmdi-hc-fw"></i>
              <a class="mr-2 mb-0 badge badge-warning" href="{{route('chapters.show', [$chapter_group->first()->book_slug, $chapter_group->first()->chap_num])}}">{{$chapter_group->first()->chap_num}}</a>
              <small class="font-weight-light text-muted">{!! $summaries->has($book_num) ? fix_utf8_apos($summaries->get($book_num)->where('chap_num', $chapter_group->first()->chap_num)->first()->text['en']) : '' !!}</small>
            </h3>
              <table class="table ml-3 mt-1">
              @foreach ($chapter_group->sortBy('start_num') AS $ref)
                <tr class="ml-3 mb-3">
                  <td class="center">
                    <a class="ref-verse" href="{{route ('verses.show', [$ref->book_slug, $ref->chap_num, $ref->human_range])}}">
                      <small class="mr-2 badge badge-light">{{$ref->human_range}}</small>
                    </a>
                  </td>
                  <td>
                    <a class="ref-verse" href="{{route ('verses.show', [$ref->book_slug, $ref->chap_num, $ref->human_range])}}">
                       {{$ref->ref_full_text}}
                    </a>
                  </td>
                </tr>
              @endforeach
            </table>
            </li>
          @endforeach
          </ul>
          <hr style="margin-top: 30px; margin-bottom: 30px;" />
        @endforeach
      @else
        @include ('layouts.themes.mouldify.partials.empty', ['text' => 'No cross references found'])
      @endif
    </article>
  </div>
</div>
@stop
