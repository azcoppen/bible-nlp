@extends ('layouts.themes.mouldify.master')

@section ('sharing')
  @include ('layouts.themes.mouldify.partials.sharing', [
    'title' => $book->title.' '.$chapter->number.', '.($verses->count() > 1 ? 'V: ' . $verses->min('number') .'-'. $verses->max('number') : 'Verse ' . $verses->first()->number).' | RocketBible.com',
    'info'  => isset($chapter_view) && $chapter_view == TRUE ? fix_utf8_apos($chapter->summaries->first()->text['en'] ?? '') : str_limit ($verses->pluck('text.en')->implode (' '), 140),
  ])
<meta property="og:type" content="books.genre">
<meta property="books:canonical_name" content="{{$book->title}}">
<meta property="books:book" content="{{htmlentities($book->summary)}}">
@stop

@section ('content')

  @if ( isset($chapter_view) && $chapter_view !== FALSE )
    @include ('layouts.themes.mouldify.partials.chapter_heading', ['book' => $book, 'chapter'=>$chapter])
  @else
    @include ('layouts.themes.mouldify.partials.verse_heading', ['book' => $book, 'chapter'=>$chapter, 'verses'=>$verses])
  @endif


  <section class="row mt-3">
    <div class="col-lg-12">
      @include ('layouts.themes.mouldify.partials.verse_quotation', ['book' => $book, 'chapter'=>$chapter, 'verses'=>$verses, 'large'=>true])
    </div>
  </section>

  <section class="row">
    <div class="col-lg-7 col-sm-12">
      <div class="gx-card">
        @include ('layouts.themes.mouldify.partials.grouped_entity_panel', ['entity_groups'=>$entity_groups])
      </div>
    </div>
    <div class="col-lg-5 col-sm-12">
      <div class="gx-card">
        <h3 class="text-center mb-2">Entity Breakdown</h3>
        <canvas id="entity-radar" class="multi-entity-polar mt-3 m-auto" data-labels='{!! json_encode ($entity_groups->keys()->all())!!}' data-colors='{!! json_encode(array_values($entity_hex)) !!}' data-values='{!! json_encode ($entity_groups->map(function ($item, $key) {
    return $item->count();
})->values()->all())!!}'></canvas>

<canvas style="margin-top: 2rem !important;" id="entity-bar" height="140" class="multi-entity-bar mt-3 m-auto" data-labels='{!! json_encode ($entity_groups->keys()->all())!!}' data-values='{!! json_encode ($entity_groups->map(function ($item, $key) {
return $item->count();
})->values()->all())!!}'></canvas>
        @include ('layouts.themes.mouldify.partials.entity_disclaimer')
      </div>
    </div>
  </section>

@if ( isset($chapter_view) && $chapter_view !== TRUE )
  <section class="row">
    <div class="col-lg-4 col-sm-12">
      @include ('layouts.themes.mouldify.partials.grouped_cross_ref_list', ['book' => $book, 'chapter'=>$chapter, 'verses'=>$verses, 'verse_range'=>$verse_range])
    </div>
    <div class="col-xl-4 col-sm-12">
      @include ('layouts.themes.mouldify.partials.mlt_summary', ['book' => $book, 'chapter'=>$chapter, 'verses'=>$verses, 'verse_range'=>$verse_range, 'similar'=>$similar])
    </div>
    <div class="col-lg-4 col-sm-12">
      @include ('layouts.themes.mouldify.partials.grouped_topic_list', ['topics'=>$verses->pluck('topic_refs')->flatten()->sortBy('title')->groupBy ('title')])
    </div>
  </section>

@endif

<section class="row">

  <div class="col-xl-12 col-sm-12">
  @include ('layouts.themes.mouldify.partials.frequency_graph', ['container'=>$chapter, 'chapter_view' => isset($chapter_view) && $chapter_view !== FALSE ? true : false])
  </div>
</section>



@stop

@push ('js')
  <script>
  window.verses_url = '{{route ('verses.index', [$book->slug, $chapter->number])}}/';
  window.verse_slider = document.getElementById('verse-slider');
  noUiSlider.create(window.verse_slider, {
      start: [{{$verses->min('number')}}, {{$verses->max('number') == $verses->min('number') ? $verses->min('number') + 1 : $verses->max('number')}}],
      behaviour: 'drag',
      connect: true,
      step: 1,
      range: {
          'min': 1,
          'max': {{$chapter->verses->count()}}
      },
      pips: {
          mode: 'values',
          values: {{json_encode (range (1, $chapter->verses->count()))}},
          density: 1
      }
  });

  $( "#slice-btn" ).click(function() {
    var positions = window.verse_slider.noUiSlider.get();
    window.location = window.verses_url + parseInt (positions[0]) + '-' +  parseInt (positions[1]);
  });

  </script>
@endpush
