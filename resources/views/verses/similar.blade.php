@extends ('layouts.themes.mouldify.master')

@section ('sharing')
  @include ('layouts.themes.mouldify.partials.sharing', [
    'title' => 'Verses Similar To : ' . $book->title.' '.$chapter->number.', '.($verses->count() > 1 ? 'Verses ' . $verses->min('number') .'-'. $verses->max('number') : 'Verse ' . $verses->first()->number).' | RocketBible.com',
    'info'  => 'Verses Similar To: '.$verses->pluck('text.en')->implode (' '),
  ])
@stop

@section ('content')

  <div class="page-heading">
      <h1 class="title"><span class="font-weight-light">Similar
          / </span> {{$book->title}} {{$chapter->number}}
          <span class="font-weight-light text-secondary"> {{ $verses->count() > 1 ? $verses->min('number') .'-'. $verses->max('number') : $verses->first()->number }} </span>
      </h1>
      <h2 class="ml-3">
        <i class="zmdi zmdi-long-arrow-right zmdi-hc-fw"></i>
        {!! fix_utf8_apos($chapter->summaries->first()->text['en']) !!}
      </h2>
  </div>

  <div class="row">
    <div class="col-lg-12 col-sm-12">


      <article class="gx-card mt-3">
        @if (isset($verses) && count($verses) )
        @foreach ($verses AS $verse)
          @if ( is_object($verse->mlt) && isset($verse->mlt->similarity) && is_array($verse->mlt->similarity) )

            <blockquote class="blockquote lead text-muted mb-3">
                  <a rel="{{$verse->number}}" class="para-verse" title="" href="{{route ('verses.show', [$book->slug, $chapter->number, $verse->number])}}">
                    {!! $verse->text['en'] !!}
                  </a>
            </blockquote>

            <div class="table-responsive mt-0">
              <table class="table table-sm mt-0 mb-3">
                <thead>
                  <tr>
                    <th>Book</th>
                    <th>Chapter</th>
                    <th>Verse</th>
                    <th>Text (Contextless)</th>
                    <th>+/-</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($verse->mlt->similarity AS $v_id => $score)
                    <?php $sim = $similar->where('_id', $v_id)->first(); ?>
                    @if ( $sim )
                      <tr>
                        <td nowrap>
                          <a href="{{route ('books.show', $sim->book_slug)}}">
                          {{$sim->book_title}}
                          </a>
                        </td>
                        <td nowrap class="text-right"><a class="ml-0 mb-0 badge badge-warning" href="{{route('chapters.show', [$sim->book_slug, $sim->chap_num])}}">{{$sim->chap_num}}</a></td>
                        <td nowrap class="text-center">{{$sim->number}}</td>
                        <td class="font-weight-light">
                          <a rel="{{$sim->number}}" class="ref-verse" href="{{route ('verses.show', [$sim->book_slug, $sim->chap_num, $sim->number])}}">
                            {!! $sim->text['en'] !!}
                          </a>
                        </td>
                        <td nowrap class="text-center text-success font-weight-light"><small>{{round($score, 2)}}</small></td>
                      </tr>
                    @endif
                  @endforeach
                </tbody>
                </table>
              </div>
            @else
              @include ('layouts.themes.mouldify.partials.empty', ['text' => 'No similar verses found'])
            @endif
          <hr style="margin-top: 30px; margin-bottom: 30px; "/>
        @endforeach
      @else
        @include ('layouts.themes.mouldify.partials.empty', ['text' => 'No similar verses found'])
      @endif

      </article>
  </div>
</div>
@stop
