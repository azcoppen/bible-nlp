@extends ('layouts.themes.mouldify.master')

@section ('sharing')
  @include ('layouts.themes.mouldify.partials.sharing', [
    'title' => '&quot;'.title_case ($frequency->word).'&quot; - Biblical Word Frequencies | RocketBible.com',
    'info'  => '',
  ])
@stop

@section ('content')

  <div class="page-heading">
    <div class="row d-block d-md-none mb-2">
      <div class="col-sm-12 col-xs-12 text-right float-right">
        <span class="mr-4 badge badge-secondary">{{$frequency->total}}</span>
      </div>
    </div>
    <div class="row clearfix" style="clear:both;">
      <h1 class="title col-xs-12 col-sm-12 pl-3 pr-2 text-center">
        Biblical Word Frequencies
        <span class="font-weight-light"> / <span class="text-secondary">{{$frequency->word}}</span> </span>
          <span class="mr-2 badge badge-secondary float-right d-none d-sm-block">{{$frequency->total}}</span>
      </h1>
    </div>
  </div>

  <div class="row mb-3">
    <div style="max-height: 250px;" id="book-group-100" class="ct-perfect-fourth bible-graph book-group-graph" data-labels='{!! json_encode($BOOKS->sortBy('order')->pluck('abbrev')->all()) !!}' data-values="{!! json_encode ([chapter_values_map($BOOKS->count(), $bible->all())]) !!}"></div>
  </div>


  <div class="row">
    <div class="col-lg-12">
      <div class="gx-card">
        @if ( isset($chapters) && count ($chapters) )
          @foreach ($chapters->groupBy('book_num') AS $book_num => $book_group)
            <h3 class="font-weight-semibold clearfix mt-3">
              <a href="{{route ('frequencies.book', [$az, $book_group->first()->word, $book_group->first()->book_slug])}}">
                {{$book_group->first()->book_title}}
              </a>

              <small class="mr-2 badge badge-secondary ml-2">{{$book_group->sum('total')}}</small>
            </h3>

            <p class="text-muted">{{$BOOKS->where('order', $book_num)->first()->summary}}</p>

            <div class="row">
              <div style="max-height: 150px;" id="book-group-{{$book_num}}" class="ct-perfect-fourth book-group-graph" data-labels="{{json_encode(range(1, $BOOKS->where('order', $book_num)->first()->chaps))}}" data-values="{!! json_encode ([chapter_values_map($BOOKS->where('order', $book_num)->first()->chaps, $book_group->sortBy ('chap_num')->pluck('total', 'chap_num')->all())]) !!}"></div>
            </div>

            <div class="row mt-0 mb-3">

              <div class="col-md-4 pl-2">
                @if (count ($book_group) )
                  <table class="table table-sm col-md-6 col-md-offset-3 ml-3">
                    <thead>
                      <tr>
                        <th class="text-center">Chapter</th>
                        <th class="text-center">Count </th>
                      </tr>
                    </thead>
                    <tbody>
                    @foreach ($book_group->sortBy ('chap_num') AS $chap_freq)
                    <tr>
                      <td width="30%" class="text-center">
                        <a class="ml-2 mb-0 badge badge-warning" href="{{route ('frequencies.chapter', [$az, $chap_freq->word, $chap_freq->book_slug, $chap_freq->chap_num])}}">{{$chap_freq->chap_num}}</a>
                      </td>
                      <td width="30%" class="text-center">
                        <span class="badge badge-light">{{$chap_freq->total}}</span>
                      </td>
                    </tr>
                    @endforeach
                    </tbody>
                  </table>
                @else
                  @include ('layouts.themes.mouldify.partials.empty', ['text' => 'No results found.'])
                @endif
              </div>

              <div class="col-md-4 text-center pt-3 pl-3">
                <div id="chapter-gauge-{{$book_num}}" style="margin-left: 5rem;" class="mt-3 float-right chapter-gauge-graph m-auto" data-total="{{$book_group->sum('total')}}" data-values="{!! json_encode ($book_group->sortBy ('chap_num')->pluck('total', 'chap_num')->values()->all()) !!}"></div>
              </div>
              <div class="col-md-4 text-center pt-3">
                <canvas id="chapter-radar-{{$book_num}}" class="chapter-radar-graph mt-3 m-auto" data-labels="{{json_encode(range(1, $BOOKS->where('order', $book_num)->first()->chaps))}}" data-values="{!! json_encode (chapter_values_map($BOOKS->where('order', $book_num)->first()->chaps, $book_group->sortBy ('chap_num')->pluck('total', 'chap_num')->all())) !!}"></canvas>
              </div>

            </div>
            <hr style="margin-top: 30px; margin-bottom: 30px;" />
          @endforeach

          <div class="row mt-0 mb-3 text-center">
            <div class="col-md-3 col-md-offset-1 m-auto text-center">
              {!! $chapters->links() !!}
            </div>
          </div>
          @include ('layouts.themes.mouldify.partials.frequency_disclaimer')

        @else
          @include ('layouts.themes.mouldify.partials.empty', ['text' => 'No results found in any book.'])
        @endif

    </div>
  </div>
</div>


@stop
