@extends ('layouts.themes.mouldify.master')

@section ('sharing')
  @include ('layouts.themes.mouldify.partials.sharing', [
    'title' => 'Biblical Word Frequencies - '.(request()->has('q') ? '&quot;'.request()->get('q').'&quot; Search -' : '') . (isset($az) ? '('.$az.') - ' : 'Top 100').'  | RocketBible.com',
    'info'  => '',
  ])
@stop

@push ('head')
  <style>
  .badge-ext {
    width: 35px !important;
    padding: 5px 5px !important;
  }
  </style>
@endpush

@section ('content')
  <div class="page-heading">
      <h1 class="title">
        Biblical Word Frequencies
        @if ( isset($az) && !empty ($az) )
          <span class="font-weight-light"> / {{$az}} </span>
          @if ( request()->has('q') )
            <span class="font-weight-light"> / <span class="text-secondary"> {{request()->get('q')}} </span></span>
          @endif
        @else
          @if ( request()->has('q') )
            <span class="font-weight-light"> / <span class="text-secondary"> {{request()->get('q')}} </span></span>
          @else
            <span class="font-weight-light"> / <span class="text-secondary"> Top 100 </span></span>
          @endif

        @endif
      </h1>
  </div>

  <div class="row">
    <div class="col-lg-12 text-center m-b-10">
      @include ('layouts.themes.mouldify.partials.az_btns', [
        'route'     => 'frequencies.az',
        'selected'  => $az ?? '',
        'reset'     => 'frequencies.index',
      ])
    </div>
    <div class="col-lg-12 text-center mt-1">
      @include ('layouts.themes.mouldify.partials.inline_search', [
          'text'      => isset ($az) && !empty ($az) ? "Search all word occurrences beginning with " . $az : "Search all words in the Bible"
      ])
    </div>
  </div>


  <div class="row">
    <div class="col-lg-12">
      <div class="gx-card">
        <div class="" role="group">
          @if ( isset($frequencies) && count ($frequencies) )
            @foreach ($frequencies AS $freq )
                <a class="gx-btn gx-btn-default col-md-2" href="{{route ('frequencies.show', [$freq->az, $freq->word])}}">
                  {{title_case($freq->word)}}
                  <small class="mb-0 badge badge-light badge-ext float-right">{{$freq->total}}</small>
                </a>
            @endforeach
          @else
            <div class="col-md-12">
              @include ('layouts.themes.mouldify.partials.empty', ['text' => 'No word occurrences found'])
            </div>
          @endif

        </div>
        @include ('layouts.themes.mouldify.partials.frequency_disclaimer')
    </div>
  </div>
</div>

@if ( isset($frequencies) && count ($frequencies) && !request()->has('q') )
<div class="row text-center">
  <div class="col-md-6 col-md-offset-3 m-auto">
    {!! $frequencies->links () !!}
  </div>
</div>
@endif

@stop
