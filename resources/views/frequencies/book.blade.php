@extends ('layouts.themes.mouldify.master')

@section ('sharing')
  @include ('layouts.themes.mouldify.partials.sharing', [
    'title' => 'Occurrences of &quot;'.title_case ($frequency->word).'&quot; found in ' .$book->title. '('.$mentions->sum('total').') - Biblical Word Frequencies | RocketBible.com',
    'info'  => '',
  ])
<meta property="og:type" content="books.genre">
<meta property="books:canonical_name" content="{{$book->title}}">
<meta property="books:book" content="{{htmlentities($book->summary)}}">
@stop

@section ('content')
  <div class="page-heading">
      <h1 class="title">
        Biblical Word Frequencies
          <span class="font-weight-light"> / <span class="text-secondary">{{$frequency->word}}</span> / Found In </span>
          {{$book->title}}
          <span class="mr-2 badge badge-secondary float-right">{{$mentions->sum('total')}}</span>
      </h1>
  </div>

  <div class="row">
    <div class="col-lg-12">
      <div class="gx-card">
        <p class="text-muted lead">{{$book->summary}}</p>

        <div style="max-height: 150px;" id="book-group-{{$book->order}}" class="ct-perfect-fourth book-group-graph" data-labels="{{json_encode(range(1, $book->chaps))}}" data-values="{!! json_encode ([chapter_values_map($book->chaps, $chap_count->all())]) !!}"></div>

        <hr style="margin-top: 30px; margin-bottom: 30px;" />

        <ul class="list-unstyled ml-1 clearfix">
        @foreach ( $chapters AS $chapter )
          @if ($mentions->whereIn ('container_id', $chapter->verses->pluck('id'))->count() > 0)
          <li class="mb-2 mt-2">
          <h3 class="">
            <i class="zmdi zmdi-long-arrow-right zmdi-hc-fw"></i>
            <a class="mb-0 mr-2 badge badge-warning" href="{{route('frequencies.chapter', [$az, $frequency->word, $book->slug, $chapter->number])}}">{{$chapter->number}}</a>
            <small class="font-weight-light text-muted">{!! fix_utf8_apos($chapter->summaries->first()->text['en'] ?? '') !!}</small>
          </h3>


            <div class="row mt-0 mb-3">
              <div class="col-md-7 pl-2">
                @if (count ($mentions) )
                  <table class="table table-sm col-md-6 col-md-offset-3 ml-3">
                    <thead>
                      <tr>
                        <th class="text-center">Verse</th>
                        <th class="text-center">Count </th>
                        <th class="text-center">Context </th>
                      </tr>
                    </thead>
                    <tbody>
                    @foreach ($chapter->verses->sortBy('number') AS $verse)
                      @if ( $mentions->contains ('container_id', $verse->id) )
                      <tr>
                        <td class="text-center">
                        {{$verse->number}}
                        </td>
                        <td class="text-center">
                          <span class="badge badge-light">{{$mentions->where('container_id', $verse->id)->first()->total}}</span>
                        </td>
                        <td>
                          <a class="ref-verse" href="{{route ('verses.show', [$verse->book_slug, $verse->chap_num, $verse->number])}}">
                             {!! searchword( $verse->text['en'], [$frequency->word], 'verse-100') !!}
                          </a>
                        </td>
                      </tr>
                      @endif
                    @endforeach
                    </tbody>
                  </table>

                @else
                  @include ('layouts.themes.mouldify.partials.empty', ['text' => 'No results found.'])
                @endif
              </div>
              <div class="col-md-5 pt-3">
                <canvas id="chapter-radar-{{$chapter->number}}" class="chapter-radar-graph mt-3 m-auto" data-labels="{{json_encode(range(1, $chapter->verses->count()))}}"
                  data-values="{!! json_encode (chapter_values_map($chapter->verses->count(),  $chapter->verses->filter(function ($value, $key) use ($mentions) {
                      return $mentions->contains ('container_id', $value->id);
                  })->mapWithKeys(function ($item) use ($mentions) {
                    return [$item->number => $mentions->where('container_id', $item->id)->first()->total];
                  })->sortKeys()->all())) !!}">
                </canvas>
              </div>
            </div>
          </li>
          <hr style="margin-top: 30px; margin-bottom: 30px;" />
          @endif
        @endforeach
      </ul>
      @include ('layouts.themes.mouldify.partials.frequency_disclaimer')
    </div>
  </div>
</div>


@stop
