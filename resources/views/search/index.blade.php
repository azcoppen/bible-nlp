@extends ('layouts.themes.mouldify.master')

@section ('sharing')
  @include ('layouts.themes.mouldify.partials.sharing', [
    'title' =>' Search of everything for &quot;'.request()->input('q', 'Jesus').'&quot; | RocketBible.com',
    'info'  => 'Results for books, chapters, verses, entities, word occurrences, similar text and all related data for &quot;'.request()->input('q', 'Jesus').'&quot;',
  ])
@stop


@section ('head')
  <style>
  .badge:not(:last-child) {
    margin-right: 0px;
  }
  </style>
@stop

@section ('content')

  <div class="page-heading">
      <h1 class="title font-weight-light">Search Everything /
          <span class="text-success"> {{request()->get('q', 'Jesus')}} </span>
      </h1>
  </div>

<div class="row mt-1">
  <div class="col-md-12">

          <article class="gx-card">
            @if ( isset($summaries) && count($summaries) > 0 )
            <h3 class="card-heading">
              <small class="mb-0 badge badge-light badge-ext float-right">{{count($summaries)}}</small>
                Chapter Outline Results / <span class="font-weight-light">By Relevance</span>
            </h3>

            <div class="row mb-3">
              <div style="max-height: 150px;" id="chapter-group-100" class="ct-perfect-fourth bible-graph search-group-graph" data-labels='{!! json_encode($BOOKS->sortBy('order')->pluck('abbrev')->all()) !!}' data-values="{!! json_encode ([chapter_values_map($BOOKS->count(), $graph_chapters->all())]) !!}"></div>
            </div>

            <div class="table-responsive">
              <table class="table table-sm">
                <thead>
                  <tr>
                    <th>Book</th>
                    <th>Chapter</th>
                    <th>Summary</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ( $summaries AS $summary )
                    <tr class="mb-2">
                      <td nowrap>
                        <a href="{{route ('books.show', $summary->book->slug)}}">
                        {{$summary->book->title}}
                        </a>
                      </td>
                      <td class="text-center" nowrap><a class="ml-0 mb-0 badge badge-warning" href="{{route('chapters.show', [$summary->book->slug, $summary->chap_num])}}">{{$summary->chap_num}}</a></td>
                      <td class="font-weight-light">
                          {!! searchword( fix_utf8_apos($summary->text['en']), str_word_count(request()->input ('q'), 1), 'verse-100') !!}
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>

            <hr class="mt-3" style="margin-bottom: 3rem;" />

            @endif


            @if ( isset($verses) && count($verses) > 0 )
            <h3 class="card-heading mb-3">
              <small class="mb-0 badge badge-light badge-ext float-right">{{count($verses)}}</small>
                Verse Text Results / <span class="font-weight-light">By Relevance</span>
            </h3>

            <div class="row mt-3 mb-3">
              <div style="max-height: 150px;" id="verse-group-100" class="ct-perfect-fourth bible-graph search-group-graph" data-labels='{!! json_encode($BOOKS->sortBy('order')->pluck('abbrev')->all()) !!}' data-values="{!! json_encode ([chapter_values_map($BOOKS->count(), $graph_verses->all())]) !!}"></div>
            </div>

              <div class="table-responsive">
                <table class="table table-sm">
                  <thead>
                    <tr>
                      <th>Book</th>
                      <th>Chapter</th>
                      <th>Verse</th>
                      <th>Text (Contextless)</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ( $verses AS $verse )
                      <tr class="mb-2">
                        <td nowrap class="">
                          <a href="{{route ('books.show', $verse->book_slug)}}">
                          {{$verse->book_title}}
                          </a>
                        </td>
                        <td nowrap class="text-center"><a class="ml-0 mb-0 badge badge-warning" href="{{route('chapters.show', [$verse->book_slug, $verse->chap_num])}}">{{$verse->chap_num}}</a></td>
                        <td nowrap class="text-center">{{$verse->number}}</td>
                        <td>
                          <a rel="{{$verse->number}}" class="ref-verse font-weight-semibold" data-toggle="tooltip" data-placement="right" title="{{$verse->extended}}" href="{{route ('verses.show', [$verse->book_slug, $verse->chap_num, $verse->number])}}">
                            {!! searchword( $verse->text['en'], str_word_count(request()->input ('q'), 1), 'verse-100') !!}
                          </a>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>

            @else
              @include ('layouts.themes.mouldify.partials.empty', ['text' => 'No results found'])
            @endif
          </article>
  </div>
</div>

  <div class="row mt-3">
    <div class="col-md-4">
      <article class="gx-card">
        <h2 class="card-heading" style="margin-bottom: 3rem;">
          @if ( isset($entity_groups) && count ($entity_groups->flatten()))
          <small class="mb-0 badge badge-light badge-ext float-right">{{$entity_groups->flatten()->count()}}</small>
          @endif
            Entity Results
        </h2>
        @if ( isset($entity_groups) && count ($entity_groups->flatten()))
          @include ('layouts.themes.mouldify.partials.grouped_entity_panel', ['entity_groups'=>$entity_groups])
          @include ('layouts.themes.mouldify.partials.entity_disclaimer')
        @else
          @include ('layouts.themes.mouldify.partials.empty', ['text' => 'No entities found'])
        @endif
      </article>
    </div>

    <div class="col-md-4 gx-card">
        <h2 class="card-heading" style="margin-bottom: 3rem;">
          @if ( isset($topics) && count ($topics))
          <small class="mb-0 badge badge-light badge-ext float-right">{{count($topics)}}</small>
          @endif
            Topic Results
        </h2>
        @if ( isset($topics) && count ($topics))
          <div class="table-responsive">
            <table class="table table-sm">
              <tbody>
              @foreach ($topics->sortBy('root_slug') AS $topic )
                <tr>
                  <td>
                    @if ($topic->root == 1)
                        <a class="" href="{{route ('topics.show', [$topic->az, str_slug($topic->root_slug)])}}">
                        {{fix_utf8($topic->root_title)}}
                        </a>
                    @else
                      <a href="{{route('topics.subject', [$topic->az, str_slug($topic->root_slug), $topic->subject_slug])}}">
                        {{fix_utf8($topic->root_title)}}, {{fix_utf8($topic->subject_title)}}
                      </a>
                    @endif
                  </td>
                </tr>
              @endforeach
              </tbody>
            </table>
          </div>
        @else
          @include ('layouts.themes.mouldify.partials.empty', ['text' => 'No topics found'])
        @endif

    </div>

    <div class="col-md-4 gx-card">
        <h2 class="card-heading" style="margin-bottom: 3rem;">
          @if ( isset($frequencies) && count ($frequencies))
          <small class="mb-0 badge badge-light badge-ext float-right">{{count($frequencies)}}</small>
          @endif
            Word Results
        </h2>
        @if ( isset($frequencies) && count ($frequencies))
          <div class="" role="group">
              @foreach ($frequencies AS $freq )
                  <a class="gx-btn gx-btn-default" href="{{route ('frequencies.show', [$freq->az, $freq->word])}}">
                    {{title_case($freq->word)}}
                  </a>
              @endforeach
          </div>
        @else
          @include ('layouts.themes.mouldify.partials.empty', ['text' => 'No word instances found'])
        @endif
    </div>
  </div>



@stop
