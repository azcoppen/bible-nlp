<!doctype html>
<html lang="{{App::getLocale()}}">
<head>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-129675889-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-129675889-1');
</script>
@yield ('title')
@yield ('sharing')
@include ('layouts.themes.mouldify.partials.head')
@stack ('head')
</head>

<body id="body" {!! isset($nav_menu) && $nav_menu == 'home' ? 'style="background-color: #FFF;"' : '' !!} data-theme="{{$theme ?? config('app.theme')}}" class="{{isset($theme) && $theme=='dark' ? 'dark-theme' : ''}}">
<main class="gx-container">

    @include ('layouts.themes.mouldify.partials.sidebar_nav')

    <container class="gx-main-container">

        @include ('layouts.themes.mouldify.partials.top_bar')

        <div class="gx-main-content">
            <section class="gx-wrapper">
              @yield ('content')
            </section>
            @include ('layouts.themes.mouldify.partials.footer_bar')
        </div>

    </container>

</main>

@include ('layouts.themes.mouldify.partials.theme_sidebar')

<div class="menu-backdrop fade"></div>

@include ('layouts.themes.mouldify.partials.footer_js')
@stack ('js')

</body>
</html>
