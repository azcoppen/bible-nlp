@if ( isset($entities) && count ($entities) )

    @foreach ($entities->sortBy('name') AS $entity)

      <a title="" href="{{route ('entities.show', [strtolower($entity->type), $entity->az, $entity->slug])}}" class="gx-btn gx-btn-sm gx-btn-{{config ('app.entity_colors.'.$type)}} m-1">
          <!--<i class="zmdi zmdi-{{$icon ?? 'collection-item'}} zmdi-hc-lg zmdi-hc-fw mr-3 text-primary"></i>-->
          {{title_case($entity->name)}}
      </a>

    @endforeach

@endif
