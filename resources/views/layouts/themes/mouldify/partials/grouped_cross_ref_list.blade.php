

      <article class="gx-card">
        <div class="gx-card-header">

            <h3 class="card-heading">
              <a class="gx-btn gx-btn-sm gx-btn-default float-right" href="{{route ('verses.crossrefs', [$book->slug, $chapter->number, $verse_range])}}">All
                <span class="ml-2 mb-0 badge badge-secondary float-right">{{$verses->pluck ('cross_refs')->flatten()->count()}}</span></a>
                Cross References
            </h3>
        </div>
        <div class="gx-card-body">
          @if ( isset ($cross_refs) && count ($cross_refs) )
          <div class="table-responsive mt-0">
            <table class="table table-sm mt-0 mb-3">
              <thead>
                <tr>
                  <th>Book</th>
                  <th>Chapter</th>
                  <th>Verses</th>
                </tr>
              </thead>
              <tbody>


              @foreach ($cross_refs->take(9) AS $ref)
                <tr>
                  <td nowrap>
                    <a href="{{route ('books.show', $ref->book_slug)}}">
                    {{$ref->book_title}}
                    </a>
                  </td>
                  <td nowrap class="text-right"><a class="ml-0 mb-0 badge badge-warning" href="{{route('chapters.show', [$ref->book_slug, $ref->chap_num])}}">{{$ref->chap_num}}</a></td>
                  <td nowrap class="text-center">
                      @if ( count($ref->verses) == 1 )
                        <a href="{{route ('verses.show', [$ref->book_slug, $ref->chap_num, $ref->verses[0]])}}"><small class="mr-2 badge badge-light">{{$ref->verses[0]}}</small></a>
                      @else
                        @if ($ref->verses[0] == $ref->verses[1])
                          <a href="{{route ('verses.show', [$ref->book_slug, $ref->chap_num, $ref->verses[0]])}}"><small class="mr-2 badge badge-light">{{$ref->verses[0]}}</small></a>
                        @else
                          <a href="{{route ('verses.show', [$ref->book_slug, $ref->chap_num, $ref->verses[0].'-'.$ref->verses[1]])}}"><small class="mr-2 badge badge-light">{{$ref->verses[0].'-'.$ref->verses[1]}}</small></a>
                        @endif
                      @endif

                  </td>
                </tr>

              @endforeach

          </tbody>
        </table>
        <a class="gx-btn gx-btn-default float-right" href="{{route ('verses.crossrefs', [$book->slug, $chapter->number, $verse_range])}}">View All</a>
        </div>
      @else
        @include ('layouts.themes.mouldify.partials.empty', ['text' => 'No cross-references available.'])
      @endif
      </div>
    </article>
