<title>{!! $title !!}</title>
<property name="og:title" content="{!! $title !!}">
<meta name="description" content="{!! $info !!}">
<property name="og:description" content="{!! $info !!}"/>
