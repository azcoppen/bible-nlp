<div class="gx-card">
  <ul class="list-unstyled zmdi-hc-ul mb-0">
    <li><i class="zmdi-hc-li zmdi zmdi-library"></i>
      <a title="" href="{{route ('books.index', $chapter->book->volume)}}">
      {{$chapter->book->volume == 'OT' ? 'Old Testament' : 'New Testament'}}
      </a>
        <ul class="list-unstyled zmdi-hc-ul">
            <li><i class="zmdi-hc-li zmdi zmdi-book"></i>
              <a title="" href="{{route ('books.show', $chapter->book->slug)}}">
                {{$chapter->book->title}} ({{$chapter->book->chaps}})
              </a>
                <ul class="list-unstyled zmdi-hc-ul">
                    <li><i class="zmdi-hc-li zmdi zmdi-collection-bookmark"></i>
                        {{$chapter->number}}
                    </li>
                </ul>
            </li>
        </ul>
    </li>
    <li><i class="zmdi-hc-li zmdi zmdi-comment-outline"></i> {{ count ($chapter->verses)}} verses
      <ul class="list-unstyled zmdi-hc-ul">
          <li><i class="zmdi-hc-li zmdi zmdi-long-arrow-up"></i>
            {{ $chapter->longest_verse->number }} ({{strlen ($chapter->longest_verse->text['en'])}})
          </li>
          <li><i class="zmdi-hc-li zmdi zmdi-long-arrow-down"></i>
            {{ $chapter->shortest_verse->number }} ({{strlen ($chapter->shortest_verse->text['en'])}})
          </li>
          <li><i class="zmdi-hc-li zmdi zmdi-trending-up"></i>
            {{ round ($chapter->average_verse_length, 1) }}
          </li>
      </ul>
    </li>

      <li><i class="zmdi-hc-li zmdi zmdi-label-alt-outline"></i> {{ str_word_count ($chapter->full_text)}} Words
      </li>
      <li><i class="zmdi-hc-li zmdi zmdi-labels"></i> {{ count ($chapter->freqs)}} Frequencies

      </li>
      <li><i class="zmdi-hc-li zmdi zmdi-collection-plus"></i>{{ count ($chapter->unique_entities)}} Entities
          <ul class="list-unstyled zmdi-hc-ul">
            @if ( count ($chapter->unique_person_entities) )
              <li><i class="zmdi-hc-li zmdi zmdi-male-female"></i>
                  {{count ($chapter->unique_person_entities)}} Persons
              </li>
            @endif
            @if ( count ($chapter->unique_org_entities) )
              <li><i class="zmdi-hc-li zmdi zmdi-group"></i>
                  {{count ($chapter->unique_org_entities)}} Organisations
              </li>
            @endif
            @if ( count ($chapter->unique_location_entities) )
              <li><i class="zmdi-hc-li zmdi zmdi-pin"></i>
                  {{count ($chapter->unique_location_entities)}} Locations
              </li>
            @endif
            @if ( count ($chapter->unique_event_entities) )
              <li><i class="zmdi-hc-li zmdi zmdi-calendar"></i>
                  {{count ($chapter->unique_event_entities)}} Events
              </li>
            @endif
            @if ( count ($chapter->unique_consumer_good_entities) )
              <li><i class="zmdi-hc-li zmdi zmdi-laptop"></i>
                  {{count ($chapter->unique_consumer_good_entities)}} Events
              </li>
            @endif
            @if ( count ($chapter->unique_work_of_art_entities) )
              <li><i class="zmdi-hc-li zmdi zmdi-format-color-fill"></i>
                  {{count ($chapter->unique_work_of_art_entities)}} Events
              </li>
            @endif
            @if ( count ($chapter->unique_other_entities) )
              <li><i class="zmdi-hc-li zmdi zmdi-collection-item"></i>
                  {{count ($chapter->unique_other_entities)}} Other
              </li>
            @endif
          </ul>
      </li>

  </ul>
</div>
