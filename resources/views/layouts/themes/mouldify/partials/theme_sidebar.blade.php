<!-- Right Sidebar-->
<div id="colorSidebar" class="app-sidebar-content right-sidebar">
    <div class="color-theme">
        <div class="color-theme-header">
            <h3 class="color-theme-title">Choose Your Theme</h3>
            <a href="javascript:void(0)" class="action-btn" id="close-setting-panel">
                <i class="zmdi zmdi-close text-white"></i>
            </a>
        </div>

        <div class="color-theme-body">
            <h3>Light</h3>
            <ul class="color-option">
                <li>
                    <a href="javascript:void(0)" class="bg-indigo gx-theme" data-theme="indigo"></a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="bg-cyan gx-theme" data-theme="cyan"></a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="bg-amber gx-theme" data-theme="amber"></a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="bg-deep-orange gx-theme" data-theme="deep-orange"></a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="bg-pink gx-theme" data-theme="pink"></a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="bg-blue gx-theme" data-theme="blue"></a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="bg-deep-purple gx-theme" data-theme="deep-purple"></a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="bg-green gx-theme" data-theme="green"></a>
                </li>
            </ul>
            <h3>Dark</h3>
            <ul class="color-option cr-op-dark-sidebar">
                <li>
                    <a href="javascript:void(0)" class="bg-indigo gx-theme" data-theme="dark-indigo"></a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="bg-cyan gx-theme" data-theme="dark-cyan"></a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="bg-amber gx-theme" data-theme="dark-amber"></a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="bg-deep-orange gx-theme" data-theme="dark-deep-orange"></a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="bg-pink gx-theme" data-theme="dark-pink"></a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="bg-blue gx-theme" data-theme="dark-blue"></a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="bg-deep-purple gx-theme" data-theme="dark-deep-purple"></a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="bg-green gx-theme" data-theme="dark-green"></a>
                </li>
            </ul>
            <h3>Night Mode</h3>
            <div class="material-switch">
                <input id="switch-dark-theme" name="switch-dark-theme" type="checkbox" data-theme="dark" />
                <label for="switch-dark-theme" class="label-default"></label>
            </div>
        </div>
    </div>
</div>
<!-- /Right Sidebar-->
