<meta charset="utf-8">
<link rel="dns-prefetch" href="//cdn.rocketbible.com" />
<link rel="dns-prefetch" href="//cdnjs.cloudflare.com" />
<link rel="dns-prefetch" href="//code.jquery.com" />
<link rel="dns-prefetch" href="//stackpath.bootstrapcdn.com" />
<link rel="dns-prefetch" href="//cdn.jsdelivr.net" />

<link rel="preconnect" href="https://cdn.rocketbible.com">
<link rel="preconnect" href="https://cdnjs.cloudflare.com">
<link rel="preconnect" href="https://code.jquery.com">
<link rel="preconnect" href="https://stackpath.bootstrapcdn.com">
<link rel="preconnect" href="https://cdn.jsdelivr.net">

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="author" content="Alex Coppen">
<meta name="robots" content="index, follow">
<meta name="subject" content="The fastest way to get around the Bible">
<meta name="copyright"content="RocketBible">
<meta name="language" content="EN">
<link rel="canonical" href="{{url('/')}}" />
<meta name="cache:view-data" content="{{isset($DATA_CACHE) ? $DATA_CACHE : 'MISS'}}">

<meta property="og:site_name" content="RocketBible.com"/>
<meta property="og:url" content="{{request()->Url()}}">
<meta property="og:image" content="{{ cdn ('snapshot.png') }}">

<meta name="twitter:card" content="app">
<meta name="twitter:site" content="@">
<meta name="twitter:app:name:iphone" content="RocketBible">
<meta name="twitter:app:id:iphone" content="RocketBible">
<meta name="twitter:app:name:ipad" content="RocketBible">
<meta name="twitter:app:id:ipad" content="RocketBible">
<meta name="twitter:app:name:googleplay" content="RocketBible">
<meta name="twitter:app:id:googleplay" content="RocketBible">

<link rel="shortcut icon" href="{{ cdn ('favicon.ico') }}" />
<link rel="apple-touch-icon" sizes="180x180" href="{{ cdn ('apple-touch-icon.png') }}">
<link rel="icon" type="image/png" sizes="32x32" href="{{ cdn ('favicon-32x32.png') }}">
<link rel="icon" type="image/png" sizes="16x16" href="{{ cdn ('favicon-16x16.png') }}">
<link rel="manifest" href="{{ cdn ('site.webmanifest') }}">
<link rel="mask-icon" href="{{ cdn ('safari-pinned-tab.svg') }}" color="#5bbad5">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css" />
<link rel="stylesheet" href="{{ cdn ('fonts/sprite-flags-master/sprite-flags-32x32.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/1.4.0/css/perfect-scrollbar.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/c3/0.6.10/c3.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chartist/0.11.0/chartist.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/12.1.0/nouislider.min.css" />
<link rel="stylesheet" href="{{ cdn ('css/rocketbible.min.css') }}" >
<link id="override-css-id" href="{{ cdn ('css/theme-'.($theme ?? config('app.theme')).'.min.css') }}" rel="stylesheet">
