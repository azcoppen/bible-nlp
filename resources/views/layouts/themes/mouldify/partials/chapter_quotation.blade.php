<article class="gx-card">
  <blockquote class="blockquote lead chapter-full">
    @foreach ($chapter->verses AS $verse)
      <a rel="{{$verse->number}}" class="para-verse" title="" href="{{route ('verses.show', [$book->slug, $chapter->number, $verse->number])}}">
        <q>{!! highlight ( quotations ($verse->text['en']), $chapter->unique_entities->pluck('name')->all()) !!}</q>
      </a>
    @endforeach
  </blockquote>
</article>
