<nav id="menu" class="side-nav gx-sidebar">
    <div class="navbar-expand-lg">
        <!-- Sidebar header  -->
        <div class="sidebar-header">
            <a title="RocketBible Home" class="site-logo" href="{{url('/')}}">
                <img src="{{ cdn ('images/logo.png') }}" alt="RocketBible Home" title="RocketBible Home">
            </a>
        </div>
        <!-- /sidebar header -->

        <!-- Main navigation -->
        <div id="main-menu" class="main-menu navbar-collapse collapse d-none d-md-block">
            <ul class="nav-menu">
                <li class="nav-header">Explore</li>

                @if (isset($nav_menu) && $nav_menu == 'search')
                <li class="menu no-arrow {{isset ($nav_menu) && $nav_menu == 'search' ? 'open selected' : ''}}">
                  <a title="Search results for &quot;{{request()->get('q')}}&quot;" href="{{route ('global.search')}}?q={{request()->get('q')}}">
                    <i class="zmdi zmdi-search zmdi-hc-fw"></i>
                    <span class="nav-text">
                      <span>Search</span>
                    </span>
                  </a>
                </li>
                @endif

                <li class="menu no-arrow {{isset ($nav_menu) && $nav_menu == 'books' ? 'open' : ''}}">
                    <a title="Books of the Bible" href="javascript:void(0)">
                        <i class="zmdi zmdi-library zmdi-hc-fw"></i>
                        <span class="nav-text">Books</span>
                        <small class="ml-2 mb-0 badge badge-primary float-right">66</small>
                    </a>
                    <ul class="sub-menu">
                        <li class="{{isset ($subnav) && $subnav == 'OT' ? 'active' : ''}}"><a title="Books of the Old Testament" href="{{route ('books.index', 'OT')}}"><span class="nav-text">Old Testament <small class="ml-2 mb-0 badge badge-light float-right">39</small></span></a></li>
                        <li class="{{isset ($subnav) && $subnav == 'NT' ? 'active' : ''}}"><a title="Books of the New Testament" href="{{route ('books.index', 'NT')}}"><span class="nav-text">New Testament <small class="ml-2 mb-0 badge badge-light float-right">27</small></span></a></li>
                    </ul>
                </li>

                <li class="menu no-arrow  {{isset ($nav_menu) && $nav_menu == 'entities' ? 'open' : ''}}">
                    <a title="Named entities found in the Bible" href="javascript:void(0)">
                        <i class="zmdi zmdi-developer-board zmdi-hc-fw"></i>
                        <span class="nav-text">Entities</span>
                        <small class="ml-2 mb-0 badge badge-primary float-right">11,795</small>
                    </a>
                    <ul class="sub-menu">
                        <li class="{{isset ($subnav) && $subnav == 'person' ? 'active' : ''}}"><a title="Named Person Entities found in the Bible" href="{{route ('entities.type', 'person')}}"><span class="nav-text">Person <small class="ml-2 mb-0 badge badge-light float-right">3,748</small></span></a></li>
                        <li class="{{isset ($subnav) && $subnav == 'organization' ? 'active' : ''}}"><a title="Named Organization Entities found in the Bible" href="{{route ('entities.type', 'organization')}}"><span class="nav-text">Organisation <small class="ml-2 mb-0 badge badge-light float-right">341</small></span></a></li>
                        <li class="{{isset ($subnav) && $subnav == 'location' ? 'active' : ''}}"><a title="Named Location Entities found in the Bible" href="{{route ('entities.type', 'location')}}"><span class="nav-text">Location <small class="ml-2 mb-0 badge badge-light float-right">1.268</small></span></a></li>
                        <li class="{{isset ($subnav) && $subnav == 'event' ? 'active' : ''}}"><a title="Named Event Entities found in the Bible" href="{{route ('entities.type', 'event')}}"><span class="nav-text">Event <small class="ml-2 mb-0 badge badge-light float-right">415</small></span></a></li>
                        <li class="{{isset ($subnav) && $subnav == 'consumer_good' ? 'active' : ''}}"><a title="Named Consumer Good Entities found in the Bible" href="{{route ('entities.type', 'consumer_good')}}"><span class="nav-text">Consumer Good <small class="ml-2 mb-0 badge badge-light float-right">225</small></span></a></li>
                        <li class="{{isset ($subnav) && $subnav == 'work_of_art' ? 'active' : ''}}"><a title="Named Work of Art Entities found in the Bible" href="{{route ('entities.type', 'work_of_art')}}"><span class="nav-text">Work Of Art <small class="ml-2 mb-0 badge badge-light float-right">210</small></span></a></li>
                        <li class="{{isset ($subnav) && $subnav == 'other' ? 'active' : ''}}"><a title="Other/Misc Named Entities found in the Bible" href="{{route ('entities.type', 'other')}}"><span class="nav-text">Other <small class="ml-2 mb-0 badge badge-light float-right">5,858</small></span></a></li>
                    </ul>
                </li>


                <li class="menu no-arrow {{isset ($nav_menu) && $nav_menu == 'topics' ? 'open' : ''}}">
                  <a title="Contextual topics found in the Bible" href="{{route ('topics.index')}}">
                    <i class="zmdi zmdi-card-giftcard zmdi-hc-fw"></i>
                    <span class="nav-text"><span>Topics</span>
                    <small class="ml-2 mb-0 badge badge-primary float-right">4,980</small>
                    </span>
                  </a>
                </li>

                <li class="menu no-arrow {{isset ($nav_menu) && $nav_menu == 'frequencies' ? 'open selected' : ''}}">
                  <a title="Word occurence frequencies in the Bible" href="{{route ('frequencies.index')}}">
                    <i class="zmdi zmdi-collection-item-7 zmdi-hc-fw"></i>
                    <span class="nav-text">
                      <span>Words</span>
                      <small class="ml-2 mb-0 badge badge-primary float-right">13,648</small>
                    </span>
                  </a>
                </li>


            </ul>
        </div>
        <!-- /main navigation -->
    </div>
</nav>
