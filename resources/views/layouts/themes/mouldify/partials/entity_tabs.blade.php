<ul class="card-header-pills nav nav-pills nav-fill" role="tablist">
    <li class="nav-item">
        <a class="nav-link {{$type == 'all' ? 'active show' : ''}}" href="{{ !is_null ($az) ? route ('entities.az', ['all', $az]) : route ('entities.type', 'all')}}" role="tab"  aria-selected="false">All</a>
    </li>

    <li class="nav-item">
        <a class="nav-link {{$type == 'person' ? 'active show' : ''}}" href="{{ !is_null ($az) ? route ('entities.az', ['person', $az]) : route ('entities.type', 'person')}}" role="tab"  aria-selected="false">Person</a>
    </li>

    <li class="nav-item">
        <a class="nav-link {{$type == 'organization' ? 'active show' : ''}}" href="{{ !is_null ($az) ? route ('entities.az', ['organization', $az]) : route ('entities.type', 'organization')}}" role="tab"  aria-selected="false">Organisation</a>
    </li>

    <li class="nav-item">
        <a class="nav-link {{$type == 'location' ? 'active show' : ''}}" href="{{ !is_null ($az) ? route ('entities.az', ['location', $az]) : route ('entities.type', 'location')}}" role="tab"  aria-selected="false">Location</a>
    </li>

    <li class="nav-item">
        <a class="nav-link {{$type == 'event' ? 'active show' : ''}}" href="{{ !is_null ($az) ? route ('entities.az', ['event', $az]) : route ('entities.type', 'event')}}" role="tab"  aria-selected="false">Event</a>
    </li>

    <li class="nav-item">
        <a class="nav-link {{$type == 'consumer_good' ? 'active show' : ''}}" href="{{ !is_null ($az) ? route ('entities.az', ['consumer_good', $az]) : route ('entities.type', 'consumer_good')}}" role="tab"  aria-selected="false">Consumer Good</a>
    </li>

    <li class="nav-item">
        <a class="nav-link {{$type == 'work_of_art' ? 'active show' : ''}}" href="{{ !is_null ($az) ? route ('entities.az', ['work_of_art', $az]) : route ('entities.type', 'work_of_art')}}" role="tab"  aria-selected="false">Work of Art</a>
    </li>

    <li class="nav-item">
        <a class="nav-link {{$type == 'other' ? 'active show' : ''}}" href="{{ !is_null ($az) ? route ('entities.az', ['other', $az]) : route ('entities.type', 'other')}}" role="tab"  aria-selected="false">Other</a>
    </li>
</ul>
