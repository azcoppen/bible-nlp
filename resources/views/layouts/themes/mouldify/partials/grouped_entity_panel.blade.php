


        @foreach ( $entity_groups AS $name => $group )
          @if ( count ($group) )
            <h3 class="">{{title_case($name)}} <span class="ml-2 mb-0 badge badge-light pull-right">{{count($group)}}</span></h3>
            <div class="row btn-group ml-1">
            @include ('layouts.themes.mouldify.partials.entity_list', [
              'type' => $name,
              'entities' => $group,
              'icon' => 'male-female'
            ])
            </div>
            <hr />
          @endif
        @endforeach
