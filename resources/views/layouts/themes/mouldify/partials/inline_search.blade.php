{!! Form::open (['method' => 'GET']) !!}
<div class="form-group row">
    <div class="input-group col-md-12 col-sm-12">
      {!! Form::text ('q', request()->get('q'), ['class' => 'form-control '. (request()->has('q') ? 'border-success' : ''), 'placeholder' => $text, 'required']) !!}
        <div class="input-group-append">
            <button class="btn btn-primary" type="submit">Q</button>
        </div>
    </div>
</div>
{!! Form::close () !!}
