

      <article class="gx-card">
        <div class="gx-card-header">
          <small class="ml-2 mb-0 badge badge-secondary float-right">{{$topics->count()}}</small></a>
            <h3 class="card-heading">
                Topics
            </h3>
        </div>
        @if (isset($topics) && count ($topics) )
        <ul class="list-unstyled ml-0">
          @foreach ( $topics AS $title => $subtopics )
            <li class="mb-2">
                <h3 class="font-weight-semibold clearfix">
                  <i class="zmdi zmdi-card-giftcard zmdi-hc-fw text-primary"></i>
                  <a class="" href="{{route ('topics.show', [$subtopics->first()->az, $subtopics->first()->title_slug])}}">
                  {{fix_utf8($title)}}
                  </a>
                </h3>
                @if ( count ($subtopics) )
                <ul class="ml-1 clearfix mb-2">
                  @foreach ($subtopics->unique('subject')->sortBy('subject') AS $sub)
                  <li class="ml-2 mb-2"><a href="{{route('topics.subject', [$sub->az, $sub->title_slug, $sub->subject_slug])}}">{{fix_utf8($sub->subject)}}</a></li>
                  @endforeach
                </ul>
                @endif
            </li>
          @endforeach
        </ul>
        @else
          @include ('layouts.themes.mouldify.partials.empty', ['text' => 'No related topics.'])
        @endif
      </article>
