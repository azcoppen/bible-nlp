@if ( isset ($frequencies) && count ($frequencies) )
  <table class="table">
    <tbody>
      @foreach ($frequencies AS $freq)
        <tr>
          <td><span class="badge badge-light">{{$freq->total}}</span></td>
          <td><a title="" href="">{{title_case($freq->word)}}</a></td>
        </tr>
      @endforeach
    </body>
  </table>
@endif
