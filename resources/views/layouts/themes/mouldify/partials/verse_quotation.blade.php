
    <article class="gx-card bg-primary text-white card verse-single">
      <blockquote class="blockquote lead {{isset($bold) && $bold !== FALSE ? 'font-weight-semibold' : ''}}">
        @foreach ($verses AS $verse)
          <h1 class="font-weight-light">
          <a rel="{{$verse->number}}" class="para-verse-white" data-toggle="tooltip" data-placement="left" title="Verse {{$verse->number}}" href="{{route ('verses.show', [$book->slug, $chapter->number, $verse->number])}}">
            <q>{!! highlight ( quotations ($verse->text['en']), $chapter->unique_entities->pluck('name')->all()) !!}</q>
          </a>
          </h1>
        @endforeach
      </blockquote>
    </article>
