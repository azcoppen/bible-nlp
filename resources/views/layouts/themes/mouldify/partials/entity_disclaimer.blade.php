<aside clas="row mt-3 clearfix" style="margin-top: 2rem;">
  <small class=""><i>
  <b>Important:</b> Named Entities are identified by machine learning (Google NLP) and should be treated as &quot;best guess&quot; data. Singular and plural can be categorised separately,
  capitalised words are often dually-categorised (e.g. as &quot;person&quot; and also &quot;other&quot;), and often entities are not identified at all. A count for &quot;Lord&quot;
  might have 7 occurrences, where &quot;lord&quot; may have 3 (total 10). &quot;God&quot; is a person, whereas a &quot;god&quot; is a thing/other. Distinct words/phrases which occur
  less frequently are likely to have higher accuracy. In some cases, entity counts will not correspond to word frequency counts. This data should be considered as an indicator with
  varying margins of error, rather than a standardised metric.
</i></small>
</aside>
