@if ( isset ($similar) && count ($similar) )

  <div class="gx-card">
      <div class="gx-card-header">
          <h3 class="card-heading">
            Similar Verses
            <a class="gx-btn gx-btn-sm gx-btn-default float-right" href="{{route ('verses.similar', [$book->slug, $chapter->number, $verse_range])}}">All
              <span class="ml-2 mb-0 badge badge-secondary float-right">{{count($similar)}}</span></a>

          </h3>
      </div>
      <div class="gx-card-body">
        @if (isset($similar) && count($similar))
        <div class="table-responsive mt-0">
          <table class="table table-sm mt-0 mb-3">
            <thead>
              <tr>
                <th>Book</th>
                <th>Chapter</th>
                <th>Verse</th>
              </tr>
            </thead>
            <tbody>
              @foreach ( $similar->take(9) AS $sim )
                <tr>
                  <td nowrap>
                    <a href="{{route ('books.show', $sim->book_slug)}}">
                    {{$sim->book_title}}
                    </a>
                  </td>
                  <td nowrap class="text-right"><a class="ml-0 mb-0 badge badge-warning" href="{{route('chapters.show', [$sim->book_slug, $sim->chap_num])}}">{{$sim->chap_num}}</a></td>
                  <td nowrap class="text-center">
                    <a href="{{route ('verses.show', [$sim->book_slug, $sim->chap_num, $sim->number])}}">
                      <small class="mr-2 badge badge-light">
                      {{$sim->number}}
                      </small>
                    </a>
                  </small></td>
                </tr>
              @endforeach
        </tbody>
      </table>
      <a class="gx-btn gx-btn-default float-right" href="{{route ('verses.similar', [$book->slug, $chapter->number, $verse_range])}}">View All</a>
      </div>
    @else
      @include ('layouts.themes.mouldify.partials.empty', ['text' => 'No similar verses available.'])
    @endif
  </div>
</div>


@endif
