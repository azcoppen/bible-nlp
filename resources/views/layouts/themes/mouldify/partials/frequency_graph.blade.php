
      <div class="gx-card">
          <div class="gx-card-header">
              <h3 class="card-heading">
                  Word Frequencies
              </h3>
          </div>
          <div class="gx-card-body">
              <div id="frequencies" class="ct-perfect-fourth"></div>
              @include ('layouts.themes.mouldify.partials.frequency_disclaimer')
          </div>

      </div>


@push ('js')
  <script>
  var freq_chart = new Chartist.Bar('#frequencies', {
    @if (isset($chapter_view) && $chapter_view !== FALSE )
      labels: {!! json_encode ($frequencies->filter(function ($value, $key) {
    return $value > 1;
})->keys ()) !!},
      series: [
          {!! json_encode ($frequencies->filter(function ($value, $key) {
    return $value > 1;
})->values ()) !!}
      ]
    @else
    labels: {!! json_encode ($frequencies->keys ()) !!},
    series: [
        {!! json_encode ($frequencies->values ()) !!}
    ]
    @endif
  }, {
      seriesBarDistance: 10,
      reverseData: true,
      horizontalBars: true,
      axisY: {
          offset: 70,
          onlyInteger: true
      }
  });
  </script>
@endpush
