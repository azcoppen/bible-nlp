<header class="main-header">
    <div class="gx-toolbar">
        <div class="sidebar-mobile-menu">
            <a title="This button auto-hides on mobiles and tablets" class="gx-menu-icon menu-toggle" href="#menu">
                <span class="menu-icon icon-grey"></span>
            </a>
        </div>

        <div class="search-bar right-side-icon bg-transparent d-none d-sm-block">
          {!! Form::open (['route' => ['global.search'], 'method' => 'GET']) !!}
            <div class="form-group">
                <input id="gq" name="q" class="form-control border-0" placeholder="Search chapters, verses, entities, topics..." value="{{request()->get('q')}}" type="search">
                <button class="search-icon" type="submit"><i class="zmdi zmdi-search zmdi-hc-lg"></i></button>
            </div>
            {!! Form::close() !!}
        </div>

        <ul class="quick-menu header-notifications ml-auto">

          <li id="colorTheme" class="theme-option bg-light">
              <a title="Change the colour and theme of RocketBible" href="javascript:void(0)"
                 class="d-inline-block text-primary font-weight-light" aria-expanded="true">
                <i class="zmdi zmdi-format-color-fill"></i>
              </a>
          </li>

            <li class="dropdown language-menu">
                <a title="Change the language you read RocketBible in (Currently only English is supported)" href="javascript:void(0)" data-toggle="dropdown" aria-haspopup="true"
                   class="d-inline-block flag-icon" aria-expanded="true">
                    <i class="flag flag-32 flag-us"></i>
                </a>

                <div role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                    <div class="messages-list">
                        <ul class="list-unstyled">
                            <li class="media">
                                <i class="flag flag-32 flag-us"></i>
                                <a title="English" href="javascript:void(0)" class="media-body align-self-center">
                                    English
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </li>

            <li class="nav-searchbox dropdown d-inline-block d-sm-none">
                <a title="Your personal RocketBible settings" href="javascript:void(0)" data-toggle="dropdown" aria-haspopup="true" class="d-inline-block icon-btn" aria-expanded="false">
                    <i class="zmdi zmdi-search zmdi-hc-fw"></i>
                </a>
                <div aria-hidden="true" class="p-0 dropdown-menu dropdown-menu-right search-bar right-side-icon search-dropdown">
                  {!! Form::open (['route' => ['global.search'], 'method' => 'GET']) !!}
                    <div class="form-group">
                        <input id="gq" name="q" class="form-control border-0" placeholder="Search chapters, verses, entities, topics..." value="{{request()->get('q')}}" type="search">
                        <button class="search-icon" type="submit"><i class="zmdi zmdi-search zmdi-hc-lg"></i></button>
                    </div>
                    {!! Form::close() !!}
                </div>

            </li>



            <li class="dropdown user-nav">
                <a title="Your RocketBible Account" class="dropdown-toggle no-arrow d-inline-block" href="#" role="button" id="userInfo"
                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img class="user-avatar size-40" src="{{ cdn ('images/placeholder.jpg')}}" alt="...">
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userInfo">
                    <div class="user-profile">
                        <img class="user-avatar border-0 size-40" src="{{ cdn ('images/placeholder.jpg')}}"
                             alt="User">
                        <div class="user-detail ml-2">
                            <h4 class="user-name mb-0">Anonymous</h4>
                            <small>Guest User</small>
                        </div>
                    </div>
                    <a title="Feature restricted by policy" class="dropdown-item" href="javascript:void(0)">
                        <i class="zmdi zmdi-face zmdi-hc-fw mr-1"></i>
                        Not Available (Beta)
                    </a>
                </div>
            </li>
        </ul>
    </div>
</header>
