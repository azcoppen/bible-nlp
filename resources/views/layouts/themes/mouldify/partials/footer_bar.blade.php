<footer class="gx-footer">
    <div class="d-flex flex-row justify-content-between">
        <p> Data Copyright RocketBible &copy; {{date('Y')}}. Created by <a title="Read the article describing how RocketBIble was built on Alex Coppen's Medium.com page" target="_blank" href="https://medium.com/@shutupcoppen">Alex Coppen</a>. Is there an API? <i>Yes</i>.
          <!--
          <br />
          <small>Thanks to
            <a target="_blank" href="https://www.biblegateway.com/versions/New-International-Version-NIV-Bible/">BibleGateway (NIV)</a>,
            <a target="_blank" href="https://www.digitalbibleplatform.com/">Digital Bible</a>,
            <a target="_blank" href="https://viz.bible/">Viz.Bible</a>,
            <a target="_blank" href="https://github.com/robertrouse/KJV-bible-database-with-metadata-MetaV-">Robert Rouse</a>,
            <a target="_blank" href="http://openscriptures.org/">OpenScriptures</a>,
            <a target="_blank" href="https://github.com/godlytalias/Bible-Database">Godly T. Alias</a>, and
            <a target="_blank" href="https://cloud.google.com/natural-language/">Google Natural Language API</a>.
            </small>
          -->
        </p>
        <a title="RocketBible.com - search the scriptures as fast as God can" href="https://rocketbible.com" class="btn-link" target="_blank">RocketBible.com</a>
    </div>
</footer>
