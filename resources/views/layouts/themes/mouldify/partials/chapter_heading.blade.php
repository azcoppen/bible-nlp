<div class="row">

  <ul class="col-xs-12 col-sm-12" style="list-style-type: none; margin: 0; padding: 0;">

    @if ( $chapter->previous )
      <li class="text-left float-left pl-3" style="display:block;">
          <a class="gx-btn gx-btn-sm gx-btn-default" style="font-size: 16px;" data-toggle="tooltip" data-placement="bottom" title="Read Chapter {{$chapter->previous}}" href="{{route ('chapters.show', [$book->slug, $chapter->previous])}}">
            <i class="zmdi zmdi-fast-rewind text-secondary zmdi-hc-2x"></i>&nbsp;&nbsp;{{$chapter->previous}}</span>
          </a>
      </li>
    @endif

    @if ( $chapter->next )
      <li class="text-right float-right" style="display:block; ">
          <a class="gx-btn gx-btn-sm gx-btn-default" style="font-size: 16px;" data-toggle="tooltip" data-placement="bottom" title="Read Chapter {{$chapter->next}}" href="{{route ('chapters.show', [$book->slug, $chapter->next])}}">
            {{$chapter->next}}&nbsp;&nbsp; <i class="zmdi zmdi-fast-forward text-success zmdi-hc-2x"></i>
          </a>
      </li>
    @endif

  </ul>

</div>

  <div class="row">

    <div class="col-md-4 col-md-12 text-center mb-2">
      <h1 class="">{{strtoupper($book->title)}} <span class="font-weight-semibold">CHAPTER <span class="text-secondary">{{$chapter->number}}</span></span></h1>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      {!! Form::open (['route' => ['books.search', $book->slug], 'method' => 'GET', 'class'=>'form-horizontal']) !!}
      <div class="input-group" style="margin-top: -5px !important;">
          <input id="q" name="q" type="text" class="form-control" required placeholder="Search all of {{$book->title}}" aria-label="Search {{$book->title}}" value="{{request()->input('q')}}">
          <div class="input-group-append">
              <button class="btn btn-default" type="submit"><i class="zmdi zmdi-search"></i></button>
          </div>
      </div>

      {!! Form::close () !!}
    </div>
  </div>


</header>

<div class="row mt-3 mb-4 d-none d-md-block">
  <div class="col-md-10 mb-3 float-left">
    <div id="verse-slider" class="mb-3"></div>
  </div>
  <div class="col-md-2 mb-3 float-right text-right">
    <a href="javascript:void(0)" id="slice-btn" class="gx-btn gx-btn-sm gx-btn-label left gx-btn-secondary">
        <i class="zmdi zmdi-refresh"></i>
        <span>Slice</span>
    </a>
  </div>
</div>

<div class="" style="clear:both;">&nbsp;</div>

<div class="row clearfix mt-2" style="clear:both;">
  <h3 class="text-muted lead col-md-12 mt-2">
    <summary>{{$book->summary}}</summary>
  </h3>
  <div class="pr-2 col-md-12 ml-3 mt-2">
    <h6 class="font-weight-light blockquote pl-2" style="font-size: 1rem;">
      <summary>
      <span class="text-warning">{{$chapter->number}} <i class="zmdi zmdi-long-arrow-right zmdi-hc-fw"></i></span>
      {!! fix_utf8_apos($chapter->summaries->first()->text['en'] ?? '') !!}
    </h6>
    </summary>
  </div>
</div>
