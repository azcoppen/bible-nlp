<div class="alert-addon-card bg-info text-white alert alert-dismissible fade show border border-info" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    <span class="icon-addon alert-addon"><i class="zmdi zmdi-info zmdi-hc-fw zmdi-hc-lg"></i></span>
    <span class="d-inline-block">{{$text ?? ''}}</span>
</div>
