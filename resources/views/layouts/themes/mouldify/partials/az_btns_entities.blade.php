<div class="btn-group flex-wrap text-center" role="group" aria-label="A-Z List">
  <a title="" class="gx-btn gx-btn-sm gx-btn-{{ !isset($selected) || empty($selected) ? 'secondary' : 'default' }}" href="{{$reset}}"><i class="zmdi zmdi-collection-bookmark zmdi-hc-fw {{ !isset($selected) || empty($selected) ? 'text-white' : '' }}"></i></a>
  @foreach ( range ('A', 'Z') AS $index => $letter )
    <a title="" class="gx-btn gx-btn-sm gx-btn-{{ isset($selected) && $selected == $letter ? 'secondary' : 'default' }}" href="{{route($route, [$type, $letter])}}">{{$letter}}</a>
  @endforeach
</div>
