@extends ('layouts.themes.mouldify.master')

@section ('sharing')
  @include ('layouts.themes.mouldify.partials.sharing', [
    'title' => 'Welcome To RocketBible: scan scripture as fast as God does',
    'info'  => 'The fastest way to read the Bible. Search books, chapters, verses and related content instantly. Track named entities, word occurrences, and scriptural topics across all 66 books.',
  ])
@stop

@section ('content')
  <div class="dashboard">
      <div class="mouldifitron">
          <div class="display-4 mb-3 font-weight-light ml-1">The Fastest Way To Read The Bible</div>

          <h2 class="font-weight-light ml-1">66 books, 1,189 chapters, 31,102 verses, 773,692 words, and 3,116,480 letters, written over 1600 years by 40 people. 6,300 prophecies, 8,810 promises, 3,294 questions, and 6,468 commands. 180,000 copies sold a day in 1,200 languages.</h2>
          <hr class="d-none d-sm-block mb-3"  />

          <section class="row ml-2 mb-3 mr-1">
            <article class="col-md-4 col-xs-12">
              <div class="row">
                <div class="size-40 bg-white lighten-1 d-flex-xy-ctr col-md-2 col-sm-12 col-xs-12 d-none d-md-block">
                    <i class="zmdi zmdi-desktop-mac text-green zmdi-hc-2x "></i>
                </div>
                <div class="col-xs-12 col-sm-12 d-block d-md-none text-center mb-4 mt-4">
                  <div class="size-40 bg-white lighten-1 d-flex-xy-ctr m-auto">
                      <i class="zmdi zmdi-desktop-mac text-green zmdi-hc-4x"></i>
                  </div>
                </div>
                <div class="col-md-10 col-xs-12">
                  <h3 class="font-weight-semibold text-green">A new way for anyone to explore scripture interactively</h2>
                    <p class="dash-details text-muted">The entire <a class="text-success" target="_blank" href="https://www.biblegateway.com/versions/New-International-Version-NIV-Bible/">NIV biblical corpus</a> broken down logically as an interactive, searchable, analytics-based database of <a class="text-success" target="_blank" href="http://linkeddata.org/">auto-linked semantic data</a> available to anyone instantly
                      on a computer, TV, phone, or tablet.</p>
                </div>
              </div>
            </article>

            <article class="col-md-4">
              <div class="row">
                <div class="size-40 bg-white lighten-1 d-flex-xy-ctr col-md-2">
                    <i class="zmdi zmdi-search text-secondary zmdi-hc-2x d-none d-md-block"></i>
                </div>
                <div class="col-xs-12 col-sm-12 d-block d-md-none text-center mb-4">
                  <div class="size-40 bg-white lighten-1 d-flex-xy-ctr m-auto">
                      <i class="zmdi zmdi-search text-secondary zmdi-hc-4x"></i>
                  </div>
                </div>
                <div class="col-md-10">
                  <h3 class="font-weight-semibold text-secondary">Search the 31,000 verses of the Bible programmatically</h2>
                  <p class="dash-details text-muted">Every piece of text in the NIV Bible (and it's metadata) indexed in <a class="text-secondary" target="_blank" href="https://www.elastic.co/">Elasticsearch</a>; accessible through ontological hierarchy, scanned for word frequency, NLP Named Entities, and related references. </p>
                </div>
              </div>
            </article>

            <article class="col-md-4">
              <div class="row">
                <div class="size-40 bg-white lighten-1 d-flex-xy-ctr col-md-2">
                    <i class="zmdi zmdi-accounts-list text-blue zmdi-hc-2x d-none d-md-block"></i>
                </div>
                <div class="col-xs-12 col-sm-12 d-block d-md-none text-center mb-4">
                  <div class="size-40 bg-white lighten-1 d-flex-xy-ctr m-auto">
                      <i class="zmdi zmdi-accounts-list text-blue zmdi-hc-4x"></i>
                  </div>
                </div>
                <div class="col-md-10">
                  <h3 class="font-weight-semibold text-blue">Track AI entities (people, locations) through every chapter</h2>
                    <p class="dash-details text-muted">Follow the occurences of Named Entities detected by Google's <a class="text-blue" style="color: #2196F3" target="_blank" href="https://cloud.google.com/natural-language/">Natural Language Processing service</a>:
                      people, organisations, locations, events, consumer goods, works of art, and recognisable "other" things.
                    </p>
                </div>
              </div>
            </article>
          </section>

          <hr class="d-none d-md-block" />

          <section class="row mt-4 ml-2 mb-3 mr-1">
            <article class="col-md-4">
              <div class="row">
                <div class="size-40 bg-white lighten-1 d-flex-xy-ctr col-md-2">
                    <i class="zmdi zmdi-globe-lock text-indigo zmdi-hc-2x d-none d-md-block"></i>
                </div>
                <div class="col-xs-12 col-sm-12 d-block d-md-none text-center mb-4">
                  <div class="size-40 bg-white lighten-1 d-flex-xy-ctr m-auto">
                      <i class="zmdi zmdi-globe-lock text-indigo zmdi-hc-4x"></i>
                  </div>
                </div>
                <div class="col-md-10">
                  <h3 class="font-weight-semibold text-indigo">Explore thousands of cross-references and similar sentences</h2>
                    <p class="dash-details text-muted">Thousands of human-organised cross-references from the <a target="_blank" href="https://www.biblestudytools.com/concordances/treasury-of-scripture-knowledge/">TSK</a>, coupled with "recommendation" results from <a target="_blank" href="https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-mlt-query.html">Elasticsearch's MLT similarity algorithm</a>, analysed against every verse in the corpus.</p>
                </div>
              </div>
            </article>

            <article class="col-md-4">
              <div class="row">
                <div class="size-40 bg-white lighten-1 d-flex-xy-ctr col-md-2">
                    <i class="zmdi zmdi-chart text-teal zmdi-hc-2x d-none d-md-block"></i>
                </div>
                <div class="col-xs-12 col-sm-12 d-block d-md-none text-center mb-4">
                  <div class="size-40 bg-white lighten-1 d-flex-xy-ctr m-auto">
                      <i class="zmdi zmdi-chart text-teal zmdi-hc-4x"></i>
                  </div>
                </div>
                <div class="col-md-10">
                  <h3 class="font-weight-semibold text-teal">Track word occurences through by book, chapter, and verse</h2>
                    <p class="dash-details text-muted">What if you wanted to know exactly where, and how many times, a <a style="color: #009688;" target="_blank" href="https://www.bibleanalyzer.com/manual/biblewordlists.htm">word is used in the Bible</a>? RocketBible will show you how frequently it occurs
                      across every book, its chapters, and/or its verses.</p>
                  </div>
                </div>
            </article>

            <article class="col-md-4">
              <div class="row">
                <div class="size-40 bg-white lighten-1 d-flex-xy-ctr col-md-2">
                    <i class="zmdi zmdi-collection-image-o text-yellow zmdi-hc-2x d-none d-md-block"></i>
                </div>
                <div class="col-xs-12 col-sm-12 d-block d-md-none text-center mb-4">
                  <div class="size-40 bg-white lighten-1 d-flex-xy-ctr m-auto">
                      <i class="zmdi zmdi-collection-image-o text-yellow zmdi-hc-4x"></i>
                  </div>
                </div>
                <div class="col-md-10">
                  <h3 class="font-weight-semibold text-yellow">Explore verses grouped into scriptural topics and subjects</h2>
                  <p class="dash-details text-muted">Every one of 5000+ sub-categories listed in the <a class="text-yellow" style="color: #ffeb3b;" target="_blank" href="http://www.tsk-online.com/">Treasury of Scripture Knowledge</a> (TSK) is linked against every record of every verse in the NIV Bible, allowing aggregated browsing of passages. </p>
                </div>
              </div>
            </article>

          </section>

          <hr class="d-none d-sm-block" />

          <section class="row mt-4 ml-2 mr-1">
            <article class="col-md-4">
              <div class="row">
                <div class="size-40 bg-white lighten-1 d-flex-xy-ctr col-md-2 ">
                    <i class="zmdi zmdi-format-color-fill text-pink zmdi-hc-2x d-none d-md-block"></i>
                </div>
                <div class="col-xs-12 col-sm-12 d-block d-md-none text-center mb-4">
                  <div class="size-40 bg-white lighten-1 d-flex-xy-ctr m-auto">
                      <i class="zmdi zmdi-format-color-fill text-pink zmdi-hc-4x"></i>
                  </div>
                </div>
                <div class="col-md-10">
                  <h3 class="font-weight-semibold text-pink">Choose your personal theme with your favourite colours</h2>
                    <p class="dash-details text-muted">Don't like purple on white? Hit the "paint" icon in the top bar to change to a new favourite: dark with pink, white with green, amber, light blue, or if you're feeling experimental: night-mode.</p>
                  </div>
                </div>
            </article>

            <article class="col-md-4">
              <div class="row">
                <div class="size-40 bg-white lighten-1 d-flex-xy-ctr col-md-2">
                    <i class="zmdi zmdi-book text-amber zmdi-hc-2x d-none d-md-block"></i>
                </div>
                <div class="col-xs-12 col-sm-12 d-block d-md-none text-center mb-4">
                  <div class="size-40 bg-white lighten-1 d-flex-xy-ctr m-auto">
                      <i class="zmdi zmdi-book text-amber zmdi-hc-4x"></i>
                  </div>
                </div>
                <div class="col-md-10">
                  <h3 class="font-weight-semibold text-amber">Speed-read with concise chapter-by-chapter summaries</h2>
                  <p class="dash-details text-muted">Every one of the 1,189 chapters of the bible has its own imported <a class="text-amber" style="color: #ffc107;" target="_blank" href="https://www.biblestudytools.com/concordances/treasury-of-scripture-knowledge/">TSK</a> summary to outline the content of the text. Instantly speed-read any book before you dive into deeper study of the individual verses. </p>
                </div>
              </div>
            </article>

            <article class="col-md-4">
              <div class="row">
                <div class="size-40 bg-white lighten-1 d-flex-xy-ctr col-md-2">
                    <i class="zmdi zmdi-code-setting text-cyan zmdi-hc-2x d-none d-md-block"></i>
                </div>
                <div class="col-xs-12 col-sm-12 d-block d-md-none text-center mb-4">
                  <div class="size-40 bg-white lighten-1 d-flex-xy-ctr m-auto">
                      <i class="zmdi zmdi-code-setting text-cyan zmdi-hc-4x"></i>
                  </div>
                </div>
                <div class="col-md-10">
                  <h3 class="font-weight-semibold text-cyan">Connect your app or site to the JSON REST API</h2>
                  <p class="dash-details text-muted">RocketBible can talk to any system which understands <a class="text-cyan" style="color: #17a2b8;" target="_blank" href="https://oauth.net/2/">OAuth</a> & <a style="color: #17a2b8;" target="_blank" href="https://jsonapi.org/">JSON</a>, or <a style="color: #17a2b8;" target="_blank" href="https://www.w3.org/OWL/">OWL</a>/<a style="color: #17a2b8;" target="_blank" href="https://www.w3.org/RDF/">RDF Linked Data</a>. Each part of the site is broken down into logical REST URLs you can request and integrate.</p>
                </div>
              </div>
            </article>

          </section>

          <hr class="d-none d-sm-block" />

          <aside class="row mt-4 ml-1">
            <h4 class="font-weight-semibold text-dark">Like RocketBible? Check out these other amazing projects which were critical to its development: </h4>
            <p>
              <a class="text-dark" target="_blank" href="">BibleGateway</a>,
              <a class="text-dark" target="_blank" href="https://viz.bible/">Viz.bible</a>,
              <a class="text-dark" target="_blank" href="http://areopage.net/blog/wp-content/uploads/2013/08/TSKe.pdf">Treasury of Scripture Knowledge (PDF)</a>,
              <a class="text-dark" target="_blank" href="https://www.digitalbibleplatform.com/">Digital Bible Platform</a>,
              <a class="text-dark" target="_blank" href="https://scripture.api.bible/">Scripture.API.bible</a>,
              <a class="text-dark" target="_blank" href="http://www.hackathon.bible/">Hackathon.bible</a>,
              <a class="text-dark" target="_blank" href="https://bible-api.com/">Bible API</a>.
            </p>
          </aside>

  </div>
@stop
