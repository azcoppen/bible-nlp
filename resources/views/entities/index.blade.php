@extends ('layouts.themes.mouldify.master')

@section ('sharing')
  @include ('layouts.themes.mouldify.partials.sharing', [
    'title' => (request()->has('q') ? '&quot;'.request()->get('q').'&quot; Search -' : '') .(isset($type) ? entity_label($type) : '') . (isset($az) ? ' ('.$az.')' : ' - Top 100 - ').' Named Biblical Entities | RocketBible.com',
    'info'  => '',
  ])
@stop

@push ('head')
  <style>
  .badge-ext {
    width: 35px !important;
    padding: 5px 5px !important;
  }
  </style>
@endpush

@section ('content')
  <div class="page-heading">
      <h1 class="title">Entities
        @if ( isset ($type) && $type != 'all' )
          <span class="font-weight-light"> / {{entity_label($type)}} </span>
          @if ( $az )
            <span class="font-weight-light"> / {{$az}}</span>
            @if ( request()->has('q') )
              <span class="font-weight-light"> / <span class="text-secondary"> {{request()->get('q')}} </span></span>
            @endif

          @else
            @if ( request()->has('q') )
              <span class="font-weight-light"> / <span class="text-secondary"> {{request()->get('q')}} </span></span>
            @else
              <span class="font-weight-light">   / </span>
            <span class="font-weight-light text-secondary"> Top 100 </span>
            @endif
          @endif
        @else
          @if ( request()->has('q') )
            <span class="font-weight-light"> / <span class="text-secondary"> {{request()->get('q')}} </span></span>
          @else
            <span class="font-weight-light"> / <span class="text-secondary"> Top 100 </span></span>
          @endif
        @endif
      </h1>
  </div>

  <div class="row">
    <div class="col-lg-12 text-center m-b-10">
      @include ('layouts.themes.mouldify.partials.az_btns_entities', [
        'route'     => 'entities.az',
        'selected'  => $az ?? '',
        'type'      => $type ?? 'person',
        'reset'     => isset($type) ? route ('entities.type', $type) : route('entities.index'),
      ])
    </div>
    <div class="col-lg-12 text-center mt-1">
      @include ('layouts.themes.mouldify.partials.inline_search', [
          'text'      => isset ($az) && !empty ($az) ? "Search all entities beginning with " . $az : "Search all available entities"
      ])
    </div>
  </div>


  <div class="row">
    <div class="col-lg-12">
          <div class="gx-card gx-card-full-height">
            <div class="card-header">
              @include ('layouts.themes.mouldify.partials.entity_tabs', ['type'=>$type, 'az' => $az])
            </div>

            <div class="mt-2" role="group">
              @if ( isset($entities) && count ($entities) )
                @foreach ($entities AS $entity )
                    <a class="gx-btn gx-btn-{{config ('app.entity_btns.'.strtolower($entity->type))}} col-md-3" href="{{route ('entities.show', [strtolower($entity->type), $entity->az, $entity->slug])}}">
                      {{title_case($entity->name)}}
                      <small class="mb-0 badge badge-light badge-ext float-right">{{$entity->refs_total}}</small>
                    </a>
                @endforeach
              @else
                <div class="col-md-12">
                  @include ('layouts.themes.mouldify.partials.empty', ['text' => 'No entities found'])
                </div>
              @endif

              @if ( !request()->has('q') )
              <div class="row mt-3">
                <div class="col-md-6 m-auto">
                  {!! $entities->links () !!}
                </div>
              </div>
              @endif
              @include ('layouts.themes.mouldify.partials.entity_disclaimer')

            </div>

          </div> <!-- end gx card -->
    </div> <!-- end col -->
  </div>

@stop
