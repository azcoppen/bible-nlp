@extends ('layouts.themes.mouldify.master')

@section ('sharing')
  @include ('layouts.themes.mouldify.partials.sharing', [
    'title' => 'Occurrences of '.title_case ($entity->name) .(isset($type) ? ' ('.entity_label($type).')' : '').' found in .'.$chapter->canonical.' ('.$mentions->count().') - Named Biblical Entities | RocketBible.com',
    'info'  => '',
  ])
<meta property="og:type" content="books.genre">
<meta property="books:canonical_name" content="{{$book->title}}">
<meta property="books:book" content="{{htmlentities($book->summary)}}">
@stop

@section ('content')

  <div class="page-heading">
    <div class="row d-block d-md-none mb-2">
      <div class="col-sm-12 col-xs-12 text-right float-right">
        <span class="mr-4 badge badge-secondary">{{$mentions->count()}}</span>
      </div>
    </div>
    <div class="row clearfix" style="clear:both;">
      <h1 class="title col-xs-12 col-sm-12 pl-3 pr-2 text-center">
        Named Entities
          <span class="font-weight-light"> / {{entity_label($type)}} / <span class="text-secondary">{{title_case($entity->name)}}</span> / </span> {{$chapter->canonical}}
          <span class="mr-2 badge badge-secondary float-right d-none d-sm-block">{{$mentions->count()}}</span>
      </h1>
    </div>
  </div>


  <div class="row">
    <div class="col-lg-12">
      <div class="gx-card">
        <p class="text-muted lead">{{$book->summary}}</p>
        <p class="font-weight-light ml-3"><i class="zmdi zmdi-long-arrow-right zmdi-hc-fw"></i>
          <a class="mb-0 mr-2 badge badge-warning" href="{{route('chapters.show', [$book->slug, $chapter->number])}}">{{$chapter->number}}</a>
           {!! fix_utf8_apos($chapter->summaries->first()->text['en'] ?? '') !!}</p>
        <hr style="margin-top: 30px; margin-bottom: 30px;" />

        <div style="max-height: 150px;" id="book-group-{{$book->order}}" class="ct-perfect-fourth book-group-graph ml-0" data-labels="{{json_encode(range(1, $chapter->verses->count()))}}" data-values="{!! json_encode ([chapter_values_map($chapter->verses->count(), $verse_count->all())]) !!}"></div>


            <div class="row mt-0 mb-3">
              <div class="col-md-7 pl-2">
                @if (count ($mentions) )
                  <table class="table table-sm col-md-6 col-md-offset-3 ml-3">
                    <thead>
                      <tr>
                        <th class="text-center">Verse</th>
                        <th class="text-center">Count </th>
                        <th class="text-center">Context </th>
                      </tr>
                    </thead>
                    <tbody>
                    @foreach ($chapter->verses->sortBy('number') AS $verse)
                      @if ( $mentions->contains ('reference_id', $verse->id) )
                      <tr>
                        <td class="text-center">
                        {{$verse->number}}
                        </td>
                        <td class="text-center">
                          <span class="badge badge-light">{{$mentions->where('reference_id', $verse->id)->first()->mentions}}</span>
                        </td>
                        <td>
                          <a class="ref-verse" href="{{route ('verses.show', [$verse->book_slug, $verse->chap_num, $verse->number])}}">
                             {!! searchword( $verse->text['en'], [$entity->name], 'verse-100') !!}
                          </a>
                        </td>
                      </tr>
                      @endif
                    @endforeach
                    </tbody>
                  </table>
                @else
                  @include ('layouts.themes.mouldify.partials.empty', ['text' => 'No results found.'])
                @endif
              </div>
              <div class="col-md-5" style="padding-top: 4rem;">

                <canvas id="chapter-radar-{{$chapter->number}}" class="chapter-radar-graph mt-3 m-auto" data-labels="{{json_encode(range(1, $chapter->verses->count()))}}"
                  data-values="{!! json_encode (chapter_values_map($chapter->verses->count(),  $verse_count->all())) !!}">
                </canvas>

              </div>
            </div>

        @include ('layouts.themes.mouldify.partials.entity_disclaimer')
    </div>
  </div>
</div>


@stop
