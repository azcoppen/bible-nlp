@extends ('layouts.themes.mouldify.master')

@section ('sharing')
  @include ('layouts.themes.mouldify.partials.sharing', [
    'title' => title_case ($entity->name) . (isset($type) ? ' ('.entity_label($type).')' :'').' - Named Biblical Entities | RocketBible.com',
    'info'  => '',
  ])
@stop

@section ('content')
  <div class="page-heading">
      <h1 class="title">
        Named Entities
          <span class="font-weight-light"> / {{entity_label($type)}} / <span class="text-secondary">{{title_case($entity->name)}}</span> </span>
          <span class="mr-2 badge badge-secondary float-right">{{$entity->refs_total}}</span>
      </h1>
  </div>

  <div class="row mb-3">
    <div style="max-height: 250px;" id="book-group-100" class="ct-perfect-fourth bible-graph entity-group-graph" data-labels='{!! json_encode($BOOKS->sortBy('order')->pluck('abbrev')->all()) !!}' data-values="{!! json_encode ([chapter_values_map($BOOKS->count(), $bible->all())]) !!}"></div>
  </div>

  <div class="row">
    <div class="col-lg-12">
      <div class="gx-card">
        @if ( isset($chapters) && count ($chapters) )
          @foreach ($chapters->groupBy('book_num') AS $book_num => $book_group)
            <h3 class="font-weight-semibold clearfix mt-3">
              <a href="{{route ('entities.book', [$type, $az, $entity->slug, $book_group->first()->book_slug])}}">
                {{$book_group->first()->book_title}}
              </a>
              <small class="mr-2 badge badge-secondary ml-2">{{$book_group->unique('chap_num')->sum('mentions')}}</small>
            </h3>

            <p class="text-muted">{{$BOOKS->where('order', $book_num)->first()->summary}}</p>

            <div class="row mb-3">
              <div style="max-height: 150px;" id="book-group-{{$book_num}}" class="ct-perfect-fourth entity-group-graph" data-labels="{{json_encode(range(1, $BOOKS->where('order', $book_num)->first()->chaps))}}" data-values="{!! json_encode ([chapter_values_map($BOOKS->where('order', $book_num)->first()->chaps, $book_group->sortBy ('chap_num')->pluck('mentions', 'chap_num')->all())]) !!}"></div>
            </div>

            <div class="row mt-0 mb-3">
              <div class="col-md-6 pl-2">
                @if (count ($book_group) )
                  <table class="table table-sm col-md-6 col-md-offset-3 ml-3">
                    <thead>
                      <tr>
                        <th class="text-center">Chapter</th>
                        <th class="text-center">Mentions </th>
                        <th class="text-center">Verses </th>
                      </tr>
                    </thead>
                    <tbody>
                    @foreach ($book_group->sortBy ('chap_num')->unique('chap_num') AS $chap_entity)
                    <tr>
                      <td class="text-center">
                        <a class="ml-2 mb-0 badge badge-warning" href="{{route ('entities.chapter', [$type, $az, $entity->slug, $chap_entity->book_slug, $chap_entity->chap_num])}}">{{$chap_entity->chap_num}}</a>
                      </td>
                      <td class="text-center">
                        <span class="badge badge-light">{{$chap_entity->mentions}}</span>
                      </td>
                      <td class="">
                        @if ( isset($chap_entity->verse_nums) )
                          <small>{{$chap_entity->verse_numbers_list ?? '_none_'}}</small>
                        @endif
                      </td>
                    </tr>
                    @endforeach
                    </tbody>
                  </table>
                @else
                  @include ('layouts.themes.mouldify.partials.empty', ['text' => 'No results found.'])
                @endif
              </div>
              <div class="col-md-6 pt-3">

                <div class="row">
                  <canvas id="chapter-radar-{{$book_num}}" class="chapter-radar-graph mt-3 m-auto" data-labels="{{json_encode(range(1, $BOOKS->where('order', $book_num)->first()->chaps))}}" data-values="{!! json_encode (chapter_values_map($BOOKS->where('order', $book_num)->first()->chaps, $book_group->sortBy ('chap_num')->pluck('mentions', 'chap_num')->all())) !!}"></canvas>
                </div>

              </div>
            </div>
            <hr style="margin-bottom: 30px;" />
          @endforeach

          <div class="row mt-0 mb-3">
            <div class="col-md-3 col-md-offset-2 m-auto">
              {!! $chapters->links() !!}
            </div>
          </div>

          @include ('layouts.themes.mouldify.partials.entity_disclaimer')


        @else
          @include ('layouts.themes.mouldify.partials.empty', ['text' => 'No results found in any book.'])
        @endif

    </div>
  </div>
</div>


@stop

@push ('js')
  <script>
  window.book_graphs = [];

  $( document ).ready(function() {
    $( ".entity-group-graph" ).each(function( index ) {
      window.book_graphs[index] = new Chartist.Line('#'+$(this).attr('id'), {
        labels: JSON.parse($(this).attr('data-labels')),
          series: JSON.parse($(this).attr('data-values'))
      }, {
          fullWidth: true,
          axisY: {
            onlyInteger: true
          },
          showArea: true
      });
    });

    $( ".chapter-radar-graph" ).each(function( index ) {

        var ctx = document.getElementById($(this).attr('id')).getContext("2d");
        var radarChart = new Chart(ctx, {
            type: 'radar',
            data: {
                labels: JSON.parse($(this).attr('data-labels')),
                datasets: [
                    {
                        label: "Chapter Spread",
                        backgroundColor: "rgba(200,0,0,0.6)",
                        fillColor: "#ADF7C9",
                        strokeColor: "#93E399",
                        pointColor: "#93E399",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: JSON.parse($(this).attr('data-values'))
                    }
                ]
            },
            options: {
                legend: {
                  display: false,
                },
                scaleShowLine: true,
                angleShowLineOut: true,
                scaleShowLabels: false,
                scaleBeginAtZero: true,
                angleLineColor: "rgba(0,0,0,.1)",
                angleLineWidth: 1,
                pointLabelFontFamily: "'Arial'",
                pointLabelFontStyle: "normal",
                pointLabelFontSize: 10,
                pointLabelFontColor: "#666",
                pointDot: true,
                pointDotRadius: 3,
                pointDotStrokeWidth: 1,
                pointHitDetectionRadius: 20,
                datasetStroke: true,
                datasetStrokeWidth: 2,
                datasetFill: true,
                responsive: true,
            }
        });
    });

    $( ".chapter-gauge-graph" ).each(function( index ) {
          var gaugeChart = new Chartist.Pie('#'+$(this).attr('id'), {
          series: JSON.parse($(this).attr('data-values'))
      }, {
          donut: true,
          donutWidth: 60,
          startAngle: 270,
          total: parseInt($(this).attr('data-total')),
          showLabel: false
      });
    });




  });

  </script>
@endpush
