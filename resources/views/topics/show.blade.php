@extends ('layouts.themes.mouldify.master')

@section ('sharing')
  @include ('layouts.themes.mouldify.partials.sharing', [
    'title' => (request()->has('q') ?' &quot;'.request()->get('q').'&quot; Search -' : '') . $topic->title . (isset($subject) && $subject ? ' / '.fix_utf8($subject->subject) : ' - ') .' Biblical Topics | RocketBible.com',
    'info'  => 'Full breakdown of all scriptural verses categorised under '.$topic->title . (isset($subject) && $subject ? ' / '.fix_utf8($subject->subject) : '') ,
  ])
@stop

@section ('content')
  <div class="page-heading">
      <h1 class="title">
        <span class="font-weight-light">Biblical Topics / </span>
        @if ( $subject )
          <a href="{{route ('topics.show', [$az, $topic->slug])}}">
        @endif
        {{$topic->title}}
        @if ( $subject )
</a><span class="font-weight-light"> / <span class="text-secondary">{{fix_utf8($subject->subject)}}</span></span>
        @endif

      </h1>
  </div>

  <div class="row">
    <div class="col-lg-12 text-center m-b-10">
      @include ('layouts.themes.mouldify.partials.az_btns', [
        'route'     => 'topics.az',
        'selected'  => $az ?? '',
        'reset'     => 'topics.index',
      ])
    </div>
    <div class="col-lg-12 text-center mt-1">
      @include ('layouts.themes.mouldify.partials.inline_search', [
          'text'      => isset ($az) && !empty ($az) ? "Search all topics beginning with " . $az : "Search all available topics"
      ])
    </div>
  </div>

  <div style="max-height: 250px;" id="book-group-100" class="ct-perfect-fourth bible-graph topic-group-graph" data-labels='{!! json_encode($BOOKS->sortBy('order')->pluck('abbrev')->all()) !!}' data-values="{!! json_encode ([chapter_values_map($BOOKS->count(), $bible->all())]) !!}"></div>

  <div class="row mt-3">
    <div class="col-lg-8 col-sm-12">
    @if ( isset ($refs) && count ($refs) )

      @foreach ( $refs->groupBy ('subject') AS $subj => $ref_group )

          <article class="gx-card">
            <div class="gx-card-header">
              <h2>
                <a href="{{route ('topics.subject', [$az, $topic->slug, str_slug($subj)])}}">
                  {{fix_utf8($subj)}}
                </a>
              </h2>
            </div>

            <ul class="list-unstyled ml-0">
              @if ( count ($ref_group) )
                @foreach ($ref_group->sortBy('book_num')->groupBy('book_num') AS $book_num => $book_group )
                  <h3 class="font-weight-semibold clearfix">
                    {{$book_group->first()->book_title}}
                  </h3>


                  <p class="text-muted">{{$BOOKS->where('order', $book_num)->first()->summary}}</p>

                  <div class="row mb-3">
                    <div style="max-height: 150px;" id="book-group-{{str_slug($subj)}}-{{$book_num}}" class="ct-perfect-fourth topic-group-graph" data-labels='{!! json_encode(range(1, $BOOKS->where('order', $book_num)->first()->chaps)) !!}' data-values="{!! json_encode ([chapter_values_map($BOOKS->where('order', $book_num)->first()->chaps, $subject_span->get(str_slug($subj))->get($book_num)->all())]) !!}"></div>
                  </div>

                  <hr style="margin-top: 30px; margin-bottom: 30px;" />

                  <div class="row">
                  <ul class="list-unstyled ml-1 clearfix">
                    @foreach ( $book_group->sortBy('chap_num')->groupBy('chap_num') AS $chapter_group )
                      <li class="mb-2 mt-3">
                      <h3>
                        <i class="zmdi zmdi-long-arrow-right zmdi-hc-fw"></i>
                        <a class="mb-0 mr-2 badge badge-warning" href="{{route('chapters.show', [$chapter_group->first()->book_slug, $chapter_group->first()->chap_num])}}">{{$chapter_group->first()->chap_num}}</a>
                        <small class="font-weight-light text-muted">{!! fix_utf8_apos($summaries->get($book_num)->where('chap_num', $chapter_group->first()->chap_num)->first()->text['en'] ?? '') !!}</small>
                      </h3>
                        <table class="table ml-2 mt-1">
                        @foreach ($chapter_group->sortBy('verse.number') AS $ref)
                          <tr class="ml-2 mb-2">
                            <td class="center">
                              <a class="ref-verse" href="{{route ('verses.show', [$ref->book_slug, $ref->chap_num, $ref->verse->number])}}">
                                <small class="mr-2 badge badge-light">{{$ref->verse->number}}</small>
                              </a>
                            </td>
                            <td>
                              <a class="ref-verse" href="{{route ('verses.show', [$ref->book_slug, $ref->chap_num, $ref->verse->number])}}">
                                 {{$ref->ref_full_text}}
                              </a>
                            </td>
                          </tr>
                        @endforeach
                        </table>
                      </li>
                      <hr style="margin-top: 30px; margin-bottom: 30px;" />
                    @endforeach
                  </ul>
                  </div>



                @endforeach
              @else
                @include ('layouts.themes.mouldify.partials.empty', ['text' => 'No references found'])
              @endif
            </ul>

          </article>

      @endforeach
      </div>

      <div class="col-md-4 col-sm-12">
        @include ('layouts.themes.mouldify.partials.grouped_topic_list', ['topics'=>$topic->topic_refs->sortBy('title')->groupBy ('title')])
      </div>
      <div class="row text-center">
        <div class="col-md-6 col-md-offset-3 text-center m-auto">
        {!! $refs->links () !!}
        </div>
      </div>


    @else
      @include ('layouts.themes.mouldify.partials.empty', ['text' => 'No topic references found'])
    @endif
  </div>




@stop
