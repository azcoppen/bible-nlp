@extends ('layouts.themes.mouldify.master')

@section ('sharing')
  @include ('layouts.themes.mouldify.partials.sharing', [
    'title' => (request()->has('q') ? '&quot;' . request()->get('q') .'&quot; Search - ' : '') .' Biblical Topics '. (isset($az) ? '('.$az.')' : 'Top 100' ).' | RocketBible.com',
    'info'  => 'Search over 4900+ Biblial scriptural topics for every subject in the Bible.',
  ])
@stop

@section ('content')
  <div class="page-heading">
      <h1 class="title">Biblical Topics
        @if ( $az )
          <span class="font-weight-light"> / {{$az}} </span>
          @if ( request()->has('q') )
            <span class="font-weight-light"> / <span class="text-secondary"> {{request()->get('q')}} </span></span>
          @endif
        @else
          @if ( request()->has('q') )
            <span class="font-weight-light"> / <span class="text-secondary"> {{request()->get('q')}} </span></span>
          @else
            <span class="font-weight-light"> / <span class="text-secondary"> Top 100 </span></span>
          @endif
        @endif
      </h1>
  </div>

  <div class="row">
    <div class="col-lg-12 text-center m-b-10">
      @include ('layouts.themes.mouldify.partials.az_btns', [
        'route'     => 'topics.az',
        'selected'  => $az ?? '',
        'reset'     => 'topics.index',
      ])
    </div>
    <div class="col-lg-12 text-center mt-1">
      @include ('layouts.themes.mouldify.partials.inline_search', [
          'text'      => isset ($az) && !empty ($az) ? "Search all topics beginning with " . $az : "Search all available topics"
      ])
    </div>
  </div>


  <div class="row">

    @if ( isset($topics) && count ($topics) )


        @if ( request()->has('q') )
          <div class="col-md-12">
            <article class="gx-card mt-3">
              @foreach ( $topics AS $topic)
              <div class="col-md-12">
                @if ( $topic->root == 1)
                  <a href="{{route('topics.show', [$topic->az, str_slug($topic->root_slug)])}}">
                    <h3>{{$topic->root_title}}</h3>
                  </a>
                @else
                  <a href="{{route('topics.subject', [$topic->az, str_slug($topic->root_slug), $topic->subject_slug])}}">
                  <h3>{{fix_utf8($topic->root_title)}}<span class="font-weight-light">, {{fix_utf8($topic->subject_title)}}</span></h3>
                  </a>
                @endif
              </div>
              @endforeach
            </article>
          </div>
      @else

        @foreach ( $topics AS $topic)
        <div class="card col-md-4 p-4">
            <p class="text-uppercase letter-spacing-base mb-2 font-weight-bold">
              <a title="" href="{{route('topics.show', [$topic->az, $topic->slug])}}">
                {{str_limit($topic->title, 20)}}
              </a>
              @if ( $topic->refs_total > 0 )
                <small class="ml-2 mb-0 badge badge-light float-right">{{$topic->refs_total}}</small>
              @endif
            </p>
            @if (count ($topic->subjects))
            <table class="table table-sm ml-2 mt-2">
              <tbody>
              @if ( isset($top) )
                @foreach (collect($topic->subjects)->take(5) AS $ref)
                  <tr><td class="pl-2 font-weight-light"><a href="{{route('topics.subject', [$topic->az, $topic->slug, str_slug($ref)])}}">{{fix_utf8($ref)}}</a></td></tr>
                @endforeach
              @else
                @foreach ($topic->subjects AS $ref)
                  <tr><td class="pl-3 font-weight-light"><a href="{{route('topics.subject', [$topic->az, $topic->slug, str_slug($ref)])}}">{{fix_utf8($ref)}}</a></td></tr>
                @endforeach
              @endif
              </tbody>
            </table>
              @if ( isset($top) )
                <small class="text-right float-right">
                  <a class="gx-btn gx-btn-sm gx-btn-default" href="{{route('topics.show', [$topic->az, $topic->slug])}}">MORE</a>
                </small>
              @endif
            @endif
        </div>
        @endforeach

      @endif



    @else
      <div class="col-md-12">
        @include ('layouts.themes.mouldify.partials.empty', ['text' => 'No topics found'])
      </div>
    @endif
</div>

@if ( isset($topics) && count ($topics) && !request()->has('q') )
<div class="row text-center">
  <div class="col-md-6 m-auto text-center col-md-offset-3">
  {!! $topics->links () !!}
  </div>
</div>
@endif

@stop
