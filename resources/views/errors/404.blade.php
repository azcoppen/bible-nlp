@extends ('layouts.themes.mouldify.master')

@section ('sharing')
  @include ('layouts.themes.mouldify.partials.sharing', [
    'title' => '404 Page Not Found',
    'info'  => 'The page you requested does not exist.',
  ])
@stop

@section ('content')
  <div class="page-error-container animated slideInUpTiny animation-duration-3">
      <div class="page-error-content">
          <div class="error-code mb-4 animated zoomInDown">404</div>
          <h2 class="text-center fw-regular title bounceIn animation-delay-10 animated">
              Err, so, that doesn't exist. Awkward.
          </h2>
      </div>
  </div>

@stop
