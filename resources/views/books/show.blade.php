@extends ('layouts.themes.mouldify.master')

@section ('sharing')
  @include ('layouts.themes.mouldify.partials.sharing', [
    'title' => $book->title. ' - '. ($book->volume == 'OT' ? 'Old Testament' : 'New Testament') . ' | RocketBible.com',
    'info'  => htmlentities($book->summary)
  ])
<meta property="og:type" content="books.genre">
<meta property="books:canonical_name" content="{{$book->title}}">
<meta property="books:book" content="{{htmlentities($book->summary)}}">
@stop

@section ('content')

  <div class="page-heading" style="margin-bottom: 0px;">
    <div class="col-md-12 col-sm-12">
      <h1 class="badge badge-secondary float-right" style="margin-bottom: 0px; display: inline-block;">{{$book->percent}}%</h1>
      <h2 class="mr-2 font-weight-light float-left">{{($book->volume == 'OT' ? 'Old Testament' : 'New Testament')}} / <time class="text-secondary">{{$book->human_dating}}</time>
      </h2>
    </div>

    <hr class="clearfix mt-4" />

      <h1 class="title col-md-6 col-sm-12">
           {{$book->title}} <span class="font-weight-light">({{$book->abbrev}})</span>
      </h1>
      <summary class="lead font-weight-light mb-3 col-sm-12">{{$book->summary}} </summary>
  </div>

  <div class="row mt-0 mb-3">
    <div class="col-sm-12 mt-0">
      {!! Form::open (['route' => ['books.search', $book->slug], 'method' => 'GET', 'class'=>'form-horizontal']) !!}
      <div class="input-group" style="margin-top: -5px !important;">
          <input id="q" name="q" type="text" class="form-control" required placeholder="Search all of {{$book->title}}" aria-label="Search {{$book->title}}" value="{{request()->input('q')}}">
          <div class="input-group-append">
              <button class="btn btn-default" type="submit"><i class="zmdi zmdi-search"></i></button>
          </div>
      </div>

      {!! Form::close () !!}
    </div>
  </div>

  <div class="row mb-3">
    <div style="max-height: 150px;" id="book-group-100" class="ct-perfect-fourth book-count-graph" data-labels='{!! json_encode($BOOKS->sortBy('order')->pluck('abbrev')->all()) !!}' data-values="{!! json_encode ([$BOOKS->sortBy('order')->pluck('num_words')->all()]) !!}"></div>
  </div>

  <div class="row">
      <div class="col-xl-3 col-sm-6 col-12">
          <div class="gx-card">
              <div class="row align-items-center no-gutters">
                  <div class="col-5">
                      <h1 class="chart-f30 font-weight-light mb-1">{{$book->chaps}}</h1>
                      <span class="sub-heading">Chapters</span>
                  </div>
                  <div class="col-7 pl-2">
                    <canvas id="doughnut-chart" class="height-100" height="150" data-labels='{!! json_encode (range(1, $book->num_chaps)) !!}' data-values='{!! json_encode (range(1, $book->num_chaps)) !!}'></canvas>


                  </div>

              </div>
          </div>
      </div>
      <div class="col-xl-3 col-sm-6 col-12">
          <div class="gx-card">
              <div class="row align-items-center no-gutters">
                  <div class="col-5">
                      <h1 class="chart-f30 font-weight-light mb-1">{{$book->num_verses}}</h1>
                      <span class="sub-heading"> Verses</span>
                  </div>
                  <div class="col-7 pl-3">
                      <div id="ct-news-chart" class="height-100"></div>
                  </div>
              </div>
          </div>
      </div>
      <div class="col-xl-3 col-sm-6 col-12">
          <div class="gx-card">
              <div class="row align-items-center no-gutters">
                  <div class="col-5">
                      <h1 class="chart-f30 font-weight-light mb-1">{{$book->num_words}}</h1>
                      <span class="sub-heading">Words</span>
                  </div>
                  <div class="col-7 pl-2">
                    <div id="news-ch-chart" class="height-100"></div>
                  </div>
              </div>
          </div>
      </div>
      <div class="col-xl-3 col-sm-6 col-12">
          <div class="gx-card">
              <div class="row align-items-center no-gutters">
                  <div class="col-5">
                      <h1 class="chart-f30 font-weight-light mb-1">{{$book->num_entities ?? 100}}</h1>
                      <span class="sub-heading">Entities</span>
                  </div>
                  <div class="col-7 pl-3 pt-2">
                      <canvas id="pieChart" class="height-100" height="185"></canvas>
                  </div>
              </div>
          </div>
      </div>
  </div>


  <div class="row">
    <div class="col-lg-12">
      <div class="gx-card">
        <h3 class="card-heading">
          <small class="mb-0 badge badge-light badge-ext float-right">{{$book->num_entities}}</small>
            Top Named Entities / <span class="font-weight-light">By Total</span>
        </h3>

        <div class="row">
          <div class="col-md-3">
            <h4 class="text-center mb-2">All</h4>
            @foreach (array_slice(collect($book->top_entities)->collapse()->sort()->reverse()->all(), 0, 10) AS $e => $total)
              <?php $guess = entity_stats_finder ($book->top_entities, $e); ?>
              <a class="gx-btn gx-btn-{{config ('app.entity_btns.'.str_slug($guess))}} col-md-12" href="{{ route ('entities.book', [str_slug($guess), strtoupper(substr($e, 0, 1)), str_slug($e), $book->slug]) }}">
                {{title_case($e)}}
                <small class="mb-0 badge badge-light badge-ext float-right">{{$total}}</small>
              </a>
            @endforeach
          </div> <!-- end col -->
          <div class="col-md-3">
            <h4 class="text-center mb-2">People</h4>
            @if ( isset ($book->top_entities['PERSON']) )
              @foreach (array_slice($book->top_entities['PERSON'], 0, 10) AS $e => $total)
                <a class="gx-btn gx-btn-{{config ('app.entity_btns.person')}} col-md-12" href="{{ route ('entities.book', ['person', strtoupper(substr($e, 0, 1)), str_slug($e), $book->slug]) }}">
                  {{title_case($e)}}
                  <small class="mb-0 badge badge-light badge-ext float-right">{{$total}}</small>
                </a>
              @endforeach
            @endif
          </div> <!-- end col -->
          <div class="col-md-3">
            <h4 class="text-center mb-2">Organizations</h4>
            @if ( isset ($book->top_entities['ORGANIZATION']) )
              @foreach (array_slice($book->top_entities['ORGANIZATION'], 0, 10) AS $e => $total)
                <a class="gx-btn gx-btn-{{config ('app.entity_btns.organization')}} col-md-12" href="{{ route ('entities.book', ['organization', strtoupper(substr($e, 0, 1)), str_slug($e), $book->slug]) }}">
                  {{title_case($e)}}
                  <small class="mb-0 badge badge-light badge-ext float-right">{{$total}}</small>
                </a>
              @endforeach
            @endif
          </div> <!-- end col -->
          <div class="col-md-3">
            <h4 class="text-center mb-2">Locations</h4>
            @if ( isset ($book->top_entities['LOCATION']) )
              @foreach (array_slice($book->top_entities['LOCATION'], 0, 10) AS $e => $total)
                <a class="gx-btn gx-btn-{{config ('app.entity_btns.location')}} col-md-12" href="{{ route ('entities.book', ['location', strtoupper(substr($e, 0, 1)), str_slug($e), $book->slug]) }}">
                  {{title_case($e)}}
                  <small class="mb-0 badge badge-light badge-ext float-right">{{$total}}</small>
                </a>
              @endforeach
            @endif
          </div> <!-- end col -->
        </div>
        <div class="row mt-3">
          <div class="col-md-3">
            <h4 class="text-center mb-2">Events</h4>
            @if ( isset ($book->top_entities['EVENT']) )
              @foreach (array_slice($book->top_entities['EVENT'], 0, 10) AS $e => $total)
                <a class="gx-btn gx-btn-{{config ('app.entity_btns.event')}} col-md-12" href="{{ route ('entities.book', ['event', strtoupper(substr($e, 0, 1)), str_slug($e), $book->slug]) }}">
                  {{title_case($e)}}
                  <small class="mb-0 badge badge-light badge-ext float-right">{{$total}}</small>
                </a>
              @endforeach
            @endif
          </div> <!-- end col -->
          <div class="col-md-3">
            <h4 class="text-center mb-2">Goods</h4>
            @if ( isset ($book->top_entities['CONSUMER_GOOD']) )
              @foreach (array_slice($book->top_entities['CONSUMER_GOOD'], 0, 10) AS $e => $total)
                <a class="gx-btn gx-btn-{{config ('app.entity_btns.consumer_good')}} col-md-12" href="{{ route ('entities.book', ['consumer_good', strtoupper(substr($e, 0, 1)), str_slug($e), $book->slug]) }}">
                  {{title_case($e)}}
                  <small class="mb-0 badge badge-light badge-ext float-right">{{$total}}</small>
                </a>
              @endforeach
            @endif
          </div> <!-- end col -->
          <div class="col-md-3">
            <h4 class="text-center mb-2">Artworks</h4>
            @if ( isset ($book->top_entities['WORK_OF_ART']) )
              @foreach (array_slice($book->top_entities['WORK_OF_ART'], 0, 10) AS $e => $total)
                <a class="gx-btn gx-btn-{{config ('app.entity_btns.work_of_art')}} col-md-12" href="{{ route ('entities.book', ['work_of_art', strtoupper(substr($e, 0, 1)), str_slug($e), $book->slug]) }}">
                  {{title_case($e)}}
                  <small class="mb-0 badge badge-light badge-ext float-right">{{$total}}</small>
                </a>
              @endforeach
            @endif
          </div> <!-- end col -->
          <div class="col-md-3">
            <h4 class="text-center mb-2">Other</h4>
            @if ( isset ($book->top_entities['OTHER']) )
              @foreach (array_slice($book->top_entities['OTHER'], 0, 10) AS $e => $total)
                <a class="gx-btn gx-btn-{{config ('app.entity_btns.other')}} col-md-12" href="{{ route ('entities.book', ['other', strtoupper(substr($e, 0, 1)), str_slug($e), $book->slug]) }}">
                  {{title_case($e)}}
                  <small class="mb-0 badge badge-light badge-ext float-right">{{$total}}</small>
                </a>
              @endforeach
            @endif
          </div> <!-- end col -->
        </div> <!-- end row -->
        @include ('layouts.themes.mouldify.partials.entity_disclaimer')


      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-12">
      <div class="gx-card">
        <h3 class="card-heading">
          <small class="mb-0 badge badge-light badge-ext float-right">{{$book->num_entities}}</small>
            Top Word Frequencies / <span class="font-weight-light">By Total</span>
        </h3>
        <div class="mt-3" role="group">
            @foreach ($book->top_freqs AS $word => $total )
                <a class="gx-btn gx-btn-default col-md-2" href="{{route ('frequencies.book', [strtoupper(substr($word, 0, 1)), $word, $book->slug])}}">
                  {{title_case($word)}}
                  <small class="mb-0 badge badge-light badge-ext float-right">{{$total}}</small>
                </a>
            @endforeach
        </div>
        @include ('layouts.themes.mouldify.partials.frequency_disclaimer')
    </div>
  </div>
</div>

  <div class="row">
    <h1 class="col-md-12 text-center font-weight-light mb-3">Speed-Read {{$book->title}} Chapter Outlines</h1>
        <section class="timeline-section timeline-center clearfix mt-3 ml-2 mr-3">
          @foreach ($book->summaries->sortBy('chap_num') AS $counter => $summary)
          <div class="timeline-item timeline-time-item {{$counter % 2 == 0 ? 'timeline-inverted' : ''}}">
              <div class="timeline-badge bg-amber"><a class="text-white" href="{{route('chapters.show', [$book->slug, $summary->chap_num])}}">{{$summary->chap_num}}</a></div>
              <div class="timeline-panel ">
                  <div class="timeline-panel-header">
                      <header class="timeline-heading">
                        <h3 class="">
                          <a class="" href="{{route('chapters.show', [$book->slug, $summary->chap_num])}}">Chapter {{$summary->chap_num}}</a>
                        </h3>
                      </header>
                  </div>
                  <div class="timeline-body">
                      <summary class="">{!! fix_utf8_apos($summary->text['en']) !!}</summary>
                  </div>
              </div>
          </div>
          @endforeach
        </section>

  </div>


@stop

@push ('js')
<script src="{{ cdn ('js/book_dash.js') }}"></script>
<script>
$(window).on('load', function(){
  console.log('window load');
  window.book_count.on('created', function(context) {
    $('#book-group-100 svg .ct-series line')[{{$book->order -1}}].style.stroke = '#e83e8c';
  });
});
</script>
@endpush
