@extends ('layouts.themes.mouldify.master')

@section ('sharing')
  @include ('layouts.themes.mouldify.partials.sharing', [
    'title' => (isset($volume) && !is_null ($volume) ? ($volume == 'both' ? 'Old & New Testament' : ($volume == 'OT' ? 'Old Testament' : 'New Testament'))  : '') . ' - '. title_case ($ordering) . ' - Books of the Bible | RocketBible.com',
    'info'  => ''
  ])
<meta property="og:type" content="books.genre">
<meta property="books:canonical_name" content="The Bible">
@stop

@push ('head')
  <style>
  .gx-btn-group .gx-btn, .gx-btn-group .gx-fab-btn {
    margin: 0 0px 6px;
}
  </style>
@endpush

@section ('content')

  <nav class="btn-group pull-right d-none d-md-block" style="float: right;">
    <a title="Sort books from shortest to longest" class="gx-btn gx-btn-{{$ordering =='length' ? 'secondary' : 'default'}}" href="{{route('books.index', [$volume ?? 'both', 'length'])}}">Length</a>
    <a title="Sort books in traditional canonical order" class="gx-btn gx-btn-{{$ordering =='canonical' ? 'secondary' : 'default'}}" href="{{route('books.index', [$volume ?? 'both', 'canonical'])}}">Canonical</a>
    <a title="Sort books from 0-9 A-Z" class="gx-btn gx-btn-{{$ordering =='alphabetical' ? 'secondary' : 'default'}}" href="{{route('books.index', [$volume ?? 'both', 'alphabetical'])}}">Alphabetical</a>
    <a title="Sort books from oldest to newest" class="gx-btn gx-btn-{{$ordering =='chronological' ? 'secondary' : 'default'}}" href="{{route('books.index', [$volume ?? 'both', 'chronological'])}}">Chronological</a>
  </nav>

  <nav class="row btn-group pull-right d-block d-md-none pr-3" style="float: right;">
    <a title="Sort books from shortest to longest" class="gx-btn gx-btn-xs gx-btn-{{$ordering =='length' ? 'secondary' : 'default'}}" href="{{route('books.index', [$volume ?? 'both', 'length'])}}">Length</a>
    <a title="Sort books in traditional canonical order" class="gx-btn gx-btn-xs  gx-btn-{{$ordering =='canonical' ? 'secondary' : 'default'}}" href="{{route('books.index', [$volume ?? 'both', 'canonical'])}}">Canonical</a>
    <a title="Sort books from 0-9 A-Z" class="gx-btn gx-btn-xs  gx-btn-{{$ordering =='alphabetical' ? 'secondary' : 'default'}}" href="{{route('books.index', [$volume ?? 'both', 'alphabetical'])}}">Alphabetical</a>
    <a title="Sort books from oldest to newest" class="gx-btn gx-btn-xs  gx-btn-{{$ordering =='chronological' ? 'secondary' : 'default'}}" href="{{route('books.index', [$volume ?? 'both', 'chronological'])}}">Chronological</a>
  </nav>

  <hr class="d-block d-md-none" />

  <div class="page-heading row">
      <h1 class="title d-none d-md-block">
        Books of the Bible
        @if ( isset($volume) && !is_null ($volume) )
           <span class="font-weight-light"> / {{($volume == 'both' ? 'All' : ($volume == 'OT' ? 'Old Testament' : 'New Testament'))}}</span>
        @endif
        <span class="font-weight-light"> / <span class="text-secondary">{{title_case ($ordering)}}</span></span>
      </h1>

      <div class="d-block d-md-none mt-2">
        <h4 class="col-md-6 text-right">
          Books of the Bible
          @if ( isset($volume) && !is_null ($volume) )
             <span class="font-weight-light"> / {{($volume == 'both' ? 'All' : ($volume == 'OT' ? 'Old Testament' : 'New Testament'))}}</span>
          @endif
          <span class="font-weight-light"> / <span class="text-secondary">{{title_case ($ordering)}}</span></span>
        </h4>
      </div>
  </div>


  <section class="timeline-section timeline-center clearfix">
    @foreach ( $books AS $counter => $book )
      <article class="timeline-item timeline-time-item {{$counter % 2 == 0 ? 'timeline-inverted' : ''}}">
          <time class="timeline-time" style="padding-left: 10px; font-size: 95%;">{{$book->human_dating}}</time>

          <div class="timeline-badge bg-pink" style="margin-top: 10px;">
              <i class="zmdi zmdi-book zmdi-hc-lg"></i>
          </div>
          <div class="timeline-panel bg-{{config ('app.book_colours.'.$book->order)}} text-white">
            <div class="row mt-2">
              <small style ="opacity: 0.7;" class="font-weight-light col-sm-12 {{$counter % 2 == 0 ? 'text-right' : 'text-left'}}"><strong>{{$book->percent}}</strong>% / <strong>{{$book->chaps}}</strong> {{str_plural ('chapter', $book->chaps)}} / <strong>{{number_format($book->num_verses, null, 0, ',')}}</strong> {{str_plural ('verse', $book->num_verses)}} / <strong>{{number_format($book->num_words, null, 0, ',')}}</strong> words</small>
            </div>

            <div class="row mt-3">
              <h1 class="timeline-tile col-sm-12"><a class="text-white {{$counter % 2 == 0 ? 'text-left' : 'text-right'}}" title="Overview of {{$book->title}}" href="{{route ('books.show', [$book->slug])}}">{{$book->title}}</a></h1>
            </div>

            <div class="row">
              <summary class="col-sm-12 {{$counter % 2 == 0 ? 'mb-2' : ''}}">{{$book->summary}}</summary>
            </div>

            <div class="row mt-3">
              <div class="gx-btn-group col-sm-12 pl-4">
                @for ( $i=1; $i <=$book->chaps; $i++)
                  <a class="gx-btn gx-btn-xs btn-light {{$counter % 2 == 0 ? '' : ''}}" title="Read {{$book->title}} Chapter {{$i}}" href="{{route ('chapters.show', [$book->slug, $i])}}">{{ str_pad($i, 2, '0', STR_PAD_LEFT)}}</a>
                @endfor
              </div>
            </div>
          </div>
      </article>
    @endforeach
  </section>





@stop
