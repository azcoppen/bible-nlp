@extends ('layouts.themes.mouldify.master')

@section ('sharing')
  @include ('layouts.themes.mouldify.partials.sharing', [
    'title' => (request()->has('q') ? 'Search for &quot;'.request()->input('q').'&quot; : ' : '').$book->title .' | RocketBible',
    'info'  => htmlentities($book->summary)
  ])
<meta property="og:type" content="books.genre">
<meta property="books:canonical_name" content="{{$book->title}}">
<meta property="books:book" content="{{htmlentities($book->summary)}}">
@stop

@section ('head')
  <style>
  .badge:not(:last-child) {
    margin-right: 0px;
  }
  </style>
@stop

@section ('content')

<header class="row">
  <div class="col-sm-12">
    <h1 class="align-middle">{{strtoupper($book->title)}}
      <span class="font-weight-light"> / <span class="text-success">{{request()->input('q', 'No Search Provided')}}</span></span>
      </h1>
  </div>
</header>

<div class="row">
  <div class="col-md-12">
    {!! Form::open (['route' => ['books.search', $book->slug], 'method' => 'GET', 'class'=>'form-horizontal']) !!}
    <div class="input-group">
        <input id="q" name="q" type="text" class="form-control" required placeholder="Search all of {{$book->title}}" aria-label="Search {{$book->title}}" value="{{request()->input('q')}}" >
        <div class="input-group-append">
            <button class="btn btn-default" type="button"><i class="zmdi zmdi-search"></i></button>
        </div>
    </div>

    {!! Form::close () !!}
  </div>
</div>

<div class="row mt-1">
  <div class="col-md-12">

          <article class="gx-card">
            @if ( isset($summaries) && count($summaries) > 0 )
            <h3 class="card-heading">
              <small class="mb-0 badge badge-light badge-ext float-right">{{count($summaries)}}</small>
                Chapter Outline Results / <span class="font-weight-light">By Relevance</span>
            </h3>

            <div class="row mb-3">
              <div style="max-height: 150px;" id="chapter-group-100" class="ct-perfect-fourth bible-graph search-group-graph" data-labels='{!! json_encode(range (1, $book->chaps)) !!}' data-values="{!! json_encode ([chapter_values_map($book->chaps, $graph_chapters->all())]) !!}"></div>
            </div>

            <div class="table-responsive">
              <table class="table table-sm">
                <thead>
                  <tr>
                    <th>Book</th>
                    <th>Chapter</th>
                    <th>Summary</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ( $summaries AS $summary )
                    <tr class="mb-2">
                      <td nowrap>
                        <a href="{{route ('books.show', $summary->book->slug)}}">
                        {{$summary->book->title}}
                        </a>
                      </td>
                      <td class="text-center" nowrap><a class="ml-0 mb-0 badge badge-warning" href="{{route('chapters.show', [$summary->book->slug, $summary->chap_num])}}">{{$summary->chap_num}}</a></td>
                      <td class="font-weight-light">
                          {!! searchword( fix_utf8_apos($summary->text['en']), str_word_count(request()->input ('q'), 1), 'verse-100') !!}
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>

            <hr class="mt-3" style="margin-bottom: 3rem;" />

            @endif


            @if ( isset($verses) && count($verses) > 0 )
            <h3 class="card-heading mb-3">
              <small class="mb-0 badge badge-light badge-ext float-right">{{count($verses)}}</small>
                Verse Text Results / <span class="font-weight-light">By Relevance</span>
            </h3>

            <div class="row mt-3 mb-3">
              <div style="max-height: 150px;" id="verse-group-100" class="ct-perfect-fourth bible-graph search-group-graph" data-labels='{!! json_encode(range (1, $book->chaps)) !!}' data-values="{!! json_encode ([chapter_values_map($book->chaps, $graph_verses->all())]) !!}"></div>
            </div>

              <div class="table-responsive">
                <table class="table table-sm">
                  <thead>
                    <tr>
                      <th>Book</th>
                      <th>Chapter</th>
                      <th>Verse</th>
                      <th>Text (Contextless)</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ( $verses AS $verse )
                      <tr class="mb-2">
                        <td nowrap class="">
                          <a href="{{route ('books.show', $verse->book_slug)}}">
                          {{$verse->book_title}}
                          </a>
                        </td>
                        <td nowrap class="text-center"><a class="ml-0 mb-0 badge badge-warning" href="{{route('chapters.show', [$verse->book_slug, $verse->chap_num])}}">{{$verse->chap_num}}</a></td>
                        <td nowrap class="text-center">{{$verse->number}}</td>
                        <td>
                          <a rel="{{$verse->number}}" class="ref-verse font-weight-semibold" data-toggle="tooltip" data-placement="right" title="{{$verse->extended}}" href="{{route ('verses.show', [$verse->book_slug, $verse->chap_num, $verse->number])}}">
                            {!! searchword( $verse->text['en'], str_word_count(request()->input ('q'), 1), 'verse-100') !!}
                          </a>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>

            @else
              @include ('layouts.themes.mouldify.partials.empty', ['text' => 'No results found'])
            @endif
          </article>
  </div>
</div>


@stop
