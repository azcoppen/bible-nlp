$(document).ready(function() {
    $('pre code').each(function(i, block) {
        hljs.highlightBlock(block);
    });

    $('.btn-fixed').click(function(){
        $('.right-sidebar').toggleClass('active');
        $(this).toggleClass('active');
    });

    $('.document-menus a').each(function (index) {
        var $anchor = $(this);
        var $template = $anchor.data('template');
        var $parent = $anchor.parent();

        $anchor.on('click', function (event) {
            event.preventDefault();

            $('.document-menus li').removeClass('active');
            $parent.addClass('active');
            $('.docs-section').load('templates/'+$template+'.html')
        })
    })
});