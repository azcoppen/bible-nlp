<?php

use Illuminate\Database\Seeder;
use BibleNLP\Repositories\BookRepository;

class BooksCollectionSeeder extends Seeder
{
    private $repository;

    public function __construct () {
      $this->repository = app (BookRepository::class);
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csv = array_map('str_getcsv', file(database_path('data/books.csv')));
        foreach ($csv as $index => $data) {
            $this->repository->create ([
              'code'      => 'NIV'.str_pad (($index + 1), 3, '0', STR_PAD_LEFT) .'-' . strtoupper (str_slug ($data[2])),
              'volume'    => $data[4],
              'abbrev'    => $data[1],
              'title'     => $data[2],
              'slug'      => str_slug ($data[2]),
              'chaps'     => intval ($data[3])
            ]);
        }
    }
}
