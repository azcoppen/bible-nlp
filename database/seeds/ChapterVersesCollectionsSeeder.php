<?php

use Illuminate\Database\Seeder;
use BibleNLP\Repositories\BookRepository;
use BibleNLP\Repositories\ChapterRepository;
use BibleNLP\Repositories\VerseRepository;

use BibleNLP\Contracts\Services\WordFrequencyContract;

class ChapterVersesCollectionsSeeder extends Seeder
{

    private $book_repository;
    private $chapter_repository;
    private $verse_repository;

    public function __construct () {
      $this->book_repository = app (BookRepository::class);
      $this->chapter_repository = app (ChapterRepository::class);
      $this->verse_repository = app (VerseRepository::class);
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bible = $this->book_repository->all ();
        foreach ( $bible AS $index => $book ) {
          $json_data = json_decode ( file_get_contents ( database_path ('data/BOOKS/json/'.$book->code.'.json') ) );
          foreach ( $json_data AS $num => $verses ) {

            $chapter = $this->chapter_repository->create ([
              'book_id' => $book->id,
              'number'  => intval ($num),
            ]);

            foreach ( $verses AS $vn => $verse ) {
              $verse = $this->verse_repository->create ([
                'book_id'     => $book->id,
                'chapter_id'  => $chapter->id,
                'number'      => intval ($vn),
                'text'        => [
                  'en' => $verse
                ],
                'frequencies' => app (WordFrequencyContract::class)->text ($verse)->frequencies (),
              ]);
            }

            $chapter->update ([
              'text' => [
                'en' => $chapter->full_text,
              ],
              'frequencies' => app (WordFrequencyContract::class)->text ($chapter->full_text)->frequencies (),
            ]);


          }

        }
    }
}
