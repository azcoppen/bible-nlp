<?php

Route::get ('/', 'HomeController@index')->name ('home');
Route::get ('search', 'SearchController@index')->name ('global.search');

Route::get ('books/volumes', 'BookController@index');
Route::prefix ('books/{book}')->group (function ($book) {

  Route::get ('search', 'BookController@search')->name ('books.search');

  Route::prefix ('chapters')->group (function () {

    Route::get ('/', 'ChapterController@index')->name('chapters.index');
    Route::prefix ('{chapter}')->group (function () {

      Route::get ('/', 'ChapterController@show')->name('chapters.show');

      Route::prefix ('verses')->group (function () {

        Route::get ('/', 'VerseController@index')->name('verses.index');
        Route::prefix ('{verse_range}')->group (function () {
          Route::get ('/', 'VerseController@show')->name('verses.show');
          Route::get ('crossrefs', 'VerseController@crossrefs')->name('verses.crossrefs');
          Route::get ('similar', 'VerseController@similar')->name('verses.similar');
        });

      });

    });

  });
  Route::get ('/', 'BookController@show')->name ('books.show');

});

Route::prefix ('books/volumes/{volume?}/{ordering?}')->group (function () {
  Route::get ('/', 'BookController@index')->name ('books.index');
});

Route::get ('books', 'BookController@index');


Route::prefix ('entities')->name('entities.')->group (function () {
  Route::get ('/', 'EntityController@index')->name ('index');

  Route::prefix ('{type}')->group (function () {
    Route::prefix ('{letter}')->group (function ($letter) {
      Route::get ('/', 'EntityController@index')->name ('az');
      Route::prefix ('{entity}')->group (function () {
        Route::get ('{book}/{chapter}', 'EntityController@chapter')->name ('chapter');
        Route::get ('{book}', 'EntityController@book')->name ('book');
        Route::get ('/', 'EntityController@show')->name ('show');
      });
    });
    Route::get ('/', 'EntityController@index')->name ('type');

  });
});

Route::prefix ('frequencies')->name('frequencies.')->group (function () {
  Route::get ('/', 'FrequencyController@index')->name ('index');
  Route::prefix ('{letter}')->group (function ($letter) {
    Route::get ('/', 'FrequencyController@index')->name ('az');
    Route::get ('{frequency}/{book}/{chapter}', 'FrequencyController@chapter')->name ('chapter');
    Route::get ('{frequency}/{book}', 'FrequencyController@book')->name ('book');
    Route::get ('{frequency}', 'FrequencyController@show')->name ('show');
  });
});

Route::prefix ('topics')->name('topics.')->group (function () {
  Route::get ('/', 'TopicController@index')->name ('index');
  Route::prefix ('{letter}')->group (function ($letter) {
    Route::get ('/', 'TopicController@index')->name ('az');
    Route::get ('{topic}/{subject}', 'TopicController@show')->name ('subject');
    Route::get ('{topic}', 'TopicController@show')->name ('show');
  });
});
