window.book_graphs = [];
window.book_count = null;


$( document ).ready(function() {

  $( ".book-count-graph" ).each(function( index ) {
    window.book_count  = new Chartist.Bar('#'+$(this).attr('id'), {
      labels: JSON.parse($(this).attr('data-labels')),
        series: JSON.parse($(this).attr('data-values'))
    }, {
        fullWidth: true,
        axisY: {
          onlyInteger: true
        }
    });
  });

  $( ".book-group-graph, .topic-group-graph, .search-group-graph" ).each(function( index ) {
    window.book_graphs[index] = new Chartist.Line('#'+$(this).attr('id'), {
      labels: JSON.parse($(this).attr('data-labels')),
        series: JSON.parse($(this).attr('data-values'))
    }, {
        fullWidth: true,
        axisY: {
          onlyInteger: true
        },
        showArea: true
    });
  });

  $( ".chapter-radar-graph" ).each(function( index ) {

      var ctx = document.getElementById($(this).attr('id')).getContext("2d");
      var radarChart = new Chart(ctx, {
          type: 'radar',
          data: {
              labels: JSON.parse($(this).attr('data-labels')),
              datasets: [
                  {
                      label: "Chapter Spread",
                      backgroundColor: "rgba(200,0,0,0.6)",
                      fillColor: "#ADF7C9",
                      strokeColor: "#93E399",
                      pointColor: "#93E399",
                      pointStrokeColor: "#fff",
                      pointHighlightFill: "#fff",
                      pointHighlightStroke: "rgba(220,220,220,1)",
                      data: JSON.parse($(this).attr('data-values'))
                  }
              ]
          },
          options: {
              legend: {
                display: false,
              },
              scaleShowLine: true,
              angleShowLineOut: true,
              scaleShowLabels: false,
              scaleBeginAtZero: true,
              angleLineColor: "rgba(0,0,0,.1)",
              angleLineWidth: 1,
              pointLabelFontFamily: "'Arial'",
              pointLabelFontStyle: "normal",
              pointLabelFontSize: 10,
              pointLabelFontColor: "#666",
              pointDot: true,
              pointDotRadius: 3,
              pointDotStrokeWidth: 1,
              pointHitDetectionRadius: 20,
              datasetStroke: true,
              datasetStrokeWidth: 2,
              datasetFill: true,
              responsive: true,
          }
      });
  });

  $( ".chapter-gauge-graph" ).each(function( index ) {
        var gaugeChart = new Chartist.Pie('#'+$(this).attr('id'), {
        series: JSON.parse($(this).attr('data-values'))
    }, {
        donut: true,
        donutWidth: 60,
        startAngle: 270,
        total: parseInt($(this).attr('data-total')),
        showLabel: false
    });
  });

  $('.multi-entity-bar').each ( function (index) {
    var barData = {
        labels: JSON.parse($(this).attr('data-labels')),
        datasets: [
            {
                label: "Frequency",
                fillColor: "rgba(210, 214, 222, 1)",
                strokeColor: "rgba(210, 214, 222, 1)",
                pointColor: "rgba(210, 214, 222, 1)",
                pointStrokeColor: "#c1c7d1",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: JSON.parse($(this).attr('data-values'))
            }
        ]
    };

    var barChartOptions = {
        legend: {
          display: false,
        },
        //Boolean - If we should show the scale at all
        showScale: true,
        //Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines: false,
        //String - Colour of the grid lines
        scaleGridLineColor: "rgba(0,0,0,.05)",
        //Number - Width of the grid lines
        scaleGridLineWidth: 1,
        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,
        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines: true,
        //Boolean - Whether the line is curved between points
        bezierCurve: true,
        //Number - Tension of the bezier curve between points
        bezierCurveTension: 0.3,
        //Boolean - Whether to show a dot for each point
        pointDot: false,
        //Number - Radius of each point dot in pixels
        pointDotRadius: 4,
        //Number - Pixel width of point dot stroke
        pointDotStrokeWidth: 1,
        //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
        pointHitDetectionRadius: 20,
        //Boolean - Whether to show a stroke for datasets
        datasetStroke: true,
        //Number - Pixel width of dataset stroke
        datasetStrokeWidth: 2,
        //Boolean - Whether to fill the dataset with a color
        datasetFill: true,
        //String - A legend template
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
        //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio: true,
        //Boolean - whether to make the chart responsive to window resizing
        responsive: true
    };


    var ctx = document.getElementById($(this).attr('id')).getContext("2d");

    var barChartData = barData;
    barChartData.datasets[0].backgroundColor="#bfbfbf";
    barChartOptions.datasetFill = false;
    var barChart = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: barChartOptions
    });
  });

  $( ".multi-entity-polar" ).each(function( index ) {

    var ctx = document.getElementById($(this).attr('id')).getContext("2d");
    var pieChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            datasets: [{
                data: JSON.parse($(this).attr('data-values')),
                backgroundColor: JSON.parse($(this).attr('data-colors'))
            }],
            labels: JSON.parse($(this).attr('data-labels'))
        },
        options: {
            legend: {
              display: false,
            },
            segmentShowStroke: true,
            segmentStrokeColor: "#fff",
            segmentStrokeWidth: 2,
            percentageInnerCutout: 45, // This is 0 for Pie charts
            animationSteps: 100,
            animationEasing: "easeOutBounce",
            animateRotate: true,
            animateScale: false,
            responsive: true,
        }
    });


  });


});
