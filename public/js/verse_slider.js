window.verses_url = '{{route ('verses.index', [$book->slug, $chapter->number])}}/';
window.verse_slider = document.getElementById('verse-slider');
noUiSlider.create(window.verse_slider, {
    start: [{{$verses->min('number')}}, {{$verses->max('number') == $verses->min('number') ? $verses->min('number') + 1 : $verses->max('number')}}],
    behaviour: 'drag',
    connect: true,
    step: 1,
    range: {
        'min': 1,
        'max': {{$chapter->verses->count()}}
    },
    pips: {
        mode: 'values',
        values: {{json_encode (range (1, $chapter->verses->count()))}},
        density: 1
    }
});

$( "#slice-btn" ).click(function() {
  var positions = window.verse_slider.noUiSlider.get();
  window.location = window.verses_url + parseInt (positions[0]) + '-' +  parseInt (positions[1]);
});
