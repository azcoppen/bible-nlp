$( document ).ready(function() {
  new Chart(document.getElementById("doughnut-chart"), {
      type: 'doughnut',
      data: {
          labels: ["Literature", "Literature", "Literature", "Literature", "Literature"],
          datasets: [
              {
                  label: "Words (thousands)",
                  backgroundColor: ["#FFC658", "#FFC658", "#FFC658", "#FFC658", "#FFC658", "#FFC658", "#FFFC658"],
                  data: [10, 15, 10, 15, 10, 15, 15]
              }
          ]
      },
      options: {
          title: {
              display: false,
              text: 'Literature Breakdown'
          }, legend: {
              display: false
          },
          tooltips: {
              enabled: true
          },
          segmentShowStroke: true,
          segmentStrokeColor: "#fff",
          segmentStrokeWidth: 3,
          percentageInnerCutout: 20,
          animationSteps: 100,
          animationEasing: "easeOutBounce",
          animateRotate: true,
          responsive: true,
          maintainAspectRatio: false,
          showScale: true,
          animateScale: true,
          cutoutPercentage: 70

      }
  });
});



//ct chart
var lineChart = new c3.generate({
    bindto: '#ct-news-chart',
    data: {
        columns: [
            ['data1', 60, 20, 80, 30, 70]
        ],
        colors: {
            data1: '#3f51b5'
        }
    }, padding: {
        right: 0,
        left: 0,
        bottom: 0
    }, axis: {
        x: {
            show: false
        },
        y: {
            show: false
        }
    }, legend: {
        show: false
    }, tooltip: {
        show: false
    }
});


  //metrics-Chartist
      var data = {
          labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun'],
          series: [
              [34, 40, 70, 50, 44, 68, 88]
          ]
      };

      var options = {
          seriesBarDistance: 50,
          axisX: {
              showLabel: false,
              showGrid: false,
              offset: 0
          },
          axisY: {
              showLabel: false,
              showGrid: false,
              offset: 0
          }
      };
      var responsiveOptions = [
          ['screen and (max-width: 640px)', {
                  seriesBarDistance: 8,
                  axisX: {
                      labelInterpolationFnc: function (value) {
                          return value[0];
                      }
                  }
              }]
      ];

      new Chartist.Bar('#news-ch-chart', data, options, responsiveOptions);




      //js-chart/Pie Chart
      var pieChartData = {
          datasets: [{
                  data: [700, 500, 400, 600, 300, 100],
                  backgroundColor: ["#3F51B5", "#00a65a", "#f39c12", "#00c0ef", "#3c8dbc", "#d2d6de"]
              }],
          // These labels appear in the legend and in the tooltips when hovering different arcs
          labels: [
              'Chrome',
              'IE',
              'FireFox',
              'Safari',
              'Opera',
              'Navigator'
          ]
      };

      var pieChartOptions = {
          segmentShowStroke: true,
          segmentStrokeColor: "#fff",
          segmentStrokeWidth: 2,
          percentageInnerCutout: 45, // This is 0 for Pie charts
          animationSteps: 100,
          animationEasing: "easeOutBounce",
          animateRotate: true,
          animateScale: false,
          responsive: true,
          legend: {
              display: false,
          },
          tooltips: {
              enabled: false,
          }
      };

      var ctx = document.getElementById("pieChart").getContext("2d");
      var pieChart = new Chart(ctx, {
          type: 'doughnut',
          data: pieChartData,
          options: pieChartOptions
      });
