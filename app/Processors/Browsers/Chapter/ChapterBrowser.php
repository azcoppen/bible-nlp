<?php

namespace BibleNLP\Processors\Browsers\Chapter;

use Illuminate\Http\Request;
use BibleNLP\Processors\Browser;
use BibleNLP\Models\Book;
use BibleNLP\Models\Chapter;

use BibleNLP\Exceptions\NoCachedDataException;

class ChapterBrowser extends Browser {

  public $book;

  public function __construct () {

  }

  public function ready ( Book $book, int $chapter ) {
    $this->book = $book;
    $this->book->load (['chapters']);
    return $this->book->chapters->where ('number', $chapter)->first () ? true : false;
  }

  public function verse_entity_groups ( Chapter $chapter, $verses ) {
    if ( !cache ()->has ($chapter->id.'-entity-groups') || !is_null (cache ()->get ($chapter->id.'-entity-groups')) ) {
      $entities = $verses->pluck('entities')->flatten(); // only do it once
      $data = collect ([
        'people'        => $entities->where ('type', 'PERSON')->unique('name')->sortBy('name'),
        'organisations' => $entities->where ('type', 'ORGANIZATION')->unique('name')->sortBy('name'),
        'locations'     => $entities->where ('type', 'LOCATION')->unique('name')->sortBy('name'),
        'events'        => $entities->where ('type', 'EVENT')->unique('name')->sortBy('name'),
        'artworks'      => $entities->where ('type', 'WORK_OF_ART')->unique('name')->sortBy('name'),
        'goods'         => $entities->where ('type', 'CONSUMER_GOOD')->unique('name')->sortBy('name'),
        'other'         => $entities->where ('type', 'OTHER')->unique('name')->sortBy('name'),
      ]);
      cache ()->put ($chapter->id.'-entity-groups', $data, 43800);
    }
    return cache ()->get ($chapter->id.'-entity-groups');
  }

  public function verse_frequencies ( Chapter $chapter, $verses ) {
    if ( !cache ()->has ($chapter->id.'-frequencies') || !is_null (cache ()->get ($chapter->id.'-frequencies')) ) {
      $freqs = $verses->pluck ('frequencies')->flatten()->sortBy('word')->groupBy('word');
      $combined = [];
      foreach ( $freqs AS $word => $data ) {
        $combined[$word] = $data->sum ('total');
      }
      $data = collect ($combined);
      cache ()->put ($chapter->id.'-frequencies', $data, 43800);
    }
    return cache ()->get ($chapter->id.'-frequencies');
  }

  public function setup ( Request $request, Book $book, int $chapter ) {
    try {
      return $this->data_cache ($request);
    } catch ( NoCachedDataException $e ) {
      $this->book->load (['chapters.verses']);
      $chapter = $this->book->chapters->where ('number', $chapter)->first ();
      $verses = $chapter->verses->sortBy('number');

      $verses->load ([
        'entity_refs',
        'entity_refs.entity',
        'frequencies',
        'topic_refs',
      ]);

      $data = [
        'book'            => $book,
        'nav_menu'        => 'books',
        'subnav'          => $book->volume,
        'chapter_view'    => true,
        'chapter'         => $chapter,
        'verses'          => $verses,
        'entity_groups'   => $this->verse_entity_groups ($chapter, $verses),
        'frequencies'     => $this->verse_frequencies ($chapter, $verses),
      ];

      $this->store_cached_data ($request, $data);
      return $data;

    }

  }
}
