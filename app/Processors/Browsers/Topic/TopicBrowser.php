<?php

namespace BibleNLP\Processors\Browsers\Topic;

use Illuminate\Http\Request;
use BibleNLP\Processors\Browser;

use BibleNLP\Repositories\SummaryRepository;
use BibleNLP\Repositories\TopicRepository;

use BibleNLP\Criteria\WhereInCriteria;
use BibleNLP\Criteria\WhereFieldEqualsCriteria;
use BibleNLP\Criteria\GroupedTopicsCriteria;
use BibleNLP\Criteria\TopTopicsCriteria;

use BibleNLP\Models\Topic;
use BibleNLP\Models\TopicTable;
use Illuminate\Pagination\LengthAwarePaginator;

use BibleNLP\Exceptions\NoCachedDataException;

use BibleNLP\Events\SearchEntered;

class TopicBrowser extends Browser {

  public $repository;
  public $top = false;

  public function __construct () {
    $this->repository = app (TopicRepository::class);
  }

  public function prepare ( Request $request, $az ) {
    try {
      if ( $request->has ('q') && !empty($request->get('q')) ) {
        event ( new SearchEntered (Topic::class, $az, $request->get('q')) );
      }
      return $this->data_cache ($request);
    } catch ( NoCachedDataException $e ) {
      if ( $request->has ('q') && !empty($request->get('q')) ) {
        $topics = TopicTable::search ( '*'.preg_replace("/[^A-Za-z0-9 ]/", '', $request->input ('q', 'Jesus')).'*' );

        if ( $az && !empty ($az) ) {
          $topics = $topics->where ('az', strtoupper ($az));
        }

        $topics = $topics->take(100)->get ();


      } else {
        if ( $az && !empty ($az) ) {
          $this->repository->pushCriteria ( new WhereFieldEqualsCriteria ('az', strtoupper ($az)) );
        } else {
          $this->repository->pushCriteria ( TopTopicsCriteria::class );
          $this->top = true;
        }
        $topics = $this->repository->paginate (50, ['*']);
      }

      $this->store_cached_data ($request, $topics);
      return $topics;
    }


  }

  public function view_vars ( Request $request, $az ) {
    return [
      'nav_menu'  => 'topics',
      'az'        => $az,
      'top'       => $this->top,
    ];
  }

  public function setup ( Request $request, string $az = 'all', Topic $topic, string $subject = null ) {
    try {
      return $this->data_cache ($request);
    } catch ( NoCachedDataException $e ) {
      $topic->load (['topic_refs', 'topic_refs.verse']);

      if ( $subject ) {
        $uri = url ('topics/'.$az.'/'.$topic->slug.'/'.$subject);
        $refs = $topic->topic_refs->where ('subject_slug', $subject)->sortBy('subject_slug');
      } else {
        $uri = url ('topics/'.$az.'/'.$topic->slug);
        $refs = $topic->topic_refs->sortBy('subject_slug');
      }

      $bible = $refs->groupBy('book_num')->transform(function ($item, $key) {
          return $item->count();
      })->sortKeys();

      $subject_span = $refs->groupBy('subject_slug')->transform(function ($item, $key) {
          return $item->groupBy('book_num')->transform(function ($item, $key) {
            return $item->groupBy('chap_num')->transform(function ($item, $key) {
              return $item->count();
            })->sortKeys();
          })->sortKeys();
      });

      $refs = new LengthAwarePaginator (
          $refs->forPage($request->input('page', 1), 25), $refs->count(), 25, $request->input('page', 1), ['path'=>$uri]
      );

      $chap_summaries = app (SummaryRepository::class)
        ->pushCriteria (new WhereInCriteria ('container_id', $refs->pluck('verse')->pluck('chapter_id')->unique()->all()))
        ->all();

      $data = [
        'az'            => $az,
        'bible'         => $bible,
        'subject_span'  => $subject_span,
        'topic'         => $topic,
        'subject'       => !is_null ($subject) ? $topic->topic_refs->where ('subject_slug', $subject)->first() : null,
        'refs'          => $refs,
        'summaries'     => $chap_summaries->groupBy('book_num'),
        'nav_menu'      => 'topics',
      ];

      $this->store_cached_data ($request, $data);
      return $data;

    }

  }

}
