<?php

namespace BibleNLP\Processors\Browsers\Frequency;

use Illuminate\Http\Request;
use BibleNLP\Processors\Browser;
use BibleNLP\Repositories\ChapterRepository;
use BibleNLP\Repositories\FrequencyRepository;

use BibleNLP\Models\Book;
use BibleNLP\Models\Frequency;
use BibleNLP\Models\Verse;

use BibleNLP\Criteria\GroupedCountCriteria;
use BibleNLP\Criteria\WhereFieldEqualsCriteria;
use BibleNLP\Criteria\WhereInCriteria;
use BibleNLP\Criteria\OrderByCriteria;
use BibleNLP\Criteria\TopFrequenciesCriteria;

use BibleNLP\Exceptions\NoCachedDataException;

class ChapterFrequencyBrowser extends Browser {

  public $repository;

  public function __construct () {
    $this->repository = app (FrequencyRepository::class);
  }

  public function setup ( Request $request, string $az, Frequency $frequency, Book $book, int $chap_num ) {
    try {
      return $this->data_cache ($request);
    } catch ( NoCachedDataException $e ) {
      $chapter = app (ChapterRepository::class)
        ->pushCriteria ( new WhereFieldEqualsCriteria ('book_id', $book->id) )
        ->pushCriteria ( new WhereFieldEqualsCriteria ('number', $chap_num) )
        ->with (['frequencies', 'verses', 'summaries'])
        ->first ();

      $mentions = $this->repository
        ->pushCriteria ( new WhereFieldEqualsCriteria ('container_type', Verse::class) )
        ->pushCriteria ( new WhereInCriteria ('container_id', $chapter->verses->pluck('id')->all ()) )
        ->pushCriteria ( new WhereFieldEqualsCriteria ('word', $frequency->word) )
        ->all ();

      $verse_count = $chapter->verses->filter(function ($value, $key) use ($mentions) {
          return $mentions->contains ('container_id', $value->id);
      })->mapWithKeys(function ($item) use ($mentions) {
        return [$item->number => $mentions->where('container_id', $item->id)->first()->total];
      })->sortKeys();

      if ( !$chapter ) {
        abort (404);
      }

      $data = [
        'az'          => $az,
        'book'        => $book,
        'frequency'   => $frequency,
        'chapter'     => $chapter,
        'mentions'    => $mentions,
        'verse_count' => $verse_count,
        'nav_menu'    => 'frequencies',
      ];

      $this->store_cached_data ($request, $data);
      return $data;
    }

  }

}
