<?php

namespace BibleNLP\Processors\Browsers\Frequency;

use Illuminate\Http\Request;
use BibleNLP\Processors\Browser;
use BibleNLP\Repositories\ChapterRepository;
use BibleNLP\Repositories\FrequencyRepository;

use BibleNLP\Models\Book;
use BibleNLP\Models\Frequency;
use BibleNLP\Models\Verse;

use BibleNLP\Criteria\GroupedCountCriteria;
use BibleNLP\Criteria\WhereFieldEqualsCriteria;
use BibleNLP\Criteria\WhereInCriteria;
use BibleNLP\Criteria\OrderByCriteria;
use BibleNLP\Criteria\TopFrequenciesCriteria;

use BibleNLP\Exceptions\NoCachedDataException;

class BookFrequencyBrowser extends Browser {

  public $repository;

  public function __construct () {
    $this->repository = app (FrequencyRepository::class);
  }

  public function setup ( Request $request, string $az, Frequency $frequency, Book $book ) {
    try {
      return $this->data_cache ($request);
    } catch ( NoCachedDataException $e ) {
      $chapters = app (ChapterRepository::class)
        ->pushCriteria ( new WhereFieldEqualsCriteria ('book_id', $book->id) )
        ->with (['frequencies', 'verses', 'summaries'])
        ->all ();

      $verse_ids = $chapters->pluck('verses')->flatten()->pluck('id');

      $mentions = $this->repository
        ->pushCriteria ( new WhereFieldEqualsCriteria ('container_type', Verse::class) )
        ->pushCriteria ( new WhereInCriteria ('container_id', $verse_ids->all ()) )
        ->pushCriteria ( new WhereFieldEqualsCriteria ('word', $frequency->word) )
        ->all ();

      $chap_count = $chapters->filter(function ($value, $key) use ($mentions) {
          return $value->verses->whereIn ('id', $mentions->pluck('container_id')->all())->count() > 0;
      })->mapWithKeys(function ($item) use ($mentions) {
        return [$item->number => $item->verses->whereIn ('id', $mentions->pluck('container_id')->all())->count()];
      });

      $data = [
        'az'        => $az,
        'book'      => $book,
        'frequency' => $frequency,
        'chapters'  => $chapters,
        'mentions'  => $mentions,
        'chap_count'=> $chap_count,
        'nav_menu'  => 'frequencies',
      ];

      $this->store_cached_data ($request, $data);
      return $data;

    }
  }

}
