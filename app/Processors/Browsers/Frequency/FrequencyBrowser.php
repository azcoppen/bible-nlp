<?php

namespace BibleNLP\Processors\Browsers\Frequency;

use Illuminate\Http\Request;
use BibleNLP\Processors\Browser;
use BibleNLP\Repositories\FrequencyRepository;

use BibleNLP\Models\Chapter;
use BibleNLP\Models\Frequency;

use BibleNLP\Criteria\GroupedCountCriteria;
use BibleNLP\Criteria\WhereFieldEqualsCriteria;
use BibleNLP\Criteria\OrderByCriteria;
use BibleNLP\Criteria\TopFrequenciesCriteria;

use BibleNLP\Events\SearchEntered;

use BibleNLP\Exceptions\NoCachedDataException;

class FrequencyBrowser extends Browser {

  public $repository;

  public function __construct () {
    $this->repository = app (FrequencyRepository::class);
  }

  public function prepare ( Request $request, $az = null ) {
    try {
      return $this->data_cache ($request);
    } catch ( NoCachedDataException $e ) {
      if ($request->has('q') && !empty($request->get('q')) ) {
        $freqs = Frequency::search ('*'.preg_replace("/[^A-Za-z0-9 ]/", '', $request->input ('q', 'Jesus')).'*');

        if ( $az && !empty ($az) ) {
          $freqs = $freqs->where ('az', strtoupper ($az));
        }

        $freqs = $freqs->take(100)->get();
        event ( new SearchEntered (Frequency::class, $az, $request->get('q')) );

      } else {
        $this->repository->pushCriteria ( new WhereFieldEqualsCriteria ('container_type', 'GLOBAL') );

        if ( $az && !empty ($az) ) {
          $this->repository->pushCriteria ( new WhereFieldEqualsCriteria ('az', strtoupper ($az)) );
          $this->repository->pushCriteria ( new OrderByCriteria ('word', 'ASC') );
          $freqs = $this->repository->paginate (100, ['*']);
        } else {
          $this->repository->pushCriteria ( TopFrequenciesCriteria::class );
          $freqs = $this->repository->paginate (300, ['*']);
        }
      }

      $this->store_cached_data ($request, $freqs);
      return $freqs;
    }

  }

  public function view_vars ( Request $request, $az = null ) {
    return [
      'nav_menu'  => 'frequencies',
      'az'        => $az
    ];
  }

  public function setup ( Request $request, string $az, Frequency $frequency ) {
    try {
      return $this->data_cache ($request);
    } catch ( NoCachedDataException $e ) {
      $chapters = $this->repository
        ->pushCriteria ( new WhereFieldEqualsCriteria ('container_type', Chapter::class) )
        ->pushCriteria ( new WhereFieldEqualsCriteria ('word', $frequency->word) )
        ->pushCriteria ( new OrderByCriteria ('book_num', 'ASC') )
        ->paginate (100, ['*']);

      $agg = $this->repository
        ->pushCriteria ( new GroupedCountCriteria ('book_num', ['word' => $frequency->word, 'container_type' => Chapter::class], 'total') )
        ->all();

      $bible = [];

      foreach ($agg AS $num => $result) {
        $bible[head($result['_id']->getArrayCopy())] = $result['summed'];
      }

      $data = [
        'az'        => $az,
        'frequency' => $frequency,
        'chapters'  => $chapters,
        'bible'     => collect($bible)->sortKeys(),
        'nav_menu'  => 'frequencies',
      ];

      $this->store_cached_data ($request, $data);
      return $data;

    }

  }

}
