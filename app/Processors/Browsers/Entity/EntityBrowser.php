<?php

namespace BibleNLP\Processors\Browsers\Entity;

use Illuminate\Http\Request;
use BibleNLP\Processors\Browser;
use BibleNLP\Repositories\EntityRepository;
use BibleNLP\Repositories\EntityRefRepository;

use BibleNLP\Models\Chapter;
use BibleNLP\Models\Entity;

use BibleNLP\Criteria\GroupByCriteria;
use BibleNLP\Criteria\GroupedCountCriteria;
use BibleNLP\Criteria\OrderByCriteria;
use BibleNLP\Criteria\TopEntitiesCriteria;
use BibleNLP\Criteria\WhereInCriteria;
use BibleNLP\Criteria\WhereFieldEqualsCriteria;

use BibleNLP\Events\SearchEntered;
use BibleNLP\Exceptions\NoCachedDataException;

class EntityBrowser extends Browser {

  public $repository;

  public function __construct () {
    $this->repository = app (EntityRepository::class);
  }

  public function prepare ( Request $request, $type = 'all', $az = null ) {
    try {
      return $this->data_cache ($request);
    } catch ( NoCachedDataException $e ) {
      if ($request->has('q') && !empty($request->get('q')) ) {
        $entities = Entity::search ('*'.preg_replace("/[^A-Za-z0-9 ]/", '', $request->input ('q', 'Jesus')).'*');
        if ( $type && !empty ($type) ) {
          if ( $type != 'all' ) {
            $entities = $entities->where ('type', strtoupper ($type));
          }
        }

        if ( $az && !empty ($az) ) {
          $entities = $entities->where ('az', strtoupper ($az));
        }

        $entities = $entities->take(100)->get();
        event ( new SearchEntered (Entity::class, $az, $request->get('q')) );


      } else {
        if ( $type && !empty ($type) ) {
          if ( $type != 'all' ) {
            $this->repository->pushCriteria ( new WhereFieldEqualsCriteria ('type', strtoupper ($type)) );
          }
          $entities = $this->repository->paginate (100, ['*']);
        }

        if ( $az && !empty ($az) ) {
          $this->repository->pushCriteria ( new WhereFieldEqualsCriteria ('az', strtoupper ($az)) );
          $this->repository->pushCriteria ( new OrderByCriteria ('name', 'ASC') );
          $entities = $this->repository->paginate (100, ['*']);
        } else {
          $this->repository->pushCriteria ( new TopEntitiesCriteria (strtoupper ($type)) );
          $entities = $this->repository->paginate (100, ['*']);
        }
      }

      $this->store_cached_data ($request, $entities);
      return $entities;
    }

  }

  public function view_vars ( Request $request, $type = 'all', $az = null ) {
    return [
      'nav_menu'      => 'entities',
      'type'          => $type,
      'az'            => $az,
      'subnav'        => $type,
    ];
  }

  public function valid ( $entity, $type ) {
    if (!$entity || $type != strtolower ($entity->type) ) {
      return false;
    }
    return true;
  }

  public function setup ( Request $request, string $type, string $az, string $entity_slug ) {
    try {
      return $this->data_cache ($request);
    } catch ( NoCachedDataException $e ) {
      $entity = $this->repository
        ->pushCriteria ( new WhereFieldEqualsCriteria ('type', strtoupper ($type)) )
        ->pushCriteria ( new WhereFieldEqualsCriteria ('slug', $entity_slug) )
        ->first ();

      if ( !$entity || $type != strtolower ($entity->type) ) {
        abort (404);
      }

      $chapters = app (EntityRefRepository::class)
        ->pushCriteria ( new WhereFieldEqualsCriteria ('reference_type', Chapter::class) )
        ->pushCriteria ( new WhereFieldEqualsCriteria ('entity_id', $entity->id) )
        ->pushCriteria ( new OrderByCriteria ('book_num', 'ASC') )
        ->paginate (100, ['*']); // NOTE PAGINATE

      $agg = app (EntityRefRepository::class)
        ->pushCriteria ( new GroupedCountCriteria ('book_num', ['entity_id' => $entity->id, 'reference_type' => Chapter::class], 'mentions') )
        ->all();

      $bible = [];

      foreach ($agg AS $num => $result) {
        $bible[head($result['_id']->getArrayCopy())] = $result['summed'];
      }

      $data = [
        'type'      => $type,
        'bible'     => collect($bible)->sortKeys(),
        'az'        => $az,
        'entity'    => $entity,
        'chapters'  => $chapters,
        'nav_menu'  => 'entities',
        'subnav'    => $type,
      ];

      $this->store_cached_data ($request, $data);
      return $data;

    }

  }

}
