<?php

namespace BibleNLP\Processors\Browsers\Entity;

use Illuminate\Http\Request;
use BibleNLP\Processors\Browser;
use BibleNLP\Repositories\ChapterRepository;
use BibleNLP\Repositories\EntityRepository;
use BibleNLP\Repositories\EntityRefRepository;

use BibleNLP\Models\Book;
use BibleNLP\Models\Chapter;
use BibleNLP\Models\Verse;

use BibleNLP\Criteria\WhereInCriteria;
use BibleNLP\Criteria\WhereFieldEqualsCriteria;

use BibleNLP\Exceptions\NoCachedDataException;

class BookEntityBrowser extends Browser {

  public $repository;

  public function __construct () {
    $this->repository = app (EntityRepository::class);
  }

  public function view_vars ( Request $request, $type = 'all', $az = null ) {
    return [
      'nav_menu'      => 'entities',
      'type'          => $type,
      'az'            => $az,
      'subnav'        => $type,
    ];
  }

  public function valid ( $entity, $type ) {
    if (!$entity || $type != strtolower ($entity->type) ) {
      return false;
    }
    return true;
  }

  public function setup ( Request $request, string $type, string $az, string $entity_slug, Book $book ) {
    try {
      return $this->data_cache ($request);
    } catch ( NoCachedDataException $e ) {
      $entity = $this->repository
        ->pushCriteria ( new WhereFieldEqualsCriteria ('type', strtoupper ($type)) )
        ->pushCriteria ( new WhereFieldEqualsCriteria ('slug', $entity_slug) )
        ->first ();

      if ( $type != strtolower ($entity->type) ) {
        abort (404);
      }

      $chapters = app (ChapterRepository::class)
        ->pushCriteria ( new WhereFieldEqualsCriteria ('book_id', $book->id) )
        ->with (['verses', 'summaries'])
        ->all ();

      $mentions = app (EntityRefRepository::class)
        ->pushCriteria ( new WhereFieldEqualsCriteria ('reference_type', Verse::class) )
        ->pushCriteria ( new WhereInCriteria ('reference_id', $chapters->pluck('verses')->flatten()->pluck('id')->all ()) )
        ->pushCriteria ( new WhereFieldEqualsCriteria ('entity_id', $entity->id) )
        ->all ();

      $chap_count = $chapters->filter(function ($value, $key) use ($mentions) {
          return $value->verses->whereIn ('id', $mentions->pluck('reference_id')->all())->count() > 0;
      })->mapWithKeys(function ($item) use ($mentions) {
        return [$item->number => $item->verses->whereIn ('id', $mentions->pluck('reference_id')->all())->count()];
      });

      $data = [
        'type'      => $type,
        'az'        => $az,
        'book'      => $book,
        'entity'    => $entity,
        'chapters'  => $chapters,
        'mentions'  => $mentions,
        'chap_count'=> $chap_count,
        'nav_menu'  => 'entities',
        'subnav'    => $type,
      ];

      $this->store_cached_data ($request, $data);
      return $data;

    }
  }

}
