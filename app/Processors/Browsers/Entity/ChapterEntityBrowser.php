<?php

namespace BibleNLP\Processors\Browsers\Entity;

use Illuminate\Http\Request;
use BibleNLP\Processors\Browser;

use BibleNLP\Repositories\ChapterRepository;
use BibleNLP\Repositories\EntityRepository;
use BibleNLP\Repositories\EntityRefRepository;

use BibleNLP\Models\Book;
use BibleNLP\Models\Chapter;
use BibleNLP\Models\Verse;

use BibleNLP\Criteria\GroupByCriteria;
use BibleNLP\Criteria\GroupedCountCriteria;
use BibleNLP\Criteria\OrderByCriteria;
use BibleNLP\Criteria\TopEntitiesCriteria;
use BibleNLP\Criteria\WhereInCriteria;
use BibleNLP\Criteria\WhereFieldEqualsCriteria;

use BibleNLP\Exceptions\NoCachedDataException;

class ChapterEntityBrowser extends Browser {

  public $repository;

  public function __construct () {
    $this->repository = app (EntityRepository::class);
  }

  public function setup ( Request $request, string $type, string $az, string $entity_slug, Book $book, int $chap_num ) {
    try {
      return $this->data_cache ($request);
    } catch ( NoCachedDataException $e ) {
      $entity = $this->repository
        ->pushCriteria ( new WhereFieldEqualsCriteria ('type', strtoupper ($type)) )
        ->pushCriteria ( new WhereFieldEqualsCriteria ('slug', $entity_slug) )
        ->first ();

      if ( $type != strtolower ($entity->type) ) {
        abort (404);
      }

      $chapter = app (ChapterRepository::class)
        ->pushCriteria ( new WhereFieldEqualsCriteria ('book_id', $book->id) )
        ->pushCriteria ( new WhereFieldEqualsCriteria ('number', $chap_num) )
        ->with (['verses'])
        ->first ();

      $mentions = app (EntityRefRepository::class)
        ->pushCriteria ( new WhereFieldEqualsCriteria ('reference_type', Verse::class) )
        ->pushCriteria ( new WhereInCriteria ('reference_id', $chapter->verses->pluck('id')->all ()) )
        ->pushCriteria ( new WhereFieldEqualsCriteria ('entity_id', $entity->id) )
        ->all ();

      $verse_count = $chapter->verses->filter(function ($value, $key) use ($mentions) {
          return $mentions->contains ('reference_id', $value->id);
      })->mapWithKeys(function ($item) use ($mentions) {
        return [$item->number => $mentions->where('reference_id', $item->id)->first()->mentions];
      })->sortKeys();

      if ( !$chapter ) {
        abort (404);
      }

      $data = [
        'type'      => $type,
        'az'        => $az,
        'book'      => $book,
        'entity'    => $entity,
        'chapter'   => $chapter,
        'mentions'  => $mentions,
        'verse_count' => $verse_count,
        'nav_menu'  => 'entities',
        'subnav'    => $type,
      ];

      $this->store_cached_data ($request, $data);
      return $data;

    }

  }

}
