<?php

namespace BibleNLP\Processors\Browsers\Search;

use Illuminate\Http\Request;
use BibleNLP\Processors\Browser;
use BibleNLP\Repositories\BookRepository;

use BibleNLP\Models\Book;
use BibleNLP\Models\Entity;
use BibleNLP\Models\Frequency;
use BibleNLP\Models\Summary;
use BibleNLP\Models\TopicTable;
use BibleNLP\Models\Verse;

use BibleNLP\Criteria\OrderByCriteria;
use BibleNLP\Criteria\WhereFieldEqualsCriteria;

use Illuminate\Pagination\LengthAwarePaginator;
use BibleNLP\Events\SearchEntered;

use BibleNLP\Exceptions\NoCachedDataException;

class GlobalSearchBrowser extends Browser {

  public function entity_groups ( $request, $entities ) {
    if ( !cache ()->has ('search-'.str_slug($request->get('q')).'-entity-groups') || !is_null (cache ()->get ('search-'.str_slug($request->get('q')).'-entity-groups')) ) {
      $data = collect ([
        'people'        => $entities->where ('type', 'PERSON')->unique('name')->sortBy('name'),
        'organisations' => $entities->where ('type', 'ORGANIZATION')->unique('name')->sortBy('name'),
        'locations'     => $entities->where ('type', 'LOCATION')->unique('name')->sortBy('name'),
        'events'        => $entities->where ('type', 'EVENT')->unique('name')->sortBy('name'),
        'artworks'      => $entities->where ('type', 'WORK_OF_ART')->unique('name')->sortBy('name'),
        'goods'         => $entities->where ('type', 'CONSUMER_GOOD')->unique('name')->sortBy('name'),
        'other'         => $entities->where ('type', 'OTHER')->unique('name')->sortBy('name'),
      ]);
      cache ()->put ('search-'.str_slug($request->get('q')).'-entity-groups', $data, 43800);
    }
    return cache ()->get ('search-'.str_slug($request->get('q')).'-entity-groups');
  }


  public function prepare ( Request $request ) {
    try {
      event ( new SearchEntered ('GLOBAL', 0, $request->get('q')) );
      return $this->data_cache ($request);
    } catch ( NoCachedDataException $e ) {
      //$topics       = Topic::search ( $request->input ('q', 'Jesus') )->with(['topic_refs'])->get();
      $summaries    = Summary::search ( '*'.preg_replace("/[^A-Za-z0-9 ]/", '', $request->input ('q', 'Jesus')).'*' )
        ->with (['book'])->get();

      $verses       = Verse::search ( '*'.preg_replace("/[^A-Za-z0-9 ]/", '', $request->input ('q', 'Jesus')).'*' )
        ->take(50)->get();

      $entities     = Entity::search ( '*'.preg_replace("/[^A-Za-z0-9 ]/", '', $request->input ('q', 'Jesus')).'*' )
        ->get();

      $frequencies  = Frequency::search ( '*'.preg_replace("/[^A-Za-z0-9 ]/", '', $request->input ('q', 'Jesus')).'*' )
        ->get();

      $topics  = TopicTable::search ( '*'.preg_replace("/[^A-Za-z0-9 ]/", '', $request->input ('q', 'Jesus')).'*' )
          ->get();

      $graph_chapters = [];
      foreach ($summaries->groupBy('book_num') AS $book_num => $summary_coll) {
        $graph_chapters[$book_num] = $summary_coll->count();
      }

      $graph_verses = [];
      foreach ($verses->groupBy('book_num') AS $book_num => $verse_coll) {
        $graph_verses[$book_num] = $verse_coll->count();
      }

      $data = [
        'summaries'       => $summaries,
        'verses'          => $verses,
        'entity_groups'   => $this->entity_groups ($request, $entities),
        'frequencies'     => $frequencies,
        'topics'          => $topics,
        'graph_chapters'  => collect($graph_chapters)->sortKeys(),
        'graph_verses'    => collect($graph_verses)->sortKeys(),
        'nav_menu'        => 'search',
      ];

      $this->store_cached_data ($request, $data);
      event ( new SearchEntered ('GLOBAL', 0, $request->get('q')) );

      return $data;
    }
  }
}
