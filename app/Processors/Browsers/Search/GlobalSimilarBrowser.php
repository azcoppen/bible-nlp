<?php

namespace BibleNLP\Processors\Browsers\Search;

use Illuminate\Http\Request;
use BibleNLP\Processors\Browser;
use BibleNLP\Models\Verse;

use BibleNLP\Repositories\VerseRepository;

use BibleNLP\Events\SearchEntered;

use BibleNLP\Exceptions\NoCachedDataException;

class GlobalSimilarBrowser extends Browser {

  public $repository;

  public function __construct() {
    $this->repository = app (VerseRepository::class);
  }

  public function prepare ( Request $request ) {

    $rows  = Verse::searchRaw ([
        'query' => [
            'more_like_this' => [
              "fields" => ["content", "words", "entities"],
              "like" => preg_replace("/[^A-Za-z0-9 ]/", '', $request->input ('q', 'Jesus')),
              "min_term_freq" => 1,
              "max_query_terms" => 12
            ]
        ]
    ]);

    dd(collect(data_get($rows, 'hits.hits')));

    $data = [
      'verses' => $verses
    ];



    event ( new SearchEntered ('SIMILAR', 0, $request->get('q')) );

    return $data;
  }
}
