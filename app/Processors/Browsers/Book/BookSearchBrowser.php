<?php

namespace BibleNLP\Processors\Browsers\Book;

use Illuminate\Http\Request;
use BibleNLP\Processors\Browser;
use BibleNLP\Repositories\BookRepository;

use BibleNLP\Models\Book;
use BibleNLP\Models\Verse;
use BibleNLP\Models\Summary;

use BibleNLP\Criteria\OrderByCriteria;
use BibleNLP\Criteria\WhereFieldEqualsCriteria;

use BibleNLP\Exceptions\NoCachedDataException;
use BibleNLP\Events\SearchEntered;

class BookSearchBrowser extends Browser {

  public function __construct () {

  }

  public function prepare ( Request $request, Book $book, int $chapter = null ) {
    try {
      event ( new SearchEntered (Book::class, $book->id, $request->get('q')) );
      return $this->data_cache ($request);
    } catch ( NoCachedDataException $e ) {
      $verses = Verse::search ( '*'.preg_replace("/[^A-Za-z0-9 ]/", '', $request->input ('q', 'God')).'*' )
        ->where ('book_num', $book->order);

      if ( $chapter ) {
        $verses = $verses->where ('chapter', $chapter);
      }

      $verses = $verses->take(50)->get ();

      $summaries    = Summary::search ( '*'.preg_replace("/[^A-Za-z0-9 ]/", '', $request->input ('q', 'GOd')).'*' )
        ->where ('book_num', $book->order)
        ->get();

      $graph_chapters = [];
      foreach ($summaries->groupBy('chap_num') AS $chap_num => $summary_coll) {
        $graph_chapters[$chap_num] = $summary_coll->count();
      }

      $graph_verses = [];
      foreach ($verses->groupBy('chap_num') AS $chap_num => $verse_coll) {
        $graph_verses[$chap_num] = $verse_coll->count();
      }

      event ( new SearchEntered (Book::class, $book->id, $request->get('q')) );

      return [
        'book'            => $book,
        'verses'          => $verses,
        'summaries'       => $summaries,
        'graph_chapters'  => collect($graph_chapters)->sortKeys(),
        'graph_verses'    => collect($graph_verses)->sortKeys(),
      ];

      $this->store_cached_data ($request, $data);
      return $data;
    }

  }

  public function view_vars ( Request $request, Book $book, int $chapter = null ) {
    return [
      'nav_menu'  => 'books',
      'subnav'    => $book->volume,
    ];
  }
}
