<?php

namespace BibleNLP\Processors\Browsers\Book;

use Illuminate\Http\Request;
use BibleNLP\Processors\Browser;
use BibleNLP\Repositories\BookRepository;

use BibleNLP\Models\Book;
use BibleNLP\Models\Verse;

use BibleNLP\Criteria\OrderByCriteria;
use BibleNLP\Criteria\WhereFieldEqualsCriteria;

use BibleNLP\Exceptions\NoCachedDataException;

class BookBrowser extends Browser {

  public $repository;

  public function __construct () {
    $this->repository = app (BookRepository::class);
  }

  public function prepare ( Request $request, string $volume = null, string $ordering = 'canonical' ) {

    try {
      return $this->data_cache ($request);
    } catch ( NoCachedDataException $e ) {
      if ( $volume && !empty ($volume) && $volume !='both' ) {
        $this->repository->pushCriteria ( new WhereFieldEqualsCriteria ('volume', $volume) );
      }

      if ( $ordering ) {
        switch ($ordering) {
          case 'length':
            $this->repository->pushCriteria ( new OrderByCriteria ('num_words', 'ASC') );
          break;

          case 'canonical':
            $this->repository->pushCriteria ( new OrderByCriteria ('order', 'ASC') );
          break;

          case 'alphabetical':
            $this->repository->pushCriteria ( new OrderByCriteria ('title', 'ASC') );
          break;

          case 'chronological':
            $this->repository->pushCriteria ( new OrderByCriteria ('written_from', 'ASC') );
            $this->repository->pushCriteria ( new OrderByCriteria ('written_to', 'ASC') );
          break;
        }
      }

      $books = $this->repository->paginate (100, ['*']);
      $this->store_cached_data ($request, $books);

      return $books;
    }
  }

  public function setup ( Request $request, Book $book ) {
    try {
      return $this->data_cache ($request);
    } catch ( NoCachedDataException $e ) {
      $book->load (['summaries']);
      $data = [
        'book'      => $book,
        'nav_menu'  => 'books',
        'subnav'    => $book->volume,
      ];
      $this->store_cached_data ($request, $data);
      return $data;
    }
  }

  public function view_vars ( Request $request, string $volume = null, string $ordering = 'canonical' ) {
    return [
      'nav_menu'  => 'books',
      'subnav'    => $volume,
      'volume'    => $volume,
      'ordering'  => $ordering,
    ];
  }

}
