<?php

namespace BibleNLP\Processors\Browsers\Verse;

use Illuminate\Http\Request;
use BibleNLP\Processors\Browser;
use BibleNLP\Models\Book;
use BibleNLP\Models\Chapter;

use BibleNLP\Repositories\SummaryRepository;
use BibleNLP\Repositories\VerseRepository;

use BibleNLP\Criteria\WhereInCriteria;

use BibleNLP\Exceptions\NoCachedDataException;

class VerseBrowser extends Browser {

  public $chapter;
  public $repository;

  public function __construct () {
    $this->repository = app (VerseRepository::class);
  }

  public function range ( $chapter, $verse_range ) {
    if ( str_contains ($verse_range, '-') ) {
      $parts = explode ('-', $verse_range);
      if ( is_array ($parts) && count ($parts) == 2 ) {

        if ( is_numeric ($parts[0]) && is_numeric ($parts[1]) ) {
          if ( $parts[0] == $parts[1] ) {
            if ($chapter->verses->where('number', $parts[0])->count() > 0 ) {
              return [$parts[0]];
            }
          } else if ( $parts[0] < $parts[1] ) {
            if ($chapter->verses->where('number', $parts[0])->count() > 0 && $chapter->verses->where('number', $parts[1])->count() > 0 ) {
              return range (intval($parts[0]), intval($parts[1]));
            }
          }
        }

      }
    }
    return false;
  }

  public function verse_entity_groups ( Chapter $chapter, $verses, $verse_range ) {
    if ( !cache ()->has ($chapter->id.'-'.$verse_range.'-entity-groups') || !is_null (cache ()->get ($chapter->id.'-'.$verse_range.'-entity-groups')) ) {
      $entities = $verses->pluck('entities')->flatten(); // only do it once
      $data = collect ([
        'people'        => $entities->where ('type', 'PERSON')->unique('name')->sortBy('name'),
        'organisations' => $entities->where ('type', 'ORGANIZATION')->unique('name')->sortBy('name'),
        'locations'     => $entities->where ('type', 'LOCATION')->unique('name')->sortBy('name'),
        'events'        => $entities->where ('type', 'EVENT')->unique('name')->sortBy('name'),
        'artworks'      => $entities->where ('type', 'WORK_OF_ART')->unique('name')->sortBy('name'),
        'goods'         => $entities->where ('type', 'CONSUMER_GOOD')->unique('name')->sortBy('name'),
        'other'         => $entities->where ('type', 'OTHER')->unique('name')->sortBy('name'),
      ]);
      cache ()->put ($chapter->id.'-'.$verse_range.'-entity-groups', $data, 43800);
    }
    return cache ()->get ($chapter->id.'-'.$verse_range.'-entity-groups');
  }

  public function verse_frequencies ( Chapter $chapter, $verses, $verse_range ) {
    if ( !cache ()->has ($chapter->id.'-'.$verse_range.'-frequencies') || !is_null (cache ()->get ($chapter->id.'-'.$verse_range.'-frequencies')) ) {
      $freqs = $verses->pluck ('frequencies')->flatten()->sortBy('word')->groupBy('word');
      $combined = [];
      foreach ( $freqs AS $word => $data ) {
        $combined[$word] = $data->sum ('total');
      }
      $data = collect ($combined);
      cache ()->put ($chapter->id.'-'.$verse_range.'-frequencies', $data, 43800);
    }
    return cache ()->get ($chapter->id.'-'.$verse_range.'-frequencies');
  }

  public function verse_cross_refs ( Chapter $chapter, $verses, $verse_range ) {
    if ( !cache ()->has ($chapter->id.'-'.$verse_range.'-crossrefs') || !is_null (cache ()->get ($chapter->id.'-'.$verse_range.'-crossrefs')) ) {
      $data = $verses->pluck ('cross_refs')->flatten()->sortBy('book_num')->groupBy('book_num')->transform(function ($item, $key) {
          return $item->sortBy('chap_num')->groupBy ('chap_num');
      });
      cache ()->put ($chapter->id.'-'.$verse_range.'-crossrefs', $data, 43800);
    }
    return cache ()->get ($chapter->id.'-'.$verse_range.'-crossrefs');
  }

  public function ready ( Book $book, int $chapter, string $verse_range ) {
    $this->book = $book;
    $this->book->load (['chapters']);
    $this->chapter = $this->book->chapters->where ('number', $chapter)->first ();

    if ( $this->chapter ) {
      $this->chapter->load (['verses']);
      if ( str_contains ($verse_range, '-') ) {
        $range = $this->range ( $this->chapter, $verse_range );

        if ( $range ) {
          return $this->chapter->verses->whereIn ('number', $range)->sortBy('number')->count ();
        }

      } else {

        if ( is_numeric ($verse_range) && $this->chapter->verses->where('number', $verse_range)->count () > 0 ) {
          return true;
        }

      }
    }
    return false;
  }

  public function setup ( Request $request, Book $book, int $chapter, string $verse_range ) {

    try {
      return $this->data_cache ($request);
    } catch ( NoCachedDataException $e ) {
      if ( str_contains ($verse_range, '-') ) {
        $verses = $this->chapter->verses->whereIn ('number', $this->range ( $this->chapter, $verse_range ))->sortBy('number');
      } else {
        $verses = $this->chapter->verses->where ('number', intval($verse_range));
      }

      $verses->load ([
        'cross_refs',
        'entity_refs',
        'entity_refs.entity',
        'frequencies',
        'mlt',
        'topic_refs',
      ]);

      $similar = null;

      $similar = $this->repository
        ->findWhereIn ('_id', array_keys($verses->pluck('mlt')->flatten()->pluck('similarity')->collapse()->sort()->reverse()->all()))
        ->all();

      //dd($this->verse_entity_groups ($this->chapter, $verses, $verse_range)->keys());

      $data = [
        'book'            => $book,
        'chapter'         => $this->chapter,
        'verse_range'     => $verse_range,
        'verses'          => $verses,
        'cross_refs'      => $verses->pluck ('cross_refs')->flatten()->sortBy('book_num'),
        'entity_groups'   => $this->verse_entity_groups ($this->chapter, $verses, $verse_range),
        'frequencies'     => $this->verse_frequencies ($this->chapter, $verses, $verse_range),
        'similar'         => collect($similar),
        'nav_menu'        => 'books',
        'subnav'          => $book->volume,
        'chapter_view'    => false,
      ];

      $this->store_cached_data ($request, $data);
      return $data;
    }

  }

}
