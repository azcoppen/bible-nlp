<?php

namespace BibleNLP\Processors;

use Illuminate\Http\Request;
use BibleNLP\Exceptions\NoCachedDataException;

class Browser {

  public function __construct () {

  }

  public function data_cache ( Request $request ) {
    if ( cache ()->has (md5 ($request->fullUrl()) ) ) {
      view ()->share ('DATA_CACHE', md5 ($request->fullUrl()));
      return cache ()->get (md5 ($request->fullUrl()) );
    }

    throw new NoCachedDataException;
  }

  public function store_cached_data ( Request $request, $data ) {
    cache ()->put (md5 ($request->fullUrl()), $data, 10);
  }

}
