<?php

namespace BibleNLP\Services;

use BibleNLP\Contracts\Services\BibleScraperContract;
use Symfony\Component\DomCrawler\Crawler;
use \GuzzleHttp\Client AS Guzzle;
use GuzzleHttp\Cookie\CookieJar;
use \Carbon\Carbon;
use \Exception;
use Illuminate\Support\Collection;

class BibleScraper implements BibleScraperContract {

  private $guzzle;
  private $api_url = 'https://www.biblegateway.com/passage/?version=NIV&search=';
  private $response;
  private $crawler;
  private $nodes;
  private $data = [];

  private $user_agents = [
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',
  ];

  public function __construct () {
    $this->guzzle = new Guzzle (['headers' => [
      'User-Agent'      => array_rand ($this->user_agents),
      'Accept'          => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
      'Accept-Encoding' => 'gzip, deflate, br',
    ]]);
  }

  public function book ( string $name ) : self {
    $this->book = $name;
    return $this;
  }

  public function chapter ( float $num ) : self {
    $this->chapter = $num;
    return $this;
  }

  public function scrape () : self {
    $this->request    = $this->guzzle->get ($this->api_url . urlencode($this->book).'+'.$this->chapter);
    $this->response   = (string) $this->request->getBody();
    $this->crawler    = new Crawler ( $this->response );

    $this->_extract ();
    $this->_package ();

    return $this;
  }

  private function _extract () : array {
    $this->nodes = $this->crawler->filter('.version-NIV p .text ')->each (function (Crawler $node, $i) {

      $first_space = strpos ($node->text(), ' ');

      if ( $i == 0 ) {
        $verse = 1;
      } else {
        $verse = substr ($node->text(), 0, $first_space);
      }

      $text = substr ($node->text(), $first_space);

      $text = trim(preg_replace('/\[.*?\]/', '', $text));

      return [
        'verse' => intval($verse),
        'text'  => ltrim ($text, ' '),
      ];
    });

    return $this->nodes;
  }

  private function _package () : array {
    $current_verse = 1;

    foreach ( $this->nodes AS $result ) {
      if ( $result['verse'] == 0 ) {
        $this->data[$current_verse] .= ' ' .$result['text'];
      } else {
        $current_verse = $result['verse'];
        $this->data[$current_verse] = $result['text'];
      }
    }

    return $this->data;
  }

  public function data () : Collection {
    return collect ($this->data);
  }

}
