<?php

namespace BibleNLP\Services;

use BibleNLP\Contracts\Services\NLPContract;
use DarrynTen\GoogleNaturalLanguagePhp\GoogleNaturalLanguage;
use \GuzzleHttp\Client AS Guzzle;
use \GuzzleHttp\RequestOptions;
use \Carbon\Carbon;
use Illuminate\Support\Collection;


class NLPAnalyser implements NLPContract {

  private $azure_url = 'https://eastus.api.cognitive.microsoft.com/text/analytics/v2.0/keyPhrases';
  private $google;
  private $azure;
  private $guzzle;
  private $text;
  public $google_result;
  public $azure_result;
  private $request;
  private $response;

  public function __construct () {
    $this->google = new GoogleNaturalLanguage ([
      'projectId'         => 'bible-nlp',
      'keyFilePath'       => storage_path(env('GOOGLE_JSON_KEY')),
      'cheapskate'        => false,
    ]);

    $this->guzzle = new Guzzle (['headers' => [
      'Accept'          => 'application/json',
      'Ocp-Apim-Subscription-Key' => env ('AZURE_TA_KEY1')
    ]]);
  }

  public function text ( string $text ) {
    $this->text = $text;
    return $this;
  }

  public function google () {
    $this->google->setText ($this->text);
    $this->google_result = $this->google->getEntities();
    return $this->google_result;
  }

  public function azure () {
    $payload = [
      'documents' => [
        [
          'language' => 'en',
          'id' => 1,
          'text' => $this->text,
        ]
      ]
    ];

    $this->request = $this->guzzle->post ($this->azure_url, [
      'json'  => $payload,
      'debug' => false,
    ]);

    $this->response   = (string) $this->request->getBody();
    $this->azure_result     = json_decode ($this->response);
    return $this->azure_result;
  }

}
