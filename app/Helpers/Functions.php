<?php

if ( !function_exists ('cdn') ) {
  function cdn ( string $path ) : string {
    return config ('app.cdn_url') . $path;
  }
}

if ( !function_exists ('highlight') ) {
  function highlight ($astring, $aword) {
  	if (!is_array ($aword) || !is_string ($astring) || empty ($aword) ) {
      	return false;
    }
    $aword = implode ('|', $aword);
    return preg_replace ('@\b('.$aword.')\b@si', '<span class="highlight">$1</span>', $astring);
  }
}

if ( !function_exists ('searchword') ) {
  function searchword ($astring, $aword, $css='') {
  	if (!is_array ($aword) || !is_string ($astring) || empty ($aword) ) {
      	return false;
    }
    $aword = implode ('|', $aword);
    return preg_replace ('@\b('.$aword.')\b@si', '<span class="badge badge-success mr-0 '.$css.'">$1</span>', $astring);
  }
}

if ( !function_exists ('quotations') ) {
  function quotations ( string $str ) {
    return str_replace (['“', '”'], '', $str);
  }
}

if ( !function_exists ('fix_utf8') ) {
  function fix_utf8 ( string $str ) {
    return preg_replace('/[^(\x20-\x7F)]*/','', $str);
  }
}

if ( !function_exists ('fix_utf8_apos') ) {
  function fix_utf8_apos ( string $str ) {
    return str_replace('ï¿½', '&apos;', utf8_encode ($str));
  }
}

if ( !function_exists ('entity_label') ) {
  function entity_label ( string $str ) {
    return title_case(str_replace ('_', ' ', $str));
  }
}

if ( !function_exists ('chapter_values_map') ) {
  function chapter_values_map ( $chaps, array $data ) {
    $mapped = [];
    for ( $i = 1; $i <= $chaps; $i++ ) {
      if ( array_key_exists ($i, $data) ) {
        $mapped[$i] = $data[$i];
      } else {
        $mapped[$i] = 0;
      }
    }
    return collect($mapped)->values()->all();
  }
}

if ( !function_exists ('pie_helper') ) {
  function pie_helper ( $val, $inc ) {
    $data = [];
    for($i=0; $i < $inc; $i++) {
      $data[] = $val;
    }
    return $data;
  }
}

if ( !function_exists ('entity_stats_finder') ) {
  function entity_stats_finder ( $stats, string $ent ) {
    foreach ($stats AS $type => $data) {
      foreach ($data AS $name => $total ) {
        if ( $ent == $name ) {
          return $type;
        }
      }
    }
  }
}
