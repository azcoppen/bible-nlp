<?php

namespace BibleNLP\Providers;

use Illuminate\Support\ServiceProvider;

use BibleNLP\Contracts\Services\NLPContract;
use BibleNLP\Services\NLPAnalyser;

class NLPServiceProvider extends ServiceProvider
{
    protected $defer = true;

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
      $this->app->bind ( NLPContract::class, function() {
          return new NLPAnalyser;
      });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [NLPContract::class];
    }
}
