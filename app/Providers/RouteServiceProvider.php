<?php

namespace BibleNLP\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

use BibleNLP\Repositories\BookRepository;
use BibleNLP\Repositories\EntityRepository;
use BibleNLP\Repositories\FrequencyRepository;
use BibleNLP\Repositories\TopicRepository;

use BibleNLP\Criteria\EntityReverseLookupCriteria;
use BibleNLP\Criteria\WhereFieldEqualsCriteria;

class RouteServiceProvider extends ServiceProvider
{

    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'BibleNLP\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {

        parent::boot();


        Route::pattern('ordering', '/(length|canonical|alphabetical|chronological)/i');
        Route::pattern('book', '/(genesis|exodus|leviticus|numbers|deuteronomy|joshua|judges|ruth|1-samuel|2-samuel|1-kings|2-kings|1-chronicles|2-chronicles|ezra|nehemiah|esther|job|psalms|proverbs|ecclesiastes|song-of-solomon|isaiah|jeremiah|lamentations|ezekiel|daniel|hosea|joel|amos|obadiah|jonah|micah|nahum|habakkuk|zephaniah|haggai|zechariah|malachi|matthew|mark|luke|john|acts|romans|1-corinthians|2-corinthians|galatians|ephesians|philippians|colossians|1-thessalonians|2-thessalonians|1-timothy|2-timothy|titus|philemon|hebrews|james|1-peter|2-peter|1-john|2-john|3-john|jude|revelation)/i');
        Route::pattern('volume', '/(OT|NT|both)/i');

        Route::bind('book', function ($value) {
             return app (BookRepository::class)->findByField('slug', $value)->first() ?? abort(404);
        });

        Route::bind('frequency', function ($value) {
             return app (FrequencyRepository::class)
              ->pushCriteria (new WhereFieldEqualsCriteria ('container_type', 'GLOBAL'))
              ->pushCriteria (new WhereFieldEqualsCriteria ('word', $value))
              ->first() ?? abort(404);
        });

        Route::bind('topic', function ($value) {
             return app (TopicRepository::class)->findByField('slug', $value)->first() ?? abort(404);
        });



    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
}
