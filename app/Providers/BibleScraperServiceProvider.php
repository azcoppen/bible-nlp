<?php

namespace BibleNLP\Providers;

use Illuminate\Support\ServiceProvider;
use BibleNLP\Contracts\Services\BibleScraperContract;
use BibleNLP\Services\BibleScraper;

class BibleScraperServiceProvider extends ServiceProvider
{
    protected $defer = true;

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
      $this->app->bind ( BibleScraperContract::class, function() {
          return new BibleScraper;
      });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [BibleScraperContract::class];
    }
}
