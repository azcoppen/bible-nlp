<?php

namespace BibleNLP\Providers;

use Illuminate\Support\ServiceProvider;
use BibleNLP\Contracts\Services\WordFrequencyContract;
use BibleNLP\Services\WordFrequency;

class WordFrequencyServiceProvider extends ServiceProvider
{
    protected $defer = true;

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
      $this->app->bind ( WordFrequencyContract::class, function() {
          return new WordFrequency;
      });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [WordFrequencyContract::class];
    }
}
