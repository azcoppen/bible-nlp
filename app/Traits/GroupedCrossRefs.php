<?php

namespace BibleNLP\Traits;

trait GroupedCrossRefs {

  public function getBookGroupedCrossRefsAttribute () {
    $refs = $this->cross_refs; // only do this once
    return $refs->sortBy('book_num')->groupBy('book_num')->transform(function ($item, $key) {
        return $item->sortBy('chap_num')->groupBy ('chap_num');
    });

  }
}
