<?php

namespace BibleNLP\Traits;

trait GroupedEntities {

  public function getEntitiesAttribute () {
    return $this->entity_refs->pluck ('entity');
  }

  public function getUniqueEntitiesAttribute () {
    return $this->entities->unique('name')->sortBy('name');
  }

  public function getEntityGroupsAttribute () {
    $entities = $this->entities; // only do it once
    return collect ([
      'people'        => $entities->where ('type', 'PERSON')->unique('name')->sortBy('name'),
      'organisations' => $entities->where ('type', 'ORGANIZATION')->unique('name')->sortBy('name'),
      'locations'     => $entities->where ('type', 'LOCATION')->unique('name')->sortBy('name'),
      'events'        => $entities->where ('type', 'EVENT')->unique('name')->sortBy('name'),
      'artworks'      => $entities->where ('type', 'WORK_OF_ART')->unique('name')->sortBy('name'),
      'goods'         => $entities->where ('type', 'CONSUMER_GOOD')->unique('name')->sortBy('name'),
      'other'         => $entities->where ('type', 'OTHER')->unique('name')->sortBy('name'),
    ]);
  }

}
