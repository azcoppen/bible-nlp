<?php

namespace BibleNLP\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class WhereFieldEqualsCriteria.
 *
 * @package namespace BibleNLP\Criteria;
 */
class GroupedTopicsCriteria implements CriteriaInterface
{
    private $letter;

    public function __construct ( $letter = null ) {
      $this->letter = $letter;

    }


    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->distinct('subject')->groupBy ( 'title' )->orderBy('title', 'ASC');
    }
}
