<?php

namespace BibleNLP\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class WhereFieldEqualsCriteria.
 *
 * @package namespace BibleNLP\Criteria;
 */
class TopTopicsCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->orderBy('refs_total', 'DESC')
          ->take (250);
    }
}
