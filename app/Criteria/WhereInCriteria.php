<?php

namespace BibleNLP\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class WhereFieldEqualsCriteria.
 *
 * @package namespace BibleNLP\Criteria;
 */
class WhereInCriteria implements CriteriaInterface
{
    private $field;
    private $value;

    public function __construct ( $field, $value ) {
      $this->field = $field;
      $this->value = $value;
    }


    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereIn ( $this->field, $this->value );
    }
}
