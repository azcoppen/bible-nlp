<?php

namespace BibleNLP\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class WhereFieldEqualsCriteria.
 *
 * @package namespace BibleNLP\Criteria;
 */
class GroupedCountCriteria implements CriteriaInterface
{
    private $field;
    private $matches;
    private $sum_field;

    public function __construct ( $field, $matches, $sum_field = null ) {
      $this->field = $field;
      $this->matches = $matches;
      $this->sum_field = $sum_field;
    }


    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if ($this->sum_field) {
          $group_data = [
          '$group' => [
              '_id'     => [$this->field => '$'.$this->field],
              'summed'  => ['$sum' => '$'.$this->sum_field ],
              'count'   => ['$sum' => 1],
            ]
          ];
       } else {
         $group_data = [
           '$group' => [
             '_id'     => [$this->field => '$'.$this->field],
             'count'   => ['$sum' => 1],
           ]
         ];
       }

      return $model->raw ( function ( $collection ) use ($group_data) {
        return $collection->aggregate ([
          ['$match'=> $this->matches],
          $group_data,
        ]);
      });
    }
}
