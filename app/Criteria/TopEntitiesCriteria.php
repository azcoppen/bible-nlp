<?php

namespace BibleNLP\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class WhereFieldEqualsCriteria.
 *
 * @package namespace BibleNLP\Criteria;
 */
class TopEntitiesCriteria implements CriteriaInterface
{
    private $type;

    public function __construct ( $type ) {
      $this->type = $type;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if ( $this->type != 'ALL' ) {
          return $model->where ( 'type', $this->type )
            ->orderBy('refs_total', 'DESC')
            ->take (250);
        } else {
          return $model->orderBy('refs_total', 'DESC')
            ->take (250);
        }
    }
}
