<?php

namespace BibleNLP\Http\Controllers;

use Illuminate\Http\Request;
use BibleNLP\Models\Verse AS Model;

use BibleNLP\Processors\Browsers\HomeBrowser;

class HomeController extends Controller
{
    public function test ( Request $request ) {

    }

    public function index ( Request $request, HomeBrowser $browser ) {
      return $this->view ('home', ['nav_menu'  => 'home',]);
    }
}
