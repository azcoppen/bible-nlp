<?php

namespace BibleNLP\Http\Controllers;

use Illuminate\Http\Request;
use BibleNLP\Processors\Browsers\Topic\TopicBrowser;


use BibleNLP\Repositories\SummaryRepository;
use BibleNLP\Repositories\TopicRepository;

use BibleNLP\Criteria\WhereInCriteria;
use BibleNLP\Criteria\WhereFieldEqualsCriteria;
use BibleNLP\Criteria\GroupedTopicsCriteria;
use BibleNLP\Criteria\TopTopicsCriteria;

use BibleNLP\Models\Topic;
use Illuminate\Pagination\LengthAwarePaginator;

use \Exception;

class TopicController extends Controller
{
  private $view_subdir = 'topics.';

  public function index ( Request $request, TopicBrowser $browser, string $az = null ) {
    try {

      $this->validate_az ($az);
      return $this->view ( $this->view_subdir . 'index', array_merge ([
        'topics' => $browser->prepare ($request, $az)],
        $browser->view_vars ($request, $az))
      );

    } catch ( Exception $e ) {
      return $this->page_exception_response ($e);
    }
  }

  public function show ( Request $request, TopicBrowser $browser, $az, Topic $topic, $subject = null ) {
    try {

      $this->validate_az ($az);
      return $this->view ( $this->view_subdir . 'show', $browser->setup ($request, $az, $topic, $subject));

    } catch ( Exception $e ) {
      return $this->page_exception_response ($e);
    }
  }
}
