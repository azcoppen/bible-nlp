<?php

namespace BibleNLP\Http\Controllers;

use Illuminate\Http\Request;

use BibleNLP\Processors\Browsers\Book\BookBrowser;
use BibleNLP\Processors\Browsers\Book\BookSearchBrowser;
use BibleNLP\Models\Book;

use \Exception;

class BookController extends Controller
{
    private $view_subdir = 'books.';
    public $routing = null;

    public function index ( Request $request, BookBrowser $browser, string $volume = null, string $ordering = null ) {
      try {

        if (!$ordering) {
          $ordering = 'canonical';
        }

        $this->validate_volume ($volume);
        $this->validate_vol_ordering ($ordering);

        return $this->view ( $this->view_subdir . 'index', array_merge ([
          'books' => $browser->prepare ( $request, $volume, $ordering ),
          ], $browser->view_vars ( $request, $volume, $ordering ))
        );

      } catch ( Exception $e ) {
        return $this->page_exception_response ($e);
      }
    }

    public function search ( Request $request, BookSearchBrowser $browser, Book $book, int $chapter = null ) {
      try {

        if ( $chapter ) {
          $this->validate_chapter ($book, $chapter);
        }

        if ( $request->has ('q') ) {
          return $this->view ( $this->view_subdir . 'search', array_merge (
            $browser->prepare ($request, $book, $chapter),
            $browser->view_vars ($request, $book, $chapter))
          );
        }

        return $this->view ( $this->view_subdir . 'search');
      } catch ( Exception $e ) {
        return $this->page_exception_response ($e);
      }
    }

    public function show ( Request $request, BookBrowser $browser, Book $book ) {
      try {
        return $this->view ( $this->view_subdir . 'show', $browser->setup ($request, $book));
      } catch ( Exception $e ) {
        return $this->page_exception_response ($e);
      }
    }
}
