<?php

namespace BibleNLP\Http\Controllers;

use Illuminate\Http\Request;

use BibleNLP\Processors\Browsers\Frequency\FrequencyBrowser;
use BibleNLP\Processors\Browsers\Frequency\BookFrequencyBrowser;
use BibleNLP\Processors\Browsers\Frequency\ChapterFrequencyBrowser;

use BibleNLP\Models\Book;
use BibleNLP\Models\Frequency;

use \Exception;

class FrequencyController extends Controller
{
  private $view_subdir = 'frequencies.';

  public function index ( Request $request, FrequencyBrowser $browser, string $az = null ) {
    try {

      $this->validate_az ($az);

      return $this->view ( $this->view_subdir . 'index', array_merge ([
          'frequencies' => $browser->prepare ($request, $az),
        ], $browser->view_vars ($request, $az))
      );

    } catch ( Exception $e ) {
      return $this->page_exception_response ($e);
    }
  }

  public function show ( Request $request, FrequencyBrowser $browser, string $az, Frequency $frequency ) {
    try {

      $this->validate_az ($az);
      return $this->view ( $this->view_subdir . 'show', $browser->setup ($request, $az, $frequency));

    } catch ( Exception $e ) {
      return $this->page_exception_response ($e);
    }
  }

  public function book ( Request $request, BookFrequencyBrowser $browser, string $az, Frequency $frequency, Book $book ) {
    try {

      $this->validate_az ($az);
      return $this->view ( $this->view_subdir . 'book', $browser->setup ($request, $az, $frequency, $book));

    } catch ( Exception $e ) {
      return $this->page_exception_response ($e);
    }
  }

  public function chapter ( Request $request, ChapterFrequencyBrowser $browser, string $az, Frequency $frequency, Book $book, int $chap_num ) {
    try {

      $this->validate_az ($az);
      $this->validate_chapter ($book, $chap_num);

      return $this->view ( $this->view_subdir . 'chapter', $browser->setup ($request, $az, $frequency, $book, $chap_num));

    } catch ( Exception $e ) {
      return $this->page_exception_response ($e);
    }
  }

}
