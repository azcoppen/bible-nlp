<?php

namespace BibleNLP\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use BibleNLP\Repositories\BookRepository;

use BibleNLP\Models\Book;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function view ( string $view_file, array $data = null ) {
      $preload = [
        'theme' => \Cookie::get('theme'),
        'BOOKS' => app (BookRepository::class)->all(),
        'entity_hex' => [
          'people'        => '#ff5722',
          'organisations' => '#ffc107',
          'locations'     => '#8bc34a',
          'events'        => '#03a9f4',
          'artworks'      => '#9C27B0',
          'goods'         => '#e91e63',
          'other'         => '#607d8b',
        ],
      ];

      if ( $data ) {
        $data = array_merge ($preload, $data);
      } else {
        $data = $preload;
      }

      return view ( $view_file, $data );
    }

    public function page_exception_response ( $e ) {
      throw $e;
    }

    public function validate_chapter ( Book $book, $chap_num = null ) {
      if ( !is_numeric ($chap_num) || $chap_num < 1 || $chap_num > $book->chaps ) {
        abort (404);
      }
    }

    public function validate_volume ( string $str = null ) {
      if (!is_null ($str) ) {
        if ( !in_array ($str, ['OT', 'NT', 'both']) ) {
          abort (404);
        }
      }
    }

    public function validate_vol_ordering ( string $str = null ) {
      if (!is_null ($str) ) {
        if ( !in_array ($str, ['alphabetical', 'canonical', 'chronological', 'length']) ) {
          abort (404);
        }
      }
    }

    public function validate_az ( string $str = null ) {
      if (!is_null ($str) ) {
        if ( !in_array ($str, array_merge (['all'], range ('A', 'Z'))) ) {
          abort (404);
        }
      }
    }

    public function validate_entity_type ( string $str = null ) {
      if (!is_null ($str) ) {
        if ( !in_array ($str, ['all', 'person', 'organization', 'location', 'event', 'consumer_good', 'work_of_art', 'other']) ) {
          abort (404);
        }
      }
    }

}
