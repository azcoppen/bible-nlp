<?php

namespace BibleNLP\Http\Controllers;

use Illuminate\Http\Request;

use BibleNLP\Processors\Browsers\Entity\EntityBrowser;
use BibleNLP\Processors\Browsers\Entity\BookEntityBrowser;
use BibleNLP\Processors\Browsers\Entity\ChapterEntityBrowser;

use BibleNLP\Models\Book;
use \Exception;

class EntityController extends Controller
{

  private $view_subdir = 'entities.';

  public function index ( Request $request, EntityBrowser $browser, string $type = 'all', string $az = null ) {
    try {

      $this->validate_entity_type ($type);
      $this->validate_az ($az);

      return $this->view ( $this->view_subdir . 'index', array_merge ([
        'entities'      => $browser->prepare ($request, $type, $az),
      ], $browser->view_vars ($request, $type, $az)));

    } catch ( Exception $e ) {
      return $this->page_exception_response ($e);
    }
  }

  public function show ( Request $request, EntityBrowser $browser, string $type, string $az, string $entity_slug ) {
    try {

      $this->validate_entity_type ($type);
      $this->validate_az ($az);

      return $this->view ( $this->view_subdir . 'show', $browser->setup ($request, $type, $az, $entity_slug));

    } catch ( Exception $e ) {
      return $this->page_exception_response ($e);
    }
  }

  public function book ( Request $request, BookEntityBrowser $browser, string $type, string $az, string $entity_slug, Book $book ) {
    try {

      $this->validate_entity_type ($type);
      $this->validate_az ($az);

      return $this->view ( $this->view_subdir . 'book', $browser->setup ($request, $type, $az, $entity_slug, $book));

    } catch ( Exception $e ) {
      return $this->page_exception_response ($e);
    }
  }

  public function chapter ( Request $request, ChapterEntityBrowser $browser, string $type, string $az, string $entity_slug, Book $book, int $chap_num ) {
    try {

      $this->validate_entity_type ($type);
      $this->validate_az ($az);
      $this->validate_chapter ($book, $chap_num);

      return $this->view ( $this->view_subdir . 'chapter', $browser->setup ($request, $type, $az, $entity_slug, $book, $chap_num));

    } catch ( Exception $e ) {
      return $this->page_exception_response ($e);
    }
  }


}
