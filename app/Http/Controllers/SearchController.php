<?php

namespace BibleNLP\Http\Controllers;

use Illuminate\Http\Request;
use BibleNLP\Processors\Browsers\Search\GlobalSearchBrowser;
use BibleNLP\Processors\Browsers\Search\GlobalSimilarBrowser;

use \Exception;

class SearchController extends Controller
{
    public $view_subdir = 'search.';

    public function index ( Request $request, GlobalSearchBrowser $browser ) {
      try {
        return $this->view ( $this->view_subdir . 'index', $browser->prepare ($request));
      } catch ( Exception $e ) {
        return $this->page_exception_response ($e);
      }
    }

    public function similar ( Request $request, GlobalSimilarBrowser $browser ) {
      try {
        return $this->view ( $this->view_subdir . 'similar', $browser->prepare ($request));
      } catch ( Exception $e ) {
        return $this->page_exception_response ($e);
      }
    }
}
