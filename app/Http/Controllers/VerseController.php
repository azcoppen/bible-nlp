<?php

namespace BibleNLP\Http\Controllers;

use Illuminate\Http\Request;
use BibleNLP\Processors\Browsers\Verse\VerseBrowser;
use BibleNLP\Processors\Browsers\Verse\VerseCrossBrowser;
use BibleNLP\Processors\Browsers\Verse\VerseSimilarBrowser;
use BibleNLP\Models\Book;

class VerseController extends Controller
{
  private $view_subdir = 'verses.';

  public function show ( Request $request, VerseBrowser $browser, Book $book, int $chapter, string $verse_range ) {
    try {

      $this->validate_chapter ($book, $chapter);

      if ( $browser->ready ($book,  $chapter, $verse_range) ) {
        return $this->view ( $this->view_subdir . 'show', $browser->setup ($request, $book, $chapter, $verse_range) );
      }

      abort (404);

    } catch ( Exception $e ) {
      return $this->page_exception_response ($e);
    }
  }

  public function crossrefs ( Request $request, VerseCrossBrowser $browser, Book $book, int $chapter, string $verse_range ) {
    try {

      $this->validate_chapter ($book, $chapter);

      if ( $browser->ready ($book,  $chapter, $verse_range) ) {
        return $this->view ( $this->view_subdir . 'crossrefs', $browser->setup ($request, $book,  $chapter, $verse_range) );
      }

      abort (404);

    } catch ( Exception $e ) {
      return $this->page_exception_response ($e);
    }
  }

  public function similar ( Request $request, VerseSimilarBrowser $browser, Book $book, int $chapter, string $verse_range ) {
    try {

      $this->validate_chapter ($book, $chapter);

      if ( $browser->ready ($book,  $chapter, $verse_range) ) {
        return $this->view ( $this->view_subdir . 'similar', $browser->setup ($request, $book,  $chapter, $verse_range) );
      }

      abort (404);

    } catch ( Exception $e ) {
      return $this->page_exception_response ($e);
    }
  }
}
