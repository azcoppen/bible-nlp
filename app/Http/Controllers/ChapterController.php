<?php

namespace BibleNLP\Http\Controllers;

use Illuminate\Http\Request;
use BibleNLP\Processors\Browsers\Chapter\ChapterBrowser;
use BibleNLP\Models\Book;

use \Exception;

class ChapterController extends Controller
{
  private $view_subdir = 'verses.';

  public function show ( Request $request, ChapterBrowser $browser, Book $book, int $chapter ) {
    try {

      $this->validate_chapter ($book, $chapter);

      if ( $browser->ready ($book, $chapter) ) {
        return $this->view ( $this->view_subdir . 'show', $browser->setup ( $request, $book, $chapter ));
      }

      abort (404);

    } catch ( Exception $e ) {
      return $this->page_exception_response ($e);
    }
  }

}
