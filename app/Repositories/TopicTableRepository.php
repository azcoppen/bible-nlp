<?php

namespace BibleNLP\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use BibleNLP\Contracts\Repositories\TopicTableRepositoryContract;
use BibleNLP\Models\TopicTable;
use BibleNLP\Validators\TopicTableValidator;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class TopicRepositoryEloquent.
 *
 * @package namespace BibleNLP\Repositories;
 */
class TopicTableRepository extends BaseRepository implements TopicTableRepositoryContract, CacheableInterface
{
    use CacheableRepository;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return TopicTable::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
