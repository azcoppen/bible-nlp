<?php

namespace BibleNLP\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use BibleNLP\Contracts\Repositories\CrossRefRepositoryContract;
use BibleNLP\Models\CrossRef;
use BibleNLP\Validators\CrossRefValidator;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class ReferenceRepositoryEloquent.
 *
 * @package namespace BibleNLP\Repositories;
 */
class CrossRefRepository extends BaseRepository implements CrossRefRepositoryContract, CacheableInterface
{
    use CacheableRepository;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CrossRef::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
