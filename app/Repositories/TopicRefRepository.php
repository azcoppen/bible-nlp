<?php

namespace BibleNLP\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use BibleNLP\Contracts\Repositories\TopicRefRepositoryContract;
use BibleNLP\Models\TopicRef;
use BibleNLP\Validators\TopicRefValidator;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class TopicRefRepositoryEloquent.
 *
 * @package namespace BibleNLP\Repositories;
 */
class TopicRefRepository extends BaseRepository implements TopicRefRepositoryContract, CacheableInterface
{
    use CacheableRepository;
    
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return TopicRef::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
