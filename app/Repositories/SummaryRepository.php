<?php

namespace BibleNLP\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use BibleNLP\Contracts\Repositories\SummaryRepositoryContract;
use BibleNLP\Models\Summary;
use BibleNLP\Validators\SummaryValidator;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class TopicRepositoryEloquent.
 *
 * @package namespace BibleNLP\Repositories;
 */
class SummaryRepository extends BaseRepository implements SummaryRepositoryContract, CacheableInterface
{
    use CacheableRepository;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Summary::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
