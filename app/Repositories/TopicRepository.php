<?php

namespace BibleNLP\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use BibleNLP\Contracts\Repositories\TopicRepositoryContract;
use BibleNLP\Models\Topic;
use BibleNLP\Validators\TopicValidator;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class TopicRepositoryEloquent.
 *
 * @package namespace BibleNLP\Repositories;
 */
class TopicRepository extends BaseRepository implements TopicRepositoryContract, CacheableInterface
{
    use CacheableRepository;
    
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Topic::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
