<?php

namespace BibleNLP\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use BibleNLP\Contracts\Repositories\AnalysisRepositoryContract;
use BibleNLP\Models\Analysis;
use BibleNLP\Validators\AnalysisValidator;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class AnalysisRepositoryEloquent.
 *
 * @package namespace BibleNLP\Repositories;
 */
class AnalysisRepository extends BaseRepository implements AnalysisRepositoryContract, CacheableInterface
{
    use CacheableRepository;
    
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Analysis::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
