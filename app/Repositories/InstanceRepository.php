<?php

namespace BibleNLP\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use BibleNLP\Contracts\Repositories\InstanceRepositoryContract;
use BibleNLP\Models\Instance;
use BibleNLP\Validators\InstanceValidator;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class LexiconRepositoryEloquent.
 *
 * @package namespace BibleNLP\Repositories;
 */
class InstanceRepository extends BaseRepository implements InstanceRepositoryContract, CacheableInterface
{
    use CacheableRepository;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Instance::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
