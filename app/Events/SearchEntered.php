<?php

namespace BibleNLP\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SearchEntered
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $container_type;
    public $container_id;
    public $q;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct( $container_type, $container_id = null, string $q = null )
    {
        $this->container_type = $container_type;
        $this->container_id = $container_id;
        $this->q = $q;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
