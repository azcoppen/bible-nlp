<?php

namespace BibleNLP\Search\Configurators;

use ScoutElastic\IndexConfigurator;
use ScoutElastic\Migratable;

class TopicIndexConfigurator extends IndexConfigurator
{
    use Migratable;

    protected $name = 'rb.topics';

    /**
     * @var array
     */
     protected $settings = [
       "analysis" => [
         "analyzer" => [
           "partial_match_analyzer" => [
             "tokenizer" => "standard",
             "filter" => [ "ngram_filter", "lowercase" ]
           ]
         ],
         "filter" => [
           "ngram_filter" => [
             "type" => "nGram",
             "min_gram" => 2,
             "max_gram" => 2
           ]
         ]
       ]
     ];
}
