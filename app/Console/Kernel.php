<?php

namespace BibleNLP\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\ScrapeBook::class,
        Commands\BuildNLPData::class,
        Commands\BuildFrequencyData::class,
        Commands\BuildLexiconData::class,
        Commands\ImportNLPData::class,
        Commands\BuildAnalysisRecords::class,
        Commands\BuildEntityData::class,
        Commands\BuildEntityRefs::class,
        Commands\BuildInstanceRefs::class,
        Commands\BuildCrossRefs::class,
        Commands\AlphabetizeTokens::class,
        Commands\TagVersesCanonically::class,
        Commands\BuildTopics::class,
        Commands\BuildTopicRefs::class,
        Commands\CleanupTopicRefs::class,
        Commands\FixTopicRefIDs::class,
        Commands\AlphabetizeTopics::class,
        Commands\AddDatesToBooks::class,
        Commands\AddShortcutsToVerses::class,
        Commands\CheckMissingVerses::class,
        Commands\AddShortcutsToCrossRefs::class,
        Commands\AddShortcutsToTopicRefs::class,
        Commands\AddShortcutsToFrequencies::class,
        Commands\AddSlugToEntities::class,
        Commands\AddTotalToEntities::class,
        Commands\AddShortcutsToEntityRefs::class,
        Commands\OrderCrossRefs::class,
        Commands\AddCountsToEntityRefs::class,
        Commands\ParseChapSummariesFromTSK::class,
        Commands\ImportChapSummaries::class,
        Commands\BuildTopicTable::class,
        Commands\BuildVerseMLTData::class,
        Commands\AddStatsToBooks::class,
        Commands\TestBookStats::class,
        Commands\AddPercentToBooks::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
