<?php

namespace BibleNLP\Console\Commands;

use Illuminate\Console\Command;
use BibleNLP\Repositories\EntityRepository;
use BibleNLP\Models\Verse;

class AddTotalToEntities extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'entities:totals';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->info ("Totalling entities...");
        $entities = app (EntityRepository::class)->with(['entity_refs' => function ($query) {
          return $query->where ('reference_type', Verse::class);
        }])->all();

        $bar = $this->output->createProgressBar(count($entities));
        foreach ($entities AS $entity) {
          $entity->update ([
            'refs_total' => $entity->entity_refs->count(),
          ]);
          $bar->advance();
        }
        $bar->finish();
        $this->info ("\n");

    }
}
