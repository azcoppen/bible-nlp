<?php

namespace BibleNLP\Console\Commands;

use Illuminate\Console\Command;
use BibleNLP\Contracts\Services\BibleScraperContract;
use BibleNLP\Repositories\BookRepository;

class ScrapeBook extends Command
{
    private $book;
    private $chapters;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'books:csv';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scrapes all text from BibleGateway into CSV & Json files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    private function save ( string $name ) {
      $fp = fopen (storage_path($name.'.csv'), 'wb');
      foreach ( $this->chapters as $chapter => $verse_data ) {
        foreach ($verse_data AS $num => $text ) {
          fputcsv ($fp, [
            $chapter,
            $num,
            $text
          ]);
        }
      }

      fclose($fp);

      file_put_contents (storage_path($name.'.json'), json_encode ($this->chapters, JSON_PRETTY_PRINT));

      $this->info ("Saved to ".storage_path($name.'.csv') . ' and ' . storage_path($name.'.json'));
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $bible = app (BookRepository::class)->all ();

      foreach ( $bible AS $index => $book ) {

        $this->info ("Scraping: ".$book->title);

        $bar = $this->output->createProgressBar($book->chaps);
        $this->chapters = [];

        for ( $i = 1; $i <= $book->chaps; $i++ ) {
          $this->chapters [$i] = app (BibleScraperContract::class)
            ->book ($book->title)
            ->chapter ($i)
            ->scrape ()
            ->data ();
          $bar->advance();
        }

        $bar->finish();
        $this->info ("\n");

        $this->save ( 'NIV'.str_pad (($index + 1), 3, '0', STR_PAD_LEFT).'-' . strtoupper (str_slug ($book->title)) );
      } // end foreach bible

    }
}
