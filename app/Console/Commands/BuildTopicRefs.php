<?php

namespace BibleNLP\Console\Commands;

use Illuminate\Console\Command;
use BibleNLP\Repositories\TopicRepository;
use BibleNLP\Repositories\TopicRefRepository;

class BuildTopicRefs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'build:topicrefs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Builds topic references from topic data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function topic ( $viz_topic_name ) {
      $check = \DB::collection ('topics')->where ('title', $viz_topic_name)->first();
      return $check;
    }

    public function verse ( $viz_verse_id ) {
      $viz_verse = \DB::collection ('viz_verses')->where ('VerseID', $viz_verse_id)->first();
      return \DB::collection ('verses')->where ('crossref_str', $viz_verse['OsisRef'])->first();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $viz_topics = \DB::collection ('viz_topic_index')->get();

      $bar = $this->output->createProgressBar(count($viz_topics));

      foreach ($viz_topics AS $vt) {

        $viz_topic = \DB::collection ('viz_topics')->where ('TopicID', $vt['TopicID'])->first();

        $topic = $this->topic ($viz_topic['Topic']);
        $verse = $this->verse ($vt['VerseID']);

        //$this->line ($viz_topic['Topic']." (".$viz_topic['Subtopic'].") - ".$verse['text']['en']);

        app (TopicRefRepository::class)->create ([
          'topic_id'       => $topic['_id'],
          'verse_id'       => $verse['_id'],
          'title'          => $viz_topic['Topic'],
          'title_slug'     => str_slug ($viz_topic['Topic']),
          'subject'        => $viz_topic['Subtopic'],
          'subject_slug'   => str_slug ($viz_topic['Subtopic']),
        ]);


        $bar->advance ();
      }
      $bar->finish();
    }
}
