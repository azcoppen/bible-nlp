<?php

namespace BibleNLP\Console\Commands;

use Illuminate\Console\Command;
use BibleNLP\Repositories\BookRepository;
use BibleNLP\Repositories\FrequencyRepository;
use BibleNLP\Repositories\InstanceRepository;
use BibleNLP\Criteria\WhereFieldEqualsCriteria;

class BuildInstanceRefs extends Command
{
    private $lexicon;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'build:instances';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Connects instances of words to global lexicon';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function lookup ( $word ) {
      return $this->lexicon->where ('word', $word)->first ();
    }

    public function add_ref ( $frequency, $ref ) {
      return app (InstanceRepository::class)->create ([
        'frequency_id'    => $frequency->id,
        'reference_type'  => get_class ($ref),
        'reference_id'    => $ref->id,
      ]);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $bible = app (BookRepository::class)->with (['chapters', 'chapters.verses'])->all ();

      $this->lexicon = app (FrequencyRepository::class)
        ->pushCriteria ( new WhereFieldEqualsCriteria ('container_type', 'GLOBAL') )
        ->all ();

      $this->info ("Loaded ".$this->lexicon->count(). ' words from global index.');

      foreach ( $bible AS $index => $book ) {
        $this->info ("Referencing instances for: ".$book->title);

        $bar = $this->output->createProgressBar(count($book->chapters));

        foreach ( $book->chapters AS $chapter ) {

          if ( isset ($chapter->frequencies) && count ($chapter->frequencies) ) {

            $slice = $this->lexicon->whereIn ('word', array_keys ($chapter->frequencies) );

            foreach ( $chapter->frequencies AS $chap_word => $ct ) {
              $lex_ref = $slice->where ('word', $chap_word)->first ();
              if ( $lex_ref && is_object ($lex_ref) ) {
                $this->add_ref ($lex_ref, $chapter);
              }
            }
          }

          foreach ( $chapter->verses AS $verse ) {

            if ( isset ($verse->frequencies) && count ($verse->frequencies) ) {

              $slice = $this->lexicon->whereIn ('word', array_keys ($verse->frequencies) );

              foreach ( $verse->frequencies AS $word => $c ) {
                $lexicon_ref = $slice->where ('word', $word)->first ();
                if ( $lexicon_ref && is_object ($lexicon_ref) ) {
                  $this->add_ref ($lexicon_ref, $verse);
                }
              }
            }



          } // end foreach verse

          $bar->advance();
          $this->info ("\n");

        }

      $bar->finish();
      $this->info ("\n");


      }
    }
}
