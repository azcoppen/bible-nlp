<?php

namespace BibleNLP\Console\Commands;

use Illuminate\Console\Command;
use BibleNLP\Repositories\VerseRepository;

class AddShortcutsToVerses extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'verses:shortcuts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $verses = app (VerseRepository::class)->with(['chapter', 'chapter.book'])->all();
        foreach ( $verses AS $verse ) {
          $verse->update ([
            'book_num'  => $verse->chapter->book->order,
            'book_title' => $verse->chapter->book->title,
            'book_slug' => $verse->chapter->book->slug,
            'chap_num' => $verse->chapter->number,
          ]);
          $this->line ("Updated ".$verse->canonical);
        }
    }
}
