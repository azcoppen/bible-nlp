<?php

namespace BibleNLP\Console\Commands;

use Illuminate\Console\Command;
use BibleNLP\Repositories\BookRepository;
use BibleNLP\Repositories\EntityRepository;
use BibleNLP\Models\Book;

use BibleNLP\Criteria\OrderByCriteria;

class AddPercentToBooks extends Command
{


    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'books:percent';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $total_words = Book::sum('num_words');
      $total_pc = 0;

          foreach (Book::orderBy('order', 'ASC')->get () AS $book) {
            $pc = round(($book->num_words / $total_words) * 100, 2);
            $this->line ($book->title.': '. $pc . '%');
            $total_pc += $pc;
            $book->update ([
              'percent' => $pc
            ]);
          }

        $this->info ($total_pc);

    }
}
