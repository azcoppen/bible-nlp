<?php

namespace BibleNLP\Console\Commands;

use Illuminate\Console\Command;
use BibleNLP\Repositories\BookRepository;
use BibleNLP\Repositories\EntityRepository;
use BibleNLP\Models\Book;

use BibleNLP\Criteria\OrderByCriteria;

class TestBookStats extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'books:test-entities';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }



    public function entities ($book) {
      $chapters = $book->chapters;
      $chapters->load (['entity_refs']);
      $grouped = $chapters->pluck('entity_refs')->collapse()->groupBy('entity_id');

      $totals = [];
      // grouped means a chapter, theoretcially
      foreach ($grouped AS $entity_id => $refs) {
        $unique_chapters = $refs->unique('chap_num')->sortBy('chap_num');
        $entity = app (EntityRepository::class)->find($entity_id);
        $entity_records = $unique_chapters->where('entity_id', $entity_id);
        foreach ($entity_records AS $record) {
          $this->line ('['.$entity->name.']'.' Chapter: '.$record->chap_num.' mentions: '.$record->mentions);
        }
        $this->info ('sum mentions:' . $entity_records->sum('mentions'));
        $totals[$entity->type][$entity->name] = $entity_records->sum('mentions'); // has one chapter
        $this->line ("\n");
      }

      $final = [];

      foreach ($totals AS $type => $data) {
        arsort($totals[$type]);
        $finals[$type] = array_slice ($totals[$type], 0, 25);
      }
      return $finals;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $book = Book::where('slug', '1-peter')->with(['chapters'])->first();


      $this->line ('Analysing '.$book->title);
      $entities = $this->entities ($book);
      file_put_contents (storage_path($book->slug.'-'.uniqId().'.json'), json_encode ($entities, JSON_PRETTY_PRINT));
      $total = collect($entities)->collapse()->count();
      $this->info ('Collated '.$total.' entities.');
    }
}
