<?php

namespace BibleNLP\Console\Commands;

use Illuminate\Console\Command;
use BibleNLP\Repositories\BookRepository;
use BibleNLP\Repositories\EntityRepository;
use BibleNLP\Criteria\WhereFieldEqualsCriteria;

class BuildEntityData extends Command
{
    private $added = [];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'build:entities';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates the master list of entities';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function add_entities ( $data ) {
      foreach ( $data AS $entity ) {

        $check = app (EntityRepository::class)
          ->pushCriteria ( new WhereFieldEqualsCriteria ('name', $entity['name']) )
          ->pushCriteria ( new WhereFieldEqualsCriteria ('type', $entity['type']) )
          ->all ();

        if ( count ($check) < 1 ) {
          app (EntityRepository::class)->create ([
            'name' => $entity['name'],
            'type' => $entity['type'],
          ]);
        }

      }
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $bible = app (BookRepository::class)->with (['chapters', 'chapters.verses'])->all ();

      foreach ( $bible AS $index => $book ) {
        $this->info ("Extracting entities for: ".$book->title);


        foreach ( $book->chapters AS $chapter ) {

          $this->info ("Chapter: ".$chapter->number);

          if ( isset ($chapter->analysis) ) {
            $this->add_entities ($chapter->analysis);
          }

          $bar = $this->output->createProgressBar($book->chaps);

          foreach ( $chapter->verses AS $verse ) {

            if ( isset ($verse->analysis) ) {
              $this->add_entities ($verse->analysis);
            }

            $bar->advance();
            $this->info ("\n");

          } // end foreach verse

          $bar->finish();
          $this->info ("\n");

        }

      }
    }
}
