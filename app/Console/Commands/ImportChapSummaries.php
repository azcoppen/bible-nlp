<?php

namespace BibleNLP\Console\Commands;

use Illuminate\Console\Command;
use BibleNLP\Repositories\BookRepository;
use BibleNLP\Models\Chapter;

class ImportChapSummaries extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tsk:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $books = app (BookRepository::class)->with(['chapters'])->all();

      $row = 1;
      if (($handle = fopen(database_path('data/chapter_summaries.csv'), "rb")) !== FALSE) {
        while (($data = fgetcsv($handle, 2000, ",")) !== FALSE) {

          $b = $books->where ('order', $data[0])->first();
          $c = $b->chapters->where ('number', $data[1])->first();

          \DB::collection ('summaries')->insert ([
            'book_id' => (string) $b->id,
            'container_type' => Chapter::class,
            'container_id' => (string) $c->id,
            'book_num' => $data[0],
            'chap_num' => $data[1],
            'text' => [
              'en' => $data[2]
            ]
          ]);

          $this->line ('Updated '.$data[0].':'.$data[1].': '.$data[2]);
          $row++;
        }
        fclose($handle);
      }

    }
}
