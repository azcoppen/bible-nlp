<?php

namespace BibleNLP\Console\Commands;

use Illuminate\Console\Command;
use BibleNLP\Repositories\EntityRefRepository;
use BibleNLP\Models\Chapter;
use BibleNLP\Models\EntityRef;
use BibleNLP\Models\Verse;

class AddShortcutsToEntityRefs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'entityrefs:shortcuts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // THIS ONLY GOT TO REV 22:21
        EntityRef::chunk(1000, function ($refs) {

          $bar = $this->output->createProgressBar(count($refs));


          foreach ( $refs AS $ref ) {

            if ( $ref->reference_type == Chapter::class ) {
              $ref->load (['chapter', 'chapter.book', 'entity']);
              $ref->update ([
                'az'        => $ref->entity->az,
                'book_num'  => isset($ref->chapter->book) && is_object ($ref->chapter->book) ? $ref->chapter->book->order : 0,
                'book_title'=> isset($ref->chapter->book) && is_object ($ref->chapter->book) ? $ref->chapter->book->title : '',
                'book_slug' => isset($ref->chapter->book) && is_object ($ref->chapter->book) ? $ref->chapter->book->slug : '',
                'chap_num'  => isset($ref->chapter) && is_object ($ref->chapter) ? $ref->chapter->number : 0,
                'canon_ref' => isset($ref->chapter) && is_object ($ref->chapter) ? $ref->chapter->canonical : '',
              ]);
            }

            if ( $ref->reference_type == Verse::class ) {
              $ref->load (['verse','verse.chapter', 'verse.chapter.book', 'entity']);
              $ref->update ([
                'az'        => $ref->entity->az,
                'book_num'  => isset($ref->verse->chapter->book) && is_object ($ref->verse->chapter->book) ? $ref->verse->chapter->book->order : 0,
                'book_title'=> isset($ref->verse->chapter->book) && is_object ($ref->verse->chapter->book) ? $ref->verse->chapter->book->title : '',
                'book_slug' => isset($ref->verse->chapter->book) && is_object ($ref->verse->chapter->book) ? $ref->verse->chapter->book->slug : '',
                'chap_num'  => isset($ref->verse->chapter) && is_object ($ref->verse->chapter) ? $ref->verse->chapter->number : 0,
                'canon_ref' => isset($ref->verse) && is_object ($ref->verse) ? $ref->verse->canonical : '',
              ]);
            }

            //$this->line ("Updated ".$ref->id);
            $bar->advance();
          }

          $bar->finish();
          $this->info ("\n");

        });

    }
}
