<?php

namespace BibleNLP\Console\Commands;

use Illuminate\Console\Command;
use BibleNLP\Repositories\BookRepository;
use BibleNLP\Repositories\FrequencyRepository;

class BuildFrequencyData extends Command
{
    private $chapter_freqs;
    private $global_freqs = [];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'build:frequencies';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'word frequencies per book/chapter';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function add_chapter_words ( $freqs ) {
      if ( $freqs ) {
        foreach ( $freqs AS $word => $c ) {
          if ( array_key_exists ($word, $this->chapter_freqs) ) {
            $this->chapter_freqs[$word] += $c;
          } else {
            $this->chapter_freqs[$word] = $c;
          }
        }
      }
      return false;
    }

    public function add_global_words ( $freqs ) {
      if ( $freqs ) {
        foreach ( $freqs AS $word => $c ) {
          if ( array_key_exists ($word, $this->global_freqs) ) {
            $this->global_freqs[$word] += $c;
          } else {
            $this->global_freqs[$word] = $c;
          }
        }
      }
      return false;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $bible = app (BookRepository::class)->with (['chapters', 'chapters.verses'])->all ();

      foreach ( $bible AS $index => $book ) {
        $this->chapter_freqs = [];
        $this->info ("Building frequencies for: ".$book->title);


        foreach ( $book->chapters AS $chapter ) {

          if ( $chapter->frequencies ) {
            $this->add_chapter_words ($chapter->frequencies);
          }

          $this->line ("\t\tAdding chapter frequencies (".$chapter->number.")...");
          foreach ($chapter->frequencies AS $wd => $i ) {
            app ( FrequencyRepository::class )->create ([
              'container_type' => get_class ($chapter),
              'container_id'   => $chapter->id,
              'word'           => $wd,
              'total'          => $i,
            ]);
          }

          foreach ( $chapter->verses AS $verse ) {
            $this->line ("\t\t\tAdding verse frequencies (".$verse->number.")...");
            foreach ($verse->frequencies AS $w => $j ) {
              app ( FrequencyRepository::class )->create ([
                'container_type' => get_class ($verse),
                'container_id'   => $verse->id,
                'word'           => $w,
                'total'          => $j,
              ]);
            }
          }
        }

        // BUILD NEW BOOK LEXICON
        arsort ($this->chapter_freqs);

        $this->line ("\tAdding aggregated book frequencies for ".$book->title);
        foreach ( $this->chapter_freqs AS $word => $c ) {
          app ( FrequencyRepository::class )->create ([
            'container_type' => get_class ($book),
            'container_id'   => $book->id,
            'word'           => $word,
            'total'          => $c,
          ]);
        }

        $this->add_global_words ($this->chapter_freqs);

        $this->info ("\n");
      }

      // BUILD GLOBAL FREQS
      $this->info ("Adding GLOBAL frequencies for entire NIV...");
      foreach ( $this->global_freqs AS $word => $c ) {
        app ( FrequencyRepository::class )->create ([
          'container_type' => 'GLOBAL',
          'container_id'   => 0,
          'word'           => $word,
          'total'          => $c,
        ]);
      }


    }
}
