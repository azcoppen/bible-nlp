<?php

namespace BibleNLP\Console\Commands;

use Illuminate\Console\Command;
use BibleNLP\Repositories\TopicRefRepository;

class AddShortcutsToTopicRefs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'topicrefs:shortcuts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // THIS ONLY GOT TO REV 22:21
        $refs = app (TopicRefRepository::class)->with(['verse', 'verse.chapter', 'verse.chapter.book'])->all();
        foreach ( $refs AS $ref ) {

          $ref->update ([
            'book_num'  => isset($ref->verse->chapter->book) && is_object ($ref->verse->chapter->book) ? $ref->verse->chapter->book->order : 0,
            'book_title'=> isset($ref->verse->chapter->book) && is_object ($ref->verse->chapter->book) ? $ref->verse->chapter->book->title : '',
            'book_slug' => isset($ref->verse->chapter->book) && is_object ($ref->verse->chapter->book) ? $ref->verse->chapter->book->slug : '',
            'chap_num'  => isset($ref->verse->chapter) && is_object ($ref->verse->chapter) ? $ref->verse->chapter->number : 0,
            'canon_ref' => isset($ref->verse) && is_object ($ref->verse) ? $ref->verse->canonical : '',
          ]);
          $this->line ("Updated ".$ref->id);
        }
    }
}
