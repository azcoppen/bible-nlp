<?php

namespace BibleNLP\Console\Commands;

use Illuminate\Console\Command;
use BibleNLP\Repositories\TopicRefRepository;

class FixTopicRefIDs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix:topicrefs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Changes IDs to strings';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $refs = app (TopicRefRepository::class)->all();

      $bar = $this->output->createProgressBar(count($refs));

      foreach ($refs AS $ref) {

        $letter = strtoupper(substr($ref->title, 0, 1));

        $ref->update ([
          'topic_id' => (string) $ref->topic_id,
          'verse_id' => (string) $ref->verse_id,
          'az' => $letter
        ]);


        $bar->advance();
      }

      $bar->finish();
    }
}
