<?php

namespace BibleNLP\Console\Commands;

use Illuminate\Console\Command;
use BibleNLP\Repositories\BookRepository;
use BibleNLP\Repositories\EntityRepository;
use BibleNLP\Models\Book;

use BibleNLP\Criteria\OrderByCriteria;

class AddStatsToBooks extends Command
{
    private $wc = [
      1 => 32046,
      2 => 25957,
      3 => 18852,
      4 => 25048,
      5 => 23008,
      6 => 15671,
      7 => 15385,
      8 => 2039,
      9 => 20837,
      10 =>	17170,
      11 => 20361,
      12 =>	18784,
      13 =>	16664,
      14 =>	21349,
      15 =>	5605,
      16 =>	8507,
      17 =>	4932,
      18 =>	12674,
      19 =>	30147,
      20 =>	9921,
      21 =>	4537,
      22 =>	2020,
      23 =>	25608,
      24 =>	33002,
      25 =>	2324,
      26 =>	29918,
      27 =>	9001,
      28 =>	3615,
      29 =>	1447,
      30 =>	3027,
      31 =>	440,
      32 =>	1082,
      33 =>	2118,
      34 =>	855,
      35 =>	1011,
      36 =>	1141,
      37 =>	926,
      38 =>	4855,
      39 =>	1320,
      40 =>	18346,
      41 =>	11304,
      42 =>	19482,
      43 =>	15635,
      44 =>	18450,
      45 =>	7111,
      46 => 6830,
      47 =>	4477,
      48 =>	2230,
      49 =>	2422,
      50 =>	1629,
      51 =>	1582,
      52 =>	1481,
      53 =>	823,
      54 =>	1591,
      55 =>	1238,
      56 =>	659,
      57 =>	335,
      58 => 4953,
      59 => 1742,
      60 =>	1684,
      61 =>	1099,
      62 => 2141,
      63 =>	245,
      64 =>	219,
      65 =>	461,
      66 =>	9851,
    ];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'books:stats';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function verse_count ($book) {
      $total = 0;
      $chapters = $book->chapters;
      $chapters->load(['verses']);
      foreach ($chapters AS $chap) {
        $total += $chap->verses->count();
      }
      return $total;
    }

    public function entities ($book) {
      $chapters = $book->chapters;
      $grouped = $chapters->pluck('entity_refs')->collapse()->groupBy('entity_id');

      $totals = [];
      // grouped means a chapter, theoretcially
      foreach ($grouped AS $entity_id => $refs) {
        $unique_chapters = $refs->unique('chap_num')->sortBy('chap_num');
        $entity = app (EntityRepository::class)->find($entity_id);

        $entity_records = $unique_chapters->where('entity_id', $entity_id);
        $totals[$entity->type][$entity->name] = $entity_records->sum('mentions'); // has one chapter
      }

      return $totals;
    }

    public function frequencies ($book) {
      $chapters = $book->chapters;
      $grouped = $chapters->pluck('frequencies')->collapse()->groupBy('word');
      $totals = [];
      foreach ($grouped AS $word => $instances) {
        $unique_chapters = $instances->unique('chap_num')->sortBy('chap_num');
        $instance_records = $unique_chapters->where('word', $word);
        $totals[$word] = $instance_records->sum('total');
      }
      arsort($totals);
      return array_slice ($totals, 0, 25);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //$books = app (BookRepository::class)->with(['chapters', 'chapters.verses', 'chapters.frequencies', 'chapters.entity_refs', 'chapters.entity_refs.entity'])->all();
        $query = Book::orderBy('order', 'ASC')->with(['chapters', 'chapters.entity_refs', 'chapters.frequencies'])->chunk(1, function($books) {
          foreach ($books AS $book) {
            $this->line ('Analysing '.$book->title);
            $entities = $this->entities ($book);

            $top_entities = [];

            foreach ($entities AS $type => $data) {
              arsort($entities[$type]);
              $top_entities[$type] = array_slice ($entities[$type], 0, 25);
            }

            $words = $this->frequencies($book);
            $data = [
              //'num_words'     => $this->wc[$book->order],
              //'num_verses'    => $this->verse_count ($book),
              'num_entities'  => collect($entities)->collapse()->count(),
              'top_entities'  => $top_entities,
              'top_freqs'     => $words,
            ];
            $book->update ($data);
            file_put_contents (storage_path($book->slug.'-entities.json'), json_encode ($entities, JSON_PRETTY_PRINT));
            file_put_contents (storage_path($book->slug.'-frequencies.json'), json_encode ($words, JSON_PRETTY_PRINT));
            $this->line ("Updated ".$book->title);
          }
        });

    }
}
