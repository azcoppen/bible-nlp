<?php

namespace BibleNLP\Console\Commands;

use Illuminate\Console\Command;
use BibleNLP\Repositories\BookRepository;

class ImportNLPData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:nlp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imports NLP JSON result files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $bible = app (BookRepository::class)->with (['chapters', 'chapters.verses'])->all ();

        foreach ( $bible AS $index => $book ) {
          $this->info ("Importing JSON analysis for: ".$book->title);


          foreach ( $book->chapters AS $chapter ) {

            $this->info ("Chapter: ".$chapter->number);
            $folder = 'NIV' . str_pad (intval($index + 1), 3, '0', STR_PAD_LEFT) .'-'. strtoupper (str_slug ($book->title));
            $this->line ("Cycling to folder: ".$folder);

            foreach ( $chapter->verses AS $verse ) {

              $fn = 'NIV' . str_pad (intval($index + 1), 3, '0', STR_PAD_LEFT) .
              '-'. strtoupper (str_slug ($book->title)) .
              '-' . str_pad ($chapter->number, 3, '0', STR_PAD_LEFT) .
              '-' . str_pad ($verse->number, 3, '0', STR_PAD_LEFT) .
              '.json';

              $file = database_path ( 'data/ANALYSIS/VERSES/'.$folder.'/' . $fn);

              if ( !file_exists ($file) ) {
                $this->line ('Missing ' . $fn);
              } else {

                $json_content = json_decode ( file_get_contents ( $file ) );

                $verse->update ([
                  'analysis' => $json_content
                ]);

                $this->line ("\t Imported verse ".$verse->number." from ".$fn);


              }


            } // end foreach verse




          }

        }
    }
}
