<?php

namespace BibleNLP\Console\Commands;

use Illuminate\Console\Command;
use BibleNLP\Repositories\EntityRepository;

class AddSlugToEntities extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'entities:slug';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->info ("Slugging entities...");
        $entities = app (EntityRepository::class)->all();

        $bar = $this->output->createProgressBar(count($entities));
        foreach ($entities AS $entity) {
          $entity->update ([
            'slug' => str_slug ($entity->name)
          ]);
          $bar->advance();
        }
        $bar->finish();
        $this->info ("\n");

    }
}
