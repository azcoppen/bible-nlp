<?php

namespace BibleNLP\Console\Commands;

use Illuminate\Console\Command;
use BibleNLP\Repositories\CrossRefRepository;

class OrderCrossRefs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crossrefs:ordering';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // THIS ONLY GOT TO REV 22:21
        $refs = app (CrossRefRepository::class)->all();
        foreach ( $refs AS $ref ) {
          $ref->update ([
            'start_num'  => head ($ref->verses),
          ]);
          $this->line ("Updated ".$ref->canon_ref);
        }
    }
}
