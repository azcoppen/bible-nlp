<?php

namespace BibleNLP\Console\Commands;

use Illuminate\Console\Command;
use BibleNLP\Repositories\TopicRepository;
use BibleNLP\Repositories\TopicTableRepository;

class BuildTopicTable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'topictable:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // THIS ONLY GOT TO REV 22:21
        $topics = app (TopicRepository::class)->all();
        foreach ( $topics AS $topic ) {

          app (TopicTableRepository::class)->create ([
            'az'              => strtoupper(substr($topic->title, 0, 1)),
            'root_title'      => $topic->title,
            'root_slug'       => $topic->title,
            'subject_title'   => '',
            'subject_slug'    => '',
            'root'            => 1,
          ]);

          if ( count($topic->subjects) ) {
            foreach($topic->subjects AS $sub) {
              app (TopicTableRepository::class)->create ([
                'az'              => strtoupper(substr($topic->title, 0, 1)),
                'root_title'      => $topic->title,
                'root_slug'       => $topic->title,
                'subject_title'   => $sub,
                'subject_slug'    => str_slug($sub),
                'root'            => 0,
              ]);
            }
          }


        }
    }
}
