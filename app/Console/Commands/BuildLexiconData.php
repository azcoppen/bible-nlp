<?php

namespace BibleNLP\Console\Commands;

use Illuminate\Console\Command;
use BibleNLP\Repositories\BookRepository;
use BibleNLP\Repositories\LexiconRepository;

class BuildLexiconData extends Command
{
    private $lexicon;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'build:lexicon';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Aggregates word references';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function add ( $freqs ) {
      if ( $freqs ) {
        foreach ( $freqs AS $word => $c ) {
          if ( array_key_exists ($word, $this->lexicon) ) {
            $this->lexicon[$word] += $c;
          } else {
            $this->lexicon[$word] = $c;
          }
        }
      }
      return false;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $bible = app (BookRepository::class)->with (['chapters', 'chapters.verses'])->all ();

      foreach ( $bible AS $index => $book ) {
        $this->lexicon = [];
        $this->info ("Building lexicon for: ".$book->title);

        $bar = $this->output->createProgressBar($book->chaps);

        foreach ( $book->chapters AS $chapter ) {

          if ( $chapter->frequencies ) {
            $this->add ($chapter->frequencies);
          }

          // BUILD NEW CHAPTER LEXICON

          $bar->advance();
          $this->info ("\n");

        }

        // BUILD NEW BOOK LEXICON
        arsort ($this->lexicon);

        $book->update ([
          //'lexicon' => $
        ]);

        $bar->finish();
      }

      // BUILD GLOBAL LEXICON


    }
}
