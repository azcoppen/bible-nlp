<?php

namespace BibleNLP\Console\Commands;

use Illuminate\Console\Command;

class ParseChapSummariesFromTSK extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tsk:parse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

      $content = file_get_contents (storage_path ('summaries.csv'));

      $formatted = preg_replace('/(?<!\d),(?!\s)|,(?!(\d|\s))/', ', ', $content);
      file_put_contents(storage_path ('corrected.csv'), $formatted);
      dd('done');

      $content = file_get_contents (storage_path ('pasted.txt'));
      $chapters = explode ('CHAPTER', $content);

      $write = '';

      foreach ($chapters AS $chapter) {


        $title = 'CHAPTER' .substr ($chapter, 0, 4);



        $slice = substr ($chapter, 0, 3500);

        $up_to_first_period = strstr($slice, '.', true);

        $content = trim (preg_replace( "/[0-9]+/", " ", $up_to_first_period)).'.';

        if ( empty ($content) ) {
          $this->line ($up_to_first_period);
        }

        $formatted = str_replace(array("\n","\r\n","\r"), '', $content);
        $formatted = str_replace ("  ", '', $formatted);
        $formatted = preg_replace('/(?<!\d);(?!\s)|;(?!(\d|\s))/', '; ', $formatted);

        //$this->line ($title.' '.$up_to_first_period);

        $write .= trim(substr($chapter, 0, 4)) .', "'. $formatted."\"\n";

        //$this->line ("\r\n");
      }

      file_put_contents(storage_path ('extracts-'.uniqId().'.txt'), $write);



    }
}
