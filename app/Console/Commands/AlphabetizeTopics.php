<?php

namespace BibleNLP\Console\Commands;

use Illuminate\Console\Command;
use BibleNLP\Repositories\TopicRepository;

class AlphabetizeTopics extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'topics:az';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $topics = app (TopicRepository::class)->with(['topic_refs'])->all();

      $bar = $this->output->createProgressBar(count($topics));

      foreach ($topics AS $topic) {

        $letter = strtoupper(substr($topic->title, 0, 1));

        $topic->update ([
          'az' => $letter,
          'subjects' => $topic->topic_refs->unique ('subject')->sortBy('subject')->pluck('subject')->all(),
          'refs_total' => $topic->topic_refs->count(),
        ]);


        $bar->advance();
      }

      $bar->finish();
    }
}
