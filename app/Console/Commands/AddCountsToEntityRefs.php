<?php

namespace BibleNLP\Console\Commands;

use Illuminate\Console\Command;
use BibleNLP\Repositories\EntityRefRepository;
use BibleNLP\Models\Chapter;
use BibleNLP\Models\EntityRef;
use BibleNLP\Models\Verse;

use BibleNLP\Criteria\WhereInCriteria;
use BibleNLP\Criteria\WhereFieldEqualsCriteria;

class AddCountsToEntityRefs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'entityrefs:counts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // THIS ONLY GOT TO REV 22:21
        EntityRef::chunk(1000, function ($refs) {

          $bar = $this->output->createProgressBar(count($refs));


          foreach ( $refs AS $ref ) {

            if ( $ref->reference_type == Chapter::class ) {
              $ref->load (['chapter', 'chapter.verses']);

              $verse_ids = $ref->chapter->verses->pluck('id')->all();

              $mentions = EntityRef::where ('reference_type', Verse::class)
                ->whereIn ('reference_id', $verse_ids)
                ->where ('entity_id', $ref->entity_id)
                ->with (['verse'])
                ->get ();

              $mv = $mentions->pluck ('verse')->flatten()->sortBy('number')->pluck('number')->all();

              $ref->update ([
                'mentions'    => $mentions->count(),
                'verse_nums'  => $mv,
              ]);
            }

            if ( $ref->reference_type == Verse::class ) {
              $ref->load (['verse', 'entity']);
              $ref->update ([
                'mentions'   => substr_count(strtolower($ref->verse->text['en']), strtolower($ref->entity->name)),
              ]);
            }

            //$this->line ("Updated ".$ref->id);
            $bar->advance();
          }

          $bar->finish();
          $this->info ("\n");

        });

    }
}
