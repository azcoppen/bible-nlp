<?php

namespace BibleNLP\Console\Commands;

use Illuminate\Console\Command;
use BibleNLP\Repositories\EntityRepository;
use BibleNLP\Repositories\FrequencyRepository;

class AlphabetizeTokens extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tokens:az';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add A-Z first letter to token records';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        $this->info ("Categorising entities...");
        $entities = app (EntityRepository::class)->all();

        $bar = $this->output->createProgressBar(count($entities));
        foreach ($entities AS $entity) {
          $letter = strtoupper(substr($entity->name, 0, 1));
          $entity->update ([
            'az' => $letter
          ]);
          $bar->advance();
        }
        $bar->finish();
        $this->info ("\n");


        $this->info ("Categorising frequencies...");
        $frequencies = app (FrequencyRepository::class)->all();

        $bar = $this->output->createProgressBar(count($frequencies));
        foreach ($frequencies AS $freq) {
          $letter = strtoupper(substr($freq->word, 0, 1));
          $freq->update ([
            'az' => $letter,
          ]);
          $bar->advance();
        }
        $bar->finish();
    }
}
