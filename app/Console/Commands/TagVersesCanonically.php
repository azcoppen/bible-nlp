<?php

namespace BibleNLP\Console\Commands;

use Illuminate\Console\Command;
use BibleNLP\Repositories\BookRepository;

class TagVersesCanonically extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'build:versetags';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $bible = app (BookRepository::class)->with (['chapters', 'chapters.verses'])->all ();

      foreach ( $bible AS $index => $book ) {
        $this->info ("Tagging: ".$book->title);

        foreach ( $book->chapters AS $chapter ) {

          $this->info ("Chapter: ".$chapter->number);

          $chapter->update ([
            'canonical'     => $book->title .' ' . $chapter->number,
            'extended'      => $book->title .', Chapter ' . $chapter->number,
            'crossref_str'  => $book->abbrev.'.'.$chapter->number,
            'crossref_md5'  => md5 ($book->abbrev.'.'.$chapter->number),
          ]);

          $bar = $this->output->createProgressBar($book->chaps);

          foreach ( $chapter->verses AS $verse ) {

            $verse->update ([
              'canonical'     => $book->title .' ' . $chapter->number .':' .$verse->number,
              'extended'      => $book->title .', Chapter ' . $chapter->number .', Verse ' .$verse->number,
              'crossref_str'  => $book->abbrev.'.'.$chapter->number.'.'.$verse->number,
              'crossref_md5'  => md5 ($book->abbrev.'.'.$chapter->number.'.'.$verse->number),
            ]);

            $bar->advance();
            $this->info ("\n");

          } // end foreach verse

          $bar->finish();
          $this->info ("\n");

        }

      }
    }
}
