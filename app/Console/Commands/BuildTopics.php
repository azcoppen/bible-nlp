<?php

namespace BibleNLP\Console\Commands;

use Illuminate\Console\Command;
use BibleNLP\Repositories\TopicRepository;

class BuildTopics extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'build:topics';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Extracts topics from CSV data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $viz_topics = \DB::collection ('viz_topics')->get()->pluck('Topic')->unique();

        $bar = $this->output->createProgressBar(count($viz_topics));

        foreach ($viz_topics AS $vt) {
          app (TopicRepository::class)->create ([
            'title' => $vt,
            'slug'  => str_slug ($vt),
          ]);
          $bar->advance ();
        }
        $bar->finish();

    }
}
