<?php

namespace BibleNLP\Console\Commands;

use Illuminate\Console\Command;
use BibleNLP\Contracts\Services\NLPContract;
use BibleNLP\Repositories\BookRepository;

class BuildNLPData extends Command
{
    private $google;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'build:nlp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends all database text for NLP evaluation';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

      $bible = app (BookRepository::class)->with (['chapters', 'chapters.verses'])->all ();

      foreach ( $bible AS $index => $book ) {
        $this->info ("NLP analyzing: ".$book->title);


        foreach ( $book->chapters AS $chapter ) {

          $this->info ("Chapter: ".$chapter->number);

          $bar = $this->output->createProgressBar($book->chaps);

          foreach ( $chapter->verses AS $verse ) {

            try {

              $filename = 'NIV'.str_pad (($index + 1), 3, '0', STR_PAD_LEFT) .
              '-'. strtoupper (str_slug ($book->title)) .
              '-' .str_pad ($chapter->number, 3, '0', STR_PAD_LEFT) .
              '-' .str_pad ($verse->number, 3, '0', STR_PAD_LEFT) .
              '.json';

              if ( !file_exists (storage_path ($filename)) ) {
                $results = app ( NLPContract::class )
                  ->text ($verse->text['en'])
                  ->google ()
                  ->entities ();

                file_put_contents ( storage_path ($filename), json_encode ($results, JSON_PRETTY_PRINT) );
              }

            } catch ( \Exception $e ) {
              \Log::info ('MISSING: '.$book->title.' '.$chapter->number.':'.$verse->number);
              $this->line ($e->getMessage());
            }

            $bar->advance();
            $this->info ("\n");

          } // end foreach verse

          $bar->finish();
          $this->info ("\n");

        }

      }
    }
}
