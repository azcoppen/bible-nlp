<?php

namespace BibleNLP\Console\Commands;

use Illuminate\Console\Command;
use BibleNLP\Repositories\BookRepository;
use BibleNLP\Repositories\VerseRepository;
use BibleNLP\Repositories\CrossRefRepository;
use BibleNLP\Criteria\WhereFieldEqualsCriteria;
use \DB;

class BuildCrossRefs extends Command
{
    private $bible;
    private $matches;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'build:crossrefs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imports cross references from MySQL';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    public function verse_number_from_str ( $str ) {
      $parts = explode ('.', $str);
      if ( is_array ($parts) && count ($parts) == 3 ) {
        return intval (last ($parts));
      }
      return 0;
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $this->info ("Loading all bible data...");
      $this->bible = app (VerseRepository::class);

      $this->info ("Loading all cross ref temp data...");
      $cross_ref_temp = DB::collection('cross_ref_temp')->get ();


        $bar = $this->output->createProgressBar(count($cross_ref_temp));

        $this->info ("Starting loop...");
        foreach ($cross_ref_temp as $ref) {

          try {
          /****************************
          FIND VERSE
          ***************************/
          $verse = $this->bible->findByField('crossref_str', $ref['verse'])->first();

          /****************************
          FIND CROSS REF VERSE
          ***************************/
          if ( str_contains ($ref['cross_verses'], '-') ) {
            $verse_tokens = explode ('-', $ref['cross_verses']);
            if ( is_array ($verse_tokens) && count ($verse_tokens) ) {

              $first_verse_ref = $this->bible->findByField ('crossref_str', $verse_tokens[0])->first();

              if ( !$first_verse_ref ) {
                continue;
              }

              $from_verse = $this->verse_number_from_str ($verse_tokens[0]);
              $to_verse = $this->verse_number_from_str ($verse_tokens[1]);

              $chapter_id = $first_verse_ref->chapter_id;
              $verse_nums = [$from_verse, $to_verse];
            }
          } else {
            $cross_verse_ref = $this->bible->findByField ('crossref_str', $ref['cross_verses'])->first();
            if ( !$cross_verse_ref ) {
              continue;
            }
            $chapter_id = $cross_verse_ref->chapter_id;
            $verse_nums = [$cross_verse_ref->number];
          }

          $cross_ref = app ( CrossRefRepository::class )->create ([
            'verse_id'    => $verse->id,
            'chapter_id'  => $chapter_id,
            'verses'      => $verse_nums,
            'range'       => str_contains ($ref['cross_verses'], '-') ? 1 : 0,
            'canon_ref'   => $ref['verse'],
            'canon_cross' => $ref['cross_verses'],
          ]);

          $bar->advance();

          } catch (\Exception $e) {
            \Log::error ($e);
            $this->line ($e->getMessage());
          }

        } // end foreach

        $bar->finish();
        $this->info ("\n");

    }
}
