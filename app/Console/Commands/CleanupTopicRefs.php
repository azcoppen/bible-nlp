<?php

namespace BibleNLP\Console\Commands;

use Illuminate\Console\Command;
use BibleNLP\Repositories\TopicRefRepository;

class CleanupTopicRefs extends Command
{
    private $remove = [
      '0.','1.', '2.','3.','4.','5.','6.','7.','8.','9.'
    ];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cleanup:topicrefs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Removes periods and numbers from topic ref subjects';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $refs = app (TopicRefRepository::class)->all();

        $bar = $this->output->createProgressBar(count($refs));

        foreach ($refs AS $ref) {

          if ( str_contains ($ref->subject, '.') ) {
            if ( str_contains (substr($ref->subject, 0, 3), '.') ) {
              //$this->line ("Replacing ".$ref->subject." --- WITH --- ".substr ($ref->subject, 3));
              $text = substr ($ref->subject, 3);
              $text = trim(str_replace ('.', '', $text));

              $ref->update ([
                'subject'       => $text,
                'subject_slug'  => str_slug ($text),
              ]);


            }

          }
          $bar->advance();
        }

        $bar->finish();


    }
}
