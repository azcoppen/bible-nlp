<?php

namespace BibleNLP\Console\Commands;

use Illuminate\Console\Command;
use BibleNLP\Repositories\CrossRefRepository;

class AddShortcutsToCrossRefs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crossrefs:shortcuts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // THIS ONLY GOT TO REV 22:21
        $refs = app (CrossRefRepository::class)->with(['chapter', 'chapter.book'])->all();
        foreach ( $refs AS $ref ) {
          $ref->update ([
            'book_num'  => $ref->chapter->book->order,
            'book_title' => $ref->chapter->book->title,
            'book_slug' => $ref->chapter->book->slug,
            'chap_num' => $ref->chapter->number,
          ]);
          $this->line ("Updated ".$ref->canon_ref);
        }
    }
}
