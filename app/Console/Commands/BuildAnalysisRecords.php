<?php

namespace BibleNLP\Console\Commands;

use Illuminate\Console\Command;
use BibleNLP\Repositories\BookRepository;
use BibleNLP\Repositories\AnalysisRepository;

class BuildAnalysisRecords extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'build:analyses';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates analysis records from chapter and verse properties';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $bible = app (BookRepository::class)->with (['chapters', 'chapters.verses'])->all ();

      foreach ( $bible AS $index => $book ) {
        $this->info ("NLP analyzing: ".$book->title);


        foreach ( $book->chapters AS $chapter ) {

          $this->info ("Chapter: ".$chapter->number);

          if ( isset ($chapter->analysis) ) {
            app (AnalysisRepository::class)->create ([
              'analyzable_type' => get_class ($chapter),
              'analyzable_id'   => $chapter->id,
              'data'            => $chapter->analysis,
            ]);
          }

          $bar = $this->output->createProgressBar($book->chaps);

          foreach ( $chapter->verses AS $verse ) {

            if ( isset ($verse->analysis) ) {
              app (AnalysisRepository::class)->create ([
                'analyzable_type' => get_class ($verse),
                'analyzable_id'   => $verse->id,
                'data'            => $verse->analysis,
              ]);
            }

            $bar->advance();
            $this->info ("\n");

          } // end foreach verse

          $bar->finish();
          $this->info ("\n");

        }

      }
    }
}
