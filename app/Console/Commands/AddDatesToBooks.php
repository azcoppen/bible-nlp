<?php

namespace BibleNLP\Console\Commands;

use Illuminate\Console\Command;
use BibleNLP\Repositories\BookRepository;

class AddDatesToBooks extends Command
{
    private $dates;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'books:additions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->dates = [
          '5ba1c32e9a892004c3222c32' => [1, -1445, -1405, 'Genesis answers two big questions: “How did God’s relationship with the world begin?” and “Where did the nation of Israel come from?”'], // genesis
          '5ba1c32f9a892004c3222c33' => [2, -1445, -1405, 'God saves Israel from slavery in Egypt, and then enters into a special relationship with them.'], // exodus
          '5ba1c32f9a892004c3222c34' => [3, -1445, -1405, 'God gives Israel instructions for how to worship Him.'], // leviticus
          '5ba1c32f9a892004c3222c35' => [4, -1445, -1405, 'Israel fails to trust and obey God, and wanders in the wilderness for 40 years.'], // numbers
          '5ba1c32f9a892004c3222c36' => [5, -1445, -1405, 'Moses gives Israel instructions (in some ways, a recap of the laws in Exodus–Numbers) for how to love and obey God in the Promised Land.'], // deuteronomy
          '5ba1c32f9a892004c3222c37' => [6, -1405, -1385, 'Joshua (Israel’s new leader) leads Israel to conquer the Promised land, then parcels out territories to the twelve tribes of Israel.'], // joshua
          '5ba1c32f9a892004c3222c38' => [7, -1375, -1140, 'Israel enters a cycle of turning from God, falling captive to oppressive nations, calling out to God, and being rescued by leaders God sends their way (called “judges”).'], // judges
          '5ba1c32f9a892004c3222c39' => [8, -1030, -1010, 'Two widows lose everything, and find hope in Israel—which leads to the birth of the future King David.'], // ruth
          '5ba1c32f9a892004c3222c3a' => [9, -931, -722, 'Israel demands a king, who turns out to be quite a disappointment.'], // 1 samuel
          '5ba1c32f9a892004c3222c3b' => [10, -931, -722, "David, a man after God’s own heart, becomes king of Israel."],  // 2 samuel
          '5ba1c32f9a892004c3222c3c' => [11, -561, -538, 'The kingdom of Israel has a time of peace and prosperity under King Solomon, but afterward splits, and the two lines of kings turn away from God.'], // 1 kings
          '5ba1c32f9a892004c3222c3d' => [12, -561, -538, 'Both kingdoms ignore God and his prophets, until they both fall captive to other world empires.'], // 2 kings
          '5ba1c32f9a892004c3222c3e' => [13, -450, -430, 'A brief history of Israel from Adam to David, culminating with David commissioning the temple of God in Jerusalem.'], // 1 chronicles
          '5ba1c32f9a892004c3222c3f' => [14, -450, -430, "David’s son Solomon builds the temple, but after centuries of rejecting God, the Babylonians take the southern Israelites captive and destroy the temple."], // 2 chronicles
          '5ba1c32f9a892004c3222c40' => [15, -457, -444, "The Israelites rebuild the temple in Jerusalem, and a scribe named Ezra teaches the people to once again obey God’s laws."], // ezra
          '5ba1c32f9a892004c3222c41' => [16, -424, -400, 'The city of Jerusalem is in bad shape, so Nehemiah rebuilds the wall around the city.'], // nehemiah
          '5ba1c32f9a892004c3222c42' => [17, -450, -331, "Someone hatches a genocidal plot to bring about Israel’s extinction, and Esther must face the emperor to ask for help."], // esther
          '5ba1c32f9a892004c3222c43' => [18, -6000, -4000, 'Satan attacks a righteous man named Job, and Job and his friends argue about why terrible things are happening to him.'], // job
          '5ba1c32f9a892004c3222c44' => [19, -1410, -450, 'A collection of 150 songs that Israel sang to God (and to each other)—kind of like a hymnal for the ancient Israelites.'], // psalms
          '5ba1c32f9a892004c3222c45' => [20, -971, -686, 'A collection of sayings written to help people make wise decisions that bring about justice.'], // proverbs
          '5ba1c32f9a892004c3222c46' => [21, -940, -931, 'A philosophical exploration of the meaning of life—with a surprisingly nihilistic tone for the Bible.'], // elcclesiastes
          '5ba1c32f9a892004c3222c47' => [22, -971, -965, 'A love song (or collection of love songs) celebrating love, desire, and marriage.'], // song of solomon
          '5ba1c32f9a892004c3222c48' => [23, -700, -681, 'God sends the prophet Isaiah to warn Israel of future judgment—but also to tell them about a coming king and servant who will “bear the sins of many.”'], // isaiah
          '5ba1c32f9a892004c3222c49' => [24, -586, -570, 'God sends a prophet to warn Israel about the coming Babylonian captivity, but the people don’t take the news very well.'], // jeremiah
          '5ba1c32f9a892004c3222c4a' => [25, -586, -586, 'A collection of dirges lamenting the fall of Jerusalem after the Babylonian attacks.'],// lamentations
          '5ba1c32f9a892004c3222c4b' => [26, -590, -570, 'God chooses a man to speak for Him to Israel, to tell them the error of their ways and teach them justice.'], // ezekiel
          '5ba1c32f9a892004c3222c4c' => [27, -536, -530, "Daniel becomes a high-ranking wise man in the Babylonian and Persian empires, and has prophetic visions concerning Israel’s future."], // daniel
          '5ba1c32f9a892004c3222c4d' => [28, -750, -710, 'Hosea is told to marry a prostitute who leaves him, and he must bring her back: a picture of God’s relationship with Israel.'], // hosea
          '5ba1c32f9a892004c3222c4e' => [29, -835, -796, 'God sends a plague of locusts to Judge Israel, but his judgment on the surrounding nations is coming, too.'], // joel
          '5ba1c32f9a892004c3222c4f' => [30, -750, -750, 'A shepherd named Amos preaches against the injustice of the Northern Kingdom of Israel.'], // amos
          '5ba1c32f9a892004c3222c50' => [31, -850, -840, 'Obadiah warns the neighboring nation of Edom that they will be judged for plundering Jerusalem.'], // obadiah
          '5ba1c32f9a892004c3222c51' => [32, -775, -775, 'A disobedient prophet runs from God, is swallowed by a great fish, and then preaches God’s message to the city of Nineveh.'], // jonah
          '5ba1c32f9a892004c3222c52' => [33, -735, -710, 'Micah confronts the leaders of Israel and Judah regarding their injustice, and prophecies that one day the Lord himself will rule in perfect justice.'], // micah
          '5ba1c32f9a892004c3222c53' => [34, -650, -650, "Nahum foretells of God’s judgment on Nineveh, the capital of Assyria."], // nahum
          '5ba1c32f9a892004c3222c54' => [35, -615, -605, 'Habakkuk pleads with God to stop the injustice and violence in Judah, but is surprised to find that God will use the even more violent Babylonians to do so.'], // habukkuk
          '5ba1c32f9a892004c3222c55' => [36, -635, -625, 'God warns that he will judge Israel and the surrounding nations, but also that he will restore them in peace and justice.'], // zephaniah
          '5ba1c32f9a892004c3222c56' => [37, -520, -520, "The people have abandoned the work of restoring God’s temple in Jerusalem, and so Haggai takes them to task."], // haggai
          '5ba1c32f9a892004c3222c57' => [38, -480, -470, "The prophet Zechariah calls Israel to return to God, and records prophetic visions that show what’s happening behind the scenes."], // zechariah
          '5ba1c32f9a892004c3222c58' => [39, -433, -424, 'God has been faithful to Israel, but they continue to live disconnected from him—so God sends Malachi to call them out.'], // malachi

          '5ba1c32f9a892004c3222c59' => [40, 50, 60, "An account of Jesus’ life, death, and resurrection, focusing on Jesus’ role as the true king of the Jews."],// matthew
          '5ba1c32f9a892004c3222c5a' => [41, 50, 60, "A brief account of Jesus’ earthly ministry highlights Jesus’ authority and servanthood."], // mark
          '5ba1c32f9a892004c3222c5b' => [42, 60, 61, "The most thorough account of Jesus’ life, pulling together eyewitness testimonies to tell the full story of Jesus."], // luke
          '5ba1c32f9a892004c3222c5c' => [43, 80, 90, "Stories of signs and miracles with the hope that readers will believe in Jesus."], // john
          '5ba1c32f9a892004c3222c5d' => [44, 62, 62, 'Jesus returns to the Father, the Holy Spirit comes to the church, and the gospel of Jesus spreads throughout the world.'], // acts
          '5ba1c32f9a892004c3222c5e' => [45, 56, 56, 'Paul summarizes how the gospel of Jesus works in a letter to the churches at Rome, where he plans to visit.'], // romans
          '5ba1c32f9a892004c3222c5f' => [46, 55, 55, 'Paul writes a disciplinary letter to a fractured church in Corinth, and answers some questions that they’ve had about how Christians should behave.'], // 1 cor
          '5ba1c32f9a892004c3222c60' => [47, 55, 56, 'Paul writes a letter of reconciliation to the church at Corinth, and clears up some concerns that they have.'], // 2 cor
          '5ba1c32f9a892004c3222c61' => [48, 49, 50, 'Paul hears that the Galatian churches have been lead to think that salvation comes from the law of Moses, and writes a (rather heated) letter telling them where the false teachers have it wrong.'], // galatians
          '5ba1c32f9a892004c3222c62' => [49, 60, 62, 'Paul writes to the church at Ephesus about how to walk in grace, peace, and love.'], // ephesians
          '5ba1c32f9a892004c3222c63' => [50, 60, 62, 'An encouraging letter to the church of Philippi from Paul, telling them how to have joy in Christ.'], // phillipians
          '5ba1c32f9a892004c3222c64' => [51, 60, 62, 'Paul writes the church at Colossae a letter about who they are in Christ, and how to walk in Christ.'], // colossians
          '5ba1c32f9a892004c3222c65' => [52, 51, 51, 'Paul has heard a good report on the church at Thessalonica, and encourages them to “excel still more” in faith, hope, and love.'], // 1 thess
          '5ba1c32f9a892004c3222c66' => [53, 51, 52, 'Paul instructs the Thessalonians on how to stand firm until the coming of Jesus.'], // 2 thess
          '5ba1c32f9a892004c3222c67' => [54, 62, 64, 'Paul gives his protegé Timothy instruction on how to lead a church with sound teaching and a godly example.'], // 1 tim
          '5ba1c32f9a892004c3222c68' => [55, 66, 67, 'Paul is nearing the end of his life, and encourages Timothy to continue preaching the word.'], // 2 tim
          '5ba1c32f9a892004c3222c69' => [56, 62, 64, 'Paul advises Titus on how to lead orderly, counter-cultural churches on the island of Crete.'], // titus
          '5ba1c32f9a892004c3222c6a' => [57, 60, 62, 'Paul strongly recommends that Philemon accept his runaway slave as a brother, not a slave.'], // philemon
          '5ba1c32f9a892004c3222c6b' => [58, 67, 69, 'A letter encouraging Christians to cling to Christ despite persecution, because he is greater.'], // hebrews
          '5ba1c32f9a892004c3222c6c' => [59, 44, 49, 'A letter telling Christians to live in ways that demonstrate their faith in action.'], // james
          '5ba1c32f9a892004c3222c6d' => [60, 64, 65, 'Peter writes to Christians who are being persecuted, encouraging them to testify to the truth and live accordingly.'], // 1 peter
          '5ba1c32f9a892004c3222c6e' => [61, 67, 68, 'Peter writes a letter reminding Christians about the truth of Jesus, and warning them that false teachers will come.'], // 2 peter
          '5ba1c32f9a892004c3222c6f' => [62, 90, 95, "John writes a letter to Christians about keeping Jesus’ commands, loving one another, and important things they should know."], // 1 john
          '5ba1c32f9a892004c3222c70' => [63, 90, 95, 'A very brief letter about walking in truth, love, and obedience.'], // 2 john
          '5ba1c32f9a892004c3222c71' => [64, 90, 95, 'An even shorter letter about Christian fellowship.'], // 3 john
          '5ba1c32f9a892004c3222c72' => [65, 68, 70, 'A letter encouraging Christians to content for the faith, even though ungodly persons have crept in unnoticed.'], // jude
          '5ba1c32f9a892004c3222c73' => [66, 94, 96, 'John sees visions of things that have been, things that are, and things that are yet to come.'], // revelation
        ];
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $books = app (BookRepository::class)->all();
        foreach ($books AS $index => $book) {
          //$this->line ($book->title . '['.$index.']: '.$this->dates[$index][0].','.$this->dates[$index][1] . ' --> '.$this->dates[$index][2]);

          $book->update ([
            'order'         => $this->dates[$book->id][0],
            'written_from'  => $this->dates[$book->id][1],
            'written_to'    => $this->dates[$book->id][2],
            'summary'       => $this->dates[$book->id][3],
          ]);

          $this->line ('Updated '.$book->title);
        }
    }
}
