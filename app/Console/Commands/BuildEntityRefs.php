<?php

namespace BibleNLP\Console\Commands;

use Illuminate\Console\Command;
use BibleNLP\Repositories\BookRepository;
use BibleNLP\Repositories\EntityRepository;
use BibleNLP\Repositories\EntityRefRepository;
use BibleNLP\Criteria\WhereFieldEqualsCriteria;

class BuildEntityRefs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'build:entityrefs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates references to entities for chapters and verses';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function lookup ( $name, $type ) {
      return app (EntityRepository::class)
        ->pushCriteria ( new WhereFieldEqualsCriteria ('name', $name) )
        ->pushCriteria ( new WhereFieldEqualsCriteria ('type', $type) )
        ->first ();
    }

    public function add_ref ( $entity, $ref ) {
      return app (EntityRefRepository::class)->create ([
        'entity_id'       => $entity->id,
        'reference_type'  => get_class ($ref),
        'reference_id'    => $ref->id,
      ]);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $bible = app (BookRepository::class)->with (['chapters', 'chapters.verses'])->all ();

      foreach ( $bible AS $index => $book ) {
        $this->info ("Referencing entities for: ".$book->title);

        $bar = $this->output->createProgressBar(count($book->chapters));

        foreach ( $book->chapters AS $chapter ) {

          if ( isset ($chapter->analysis) ) {
            foreach ( $chapter->analysis AS $chap_entity ) {
              $existing_chap_entity = $this->lookup ($chap_entity['name'], $chap_entity['type']);
              if ( $existing_chap_entity && is_object ($existing_chap_entity) ) {
                $this->add_ref ($existing_chap_entity, $chapter);
              }
            }

          }

          foreach ( $chapter->verses AS $verse ) {

            if ( isset ($verse->analysis) ) {
              foreach ( $verse->analysis AS $verse_entity ) {
                $existing_verse_entity = $this->lookup ($verse_entity['name'], $verse_entity['type']);
                if ( $existing_verse_entity && is_object ($existing_verse_entity) ) {
                  $this->add_ref ($existing_verse_entity, $verse);
                }
              }
            }



          } // end foreach verse

          $bar->advance();
          $this->info ("\n");


        }

        $bar->finish();
        $this->info ("\n");


      }
    }
}
