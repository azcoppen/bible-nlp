<?php

namespace BibleNLP\Console\Commands;

use Illuminate\Console\Command;
use BibleNLP\Repositories\FrequencyRepository;
use BibleNLP\Models\Chapter;
use BibleNLP\Criteria\WhereFieldEqualsCriteria;

class AddShortcutsToFrequencies extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'frequencies:chapter-shortcuts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // THIS ONLY GOT TO REV 22:21
        $freqs = app (FrequencyRepository::class)
        ->pushCriteria ( new WhereFieldEqualsCriteria ('container_type', Chapter::class) )
        ->with(['chapter', 'chapter.book'])->all();

        foreach ( $freqs AS $freq ) {

          $freq->update ([
            'book_num'  => isset($freq->chapter->book) && is_object ($freq->chapter->book) ? $freq->chapter->book->order : 0,
            'book_title'=> isset($freq->chapter->book) && is_object ($freq->chapter->book) ? $freq->chapter->book->title : '',
            'book_slug' => isset($freq->chapter->book) && is_object ($freq->chapter->book) ? $freq->chapter->book->slug : '',
            'chap_num'  => isset($freq->chapter) && is_object ($freq->chapter) ? $freq->chapter->number : 0,
          ]);
          $this->line ("Updated ".$freq->id);
        }
    }
}
