<?php

namespace BibleNLP\Console\Commands;

use Illuminate\Console\Command;
use BibleNLP\Repositories\MLTRepository;
use BibleNLP\Repositories\VerseRepository;
use BibleNLP\Models\Verse;

class BuildVerseMLTData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'verses:mlt';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function similarity ( $verse_id, $str ) {
      $rows = Verse::searchRaw([
              'query' => [
                  'more_like_this' => [
                    "fields" => ["content", "words", "entities"],
                    "like" => $str,
                    "min_term_freq" => 1,
                    "max_query_terms" => 12
                  ]
              ]
          ]);

      return collect(data_get($rows, 'hits.hits'))->pluck('_score', '_id')->reject(function ($value, $key) use ($verse_id) {
          return $key == $verse_id;
      });
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $verses = app (VerseRepository::class)->all();
        foreach ( $verses AS $verse ) {
          $similar = $this->similarity ($verse->id, $verse->text['en']);
          app (MLTRepository::class)->create ([
            'container_type'  => Verse::class,
            'container_id'    => $verse->id,
            'similarity'      => $similar->all(),
            'total'           => $similar->count(),
          ]);
          $this->line ("Added  ".$similar->count().' similar verses for '.$verse->canonical);
        }
    }
}
