<?php

namespace BibleNLP\Listeners;

use BibleNLP\Events\SearchEntered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use BibleNLP\Repositories\SearchRepository;

class RecordSearch
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SearchEntered  $event
     * @return void
     */
    public function handle(SearchEntered $event)
    {
      app (SearchRepository::class)->create ([
        'container_type'  => $event->container_type,
        'container_id'    => $event->container_id,
        'q'               => $event->q,
      ]);
    }
}
