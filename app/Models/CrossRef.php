<?php

namespace BibleNLP\Models;

use Jenssegers\Mongodb\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Reference.
 *
 * @package namespace BibleNLP\Models;
 */
class CrossRef extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'verse_id',
      'chapter_id',
      'verses',
      'range',
      'canon_ref',
      'canon_cross',
      'book_num',
      'book_title',
      'book_slug',
      'chap_num',
      'start_num',
    ];

    protected $appends = ['ref_full_text'];

    public function getCitedTextAttribute () {
      return $this->verse->text['en'];
    }

    public function getHumanRangeAttribute () {
      if ( $this->range > 0 && count ($this->verses) > 1 ) {
        return head($this->verses) .'-' .last($this->verses);
      }
      return head($this->verses);
    }

    public function getRefFullTextAttribute () {
      if ( !$this->verses ) {
        return 'no verse array';
      }

      if ( $this->range > 0 && count ($this->verses) > 1 ) {
        return $this->chapter->verses->whereIn('number', range(head($this->verses), last($this->verses)))->pluck('text.en')->implode (' ');
      }
      return $this->chapter->verses->where('number', head($this->verses))->first()->text['en'];
    }

    public function chapter () {
      return $this->belongsTo ( Chapter::class );
    }

    public function verse () {
      return $this->belongsTo ( Verse::class );
    }

}
