<?php

namespace BibleNLP\Models;

use Jenssegers\Mongodb\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use ScoutElastic\Searchable;
use BibleNLP\Search\Configurators\TopicRefIndexConfigurator;

/**
 * Class TopicRef.
 *
 * @package namespace BibleNLP\Models;
 */
class TopicRef extends Model implements Transformable
{
    use TransformableTrait, Searchable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'topic_id',
      'verse_id',
      'title',
      'title_slug',
      'subject',
      'subject_slug',
      'az',
      'book_num',
      'book_title',
      'book_slug',
      'chap_num',
      'canon_ref',
    ];

    protected $indexConfigurator = TopicRefIndexConfigurator::class;

    protected $searchRules = [
        //
    ];

    // Here you can specify a mapping for a model fields.
    protected $mapping = [
        'properties' => [
            'text' => [
                'type' => 'text',
                'fields' => [
                    'raw' => [
                        'type' => 'keyword',
                    ]
                ]
            ],
        ]
    ];

    public function getRefFullTextAttribute () {
      return $this->verse->text['en'];
    }

    public function verse () {
      return $this->belongsTo (Verse::class);
    }

}
