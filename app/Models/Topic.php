<?php

namespace BibleNLP\Models;

use Jenssegers\Mongodb\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use ScoutElastic\Searchable;
use BibleNLP\Search\Configurators\TopicIndexConfigurator;

/**
 * Class Topic.
 *
 * @package namespace BibleNLP\Models;
 */
class Topic extends Model implements Transformable
{
    use TransformableTrait, Searchable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'title', 'slug', 'az', 'subjects', 'refs_total'
    ];

    protected $indexConfigurator = TopicIndexConfigurator::class;

    protected $searchRules = [
        //
    ];

    // Here you can specify a mapping for a model fields.
    protected $mapping = [
        'properties' => [
            'az' => [
                'type' => 'keyword',
            ],
            'title' => [
                'type' => 'text',
            ],
            'subjects' => [
                'type' => 'text',
            ],
        ]
    ];

    public function toSearchableArray () : array {
      return [
        'az'       => $this->az,
        'title'    => $this->title,
        'subjects' => $this->title. ' ' .collect ($this->subjects)->implode (', '.$this->title.' ')
      ];
    }

    public function topic_refs () {
      return $this->hasMany (TopicRef::class);
    }

}
