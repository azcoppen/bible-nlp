<?php

namespace BibleNLP\Models;

use Jenssegers\Mongodb\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use ScoutElastic\Searchable;
use BibleNLP\Search\Configurators\FrequencyIndexConfigurator;

/**
 * Class Frequency.
 *
 * @package namespace BibleNLP\Models;
 */
class Frequency extends Model implements Transformable
{
    use TransformableTrait, Searchable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'container_type',
      'container_id',
      'word',
      'total',
      'az',
      'book_num',
      'book_title',
      'book_slug',
      'chap_num',
    ];

    protected $indexConfigurator = FrequencyIndexConfigurator::class;

    protected $searchRules = [
        //
    ];

    // Here you can specify a mapping for a model fields.
    protected $mapping = [
        'properties' => [
          'az' => [
              'type' => 'keyword',
              "fields" => [
                "analyzed" => [
                  "type" => "text",
                  "analyzer" => "partial_match_analyzer",
                ],
              ],
          ],
          'word' => [
              'type' => 'text',
              "fields" => [
                "analyzed" => [
                  "type" => "text",
                  "analyzer" => "partial_match_analyzer",
                ],
              ],
          ],
          'slug' => [
              'type' => 'text',
              "fields" => [
                "analyzed" => [
                  "type" => "text",
                  "analyzer" => "partial_match_analyzer",
                ],
              ],
          ],
        ]
    ];

    public function toSearchableArray () : array {
      return [
        'az'        => $this->az,
        'word'      => $this->word,
        'slug'      => str_slug($this->word),
      ];
    }

    public function shouldBeSearchable () {
      return $this->container_type == 'GLOBAL' ? true : false;
    }

    public function chapter () {
      return $this->belongsTo (Chapter::class, 'container_id');
    }

    public function instances () {
        return $this->hasMany (Instance::class)->where ('reference_type', Verse::class);
    }

}
