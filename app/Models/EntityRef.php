<?php

namespace BibleNLP\Models;

use Jenssegers\Mongodb\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class EntityRef.
 *
 * @package namespace BibleNLP\Models;
 */
class EntityRef extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'entity_id',
      'reference_type',
      'reference_id',
      'az',
      'book_num',
      'book_title',
      'book_slug',
      'chap_num',
      'canon_ref',
      'mentions',
      'verse_nums'
    ];

    public function entity () {
      return $this->belongsTo ( Entity::class );
    }

    public function chapter () {
      return $this->belongsTo ( Chapter::class, 'reference_id' );
    }

    public function verse () {
      return $this->belongsTo ( Verse::class,  'reference_id' );
    }

    public function getVerseNumbersListAttribute () {
      if ( $this->verse_nums ) {
        return collect ($this->verse_nums)->sort()->values()->implode (', ');
      }
      return null;
    }

}
