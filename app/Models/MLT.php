<?php

namespace BibleNLP\Models;

use Jenssegers\Mongodb\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Lexicon.
 *
 * @package namespace BibleNLP\Models;
 */
class MLT extends Model implements Transformable
{
    use TransformableTrait;

    protected $collection = 'mlt';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'container_type',
      'container_id',
      'similarity',
      'total'
    ];

    public function verse () {
        return $this->belongsTo ( Verse::class, 'container_id');
    }

}
