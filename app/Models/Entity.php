<?php

namespace BibleNLP\Models;

use Jenssegers\Mongodb\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use ScoutElastic\Searchable;
use BibleNLP\Search\Configurators\EntityIndexConfigurator;

/**
 * Class Entity.
 *
 * @package namespace BibleNLP\Models;
 */
class Entity extends Model implements Transformable
{
    use TransformableTrait, Searchable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'name',
      'slug',
      'type',
      'az',
      'refs_total',
    ];

    protected $indexConfigurator = EntityIndexConfigurator::class;

    protected $searchRules = [
        //
    ];

    // Here you can specify a mapping for a model fields.
    protected $mapping = [
        'properties' => [
          'az' => [
              'type' => 'keyword',
              "fields" => [
                "analyzed" => [
                  "type" => "text",
                  "analyzer" => "partial_match_analyzer",
                ],
              ],
          ],
          'type' => [
              'type' => 'keyword',
              "fields" => [
                "analyzed" => [
                  "type" => "text",
                  "analyzer" => "partial_match_analyzer",
                ],
              ],
          ],
          'name' => [
              'type' => 'text',
              "fields" => [
                "analyzed" => [
                  "type" => "text",
                  "analyzer" => "partial_match_analyzer",
                ],
              ],
          ],
          'slug' => [
              'type' => 'text',
              "fields" => [
                "analyzed" => [
                  "type" => "text",
                  "analyzer" => "partial_match_analyzer",
                ],
              ],
          ],
        ]
    ];

    public function toSearchableArray () : array {
      return [
        'az'    => $this->az,
        'type'  => $this->type,
        'name'  => $this->name,
        'slug'  => str_slug($this->name),
      ];
    }

    public function entity_refs () {
      return $this->hasMany ( EntityRef::class );
    }

}
