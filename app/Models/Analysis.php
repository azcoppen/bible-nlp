<?php

namespace BibleNLP\Models;

use Jenssegers\Mongodb\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Analysis.
 *
 * @package namespace BibleNLP\Models;
 */
class Analysis extends Model implements Transformable
{
    use TransformableTrait;

    protected $collection = 'analyses';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'analyzable_type',
      'analyzable_id',
      'data'
    ];

    public function analyzable () {
        return $this->morphTo();
    }

}
