<?php

namespace BibleNLP\Models;

use Jenssegers\Mongodb\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use ScoutElastic\Searchable;

/**
 * Class Analysis.
 *
 * @package namespace BibleNLP\Models;
 */
class Search extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'container_type',
      'container_id',
      'q'
    ];


}
