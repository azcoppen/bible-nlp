<?php

namespace BibleNLP\Models;

use Jenssegers\Mongodb\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Lexicon.
 *
 * @package namespace BibleNLP\Models;
 */
class Instance extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'frequency_id',
      'reference_type',
      'reference_id'
    ];

    public function frequency () {
        return $this->belongsTo ( Frequency::class );
    }

}
