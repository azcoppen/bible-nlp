<?php

namespace BibleNLP\Models;

use Jenssegers\Mongodb\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use ScoutElastic\Searchable;
use BibleNLP\Search\Configurators\BookIndexConfigurator;

/**
 * Class Book.
 *
 * @package namespace BibleNLP\Models;
 */
class Book extends Model implements Transformable
{
    use TransformableTrait, Searchable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'code',
      'volume',
      'abbrev',
      'title',
      'slug',
      'chaps',
      'order',
      'written_from',
      'written_to',
      'summary',
      'num_words',
      'num_verses',
      'num_entities',
      'top_entities',
      'top_freqs',
      'percent',
    ];

    protected $indexConfigurator = BookIndexConfigurator::class;

    protected $searchRules = [
        //
    ];

    // Here you can specify a mapping for a model fields.
    protected $mapping = [
        'properties' => [
          'code'      => [
            'type' => 'keyword',
            "fields" => [
              "analyzed" => [
                "type" => "text",
                "analyzer" => "partial_match_analyzer",
              ],
            ],
          ],
          'vol_code'  => [
            'type' => 'keyword',
            "fields" => [
              "analyzed" => [
                "type" => "text",
                "analyzer" => "partial_match_analyzer",
              ],
            ],
          ],
          'vol_full'  => [
            'type' => 'keyword',
            "fields" => [
              "analyzed" => [
                "type" => "text",
                "analyzer" => "partial_match_analyzer",
              ],
            ],
          ],
          'abbrev'    => [
            'type' => 'keyword',
            "fields" => [
              "analyzed" => [
                "type" => "text",
                "analyzer" => "partial_match_analyzer",
              ],
            ],
          ],
          'title'     => [
            'type' => 'keyword',
            "fields" => [
              "analyzed" => [
                "type" => "text",
                "analyzer" => "partial_match_analyzer",
              ],
            ],
          ],
          'summary'   => [
            'type' => 'keyword',
            "fields" => [
              "analyzed" => [
                "type" => "text",
                "analyzer" => "partial_match_analyzer",
              ],
            ],
          ],
          'dating'    => [
            'type' => 'keyword',
            "fields" => [
              "analyzed" => [
                "type" => "text",
                "analyzer" => "partial_match_analyzer",
              ],
            ],
          ],
          'order'     => [
            'type' => 'keyword',
            "fields" => [
              "analyzed" => [
                "type" => "text",
                "analyzer" => "partial_match_analyzer",
              ],
            ],
          ],
        ]
    ];

    public function toSearchableArray () : array {
      return [
        'code'      => $this->code,
        'vol_code'  => $this->volume,
        'vol_full'  => $this->volume == 'OT' ? 'Old Testament' : 'New Testament',
        'abbrev'    => $this->abbrev,
        'title'     => $this->title,
        'summary'   => $this->summary,
        'dating'    => $this->human_dating,
        'order'     => intval($this->order),
      ];
    }

    public function chapters () {
      return $this->hasMany ( Chapter::class );
    }

    public function summaries () {
      return $this->hasMany ( Summary::class );
    }

    public function verses () {
      return $this->hasMany ( Verse::class );
    }

    public function getFullTextAttribute () {
      return $this->verses->pluck ('text.en')->implode (' ');
    }

    public function getHumanDatingAttribute () {
      if ( $this->written_from == $this->written_to ) {
        if ( $this->written_from < 0 ) {
          return abs ($this->written_from) . ' BC';
        }
        return $this->written_from . ' AD';
      } else {
        $str = '';
        if ( $this->written_from < 0 ) {
          $str .= abs ($this->written_from) . ' BC';
        } else {
          $str .= $this->written_from . ' AD';
        }

        $str .= ' - ';

        if ( $this->written_to < 0 ) {
          $str .= abs ($this->written_to) . ' BC';
        } else {
          $str .= $this->written_to . ' AD';
        }

        return $str;

      }
    }

}
