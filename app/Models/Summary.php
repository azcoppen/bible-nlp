<?php

namespace BibleNLP\Models;

use Jenssegers\Mongodb\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use ScoutElastic\Searchable;
use BibleNLP\Search\Configurators\SummaryIndexConfigurator;

/**
 * Class Analysis.
 *
 * @package namespace BibleNLP\Models;
 */
class Summary extends Model implements Transformable
{
    use TransformableTrait, Searchable;

    protected $collection = 'summaries';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'book_id',
      'container_type',
      'container_id',
      'book_num',
      'chap_num',
      'text'
    ];

    protected $indexConfigurator = SummaryIndexConfigurator::class;

    protected $searchRules = [
        //
    ];

    // Here you can specify a mapping for a model fields.
    protected $mapping = [
        'properties' => [
          'book_num'     => [
              'type' => 'integer',
          ],
          'chap_num'     => [
              'type' => 'integer',
          ],
          'text' => [
              'type' => 'text',
              "fields" => [
                "analyzed" => [
                  "type" => "text",
                  "analyzer" => "partial_match_analyzer",
                ],
              ],
          ],
        ]
    ];

    public function toSearchableArray () : array {
      $this->load (['chapter']);
      return [
        'canonical'     => $this->chapter->canonical,
        'extended'      => $this->chapter->extended,
        'crossref_str'  => $this->chapter->crossref_str,
        'book_name'     => $this->book_title,
        'book_num'      => $this->book_num,
        'chap_num'      => $this->chap_num,
        'text'          => fix_utf8_apos($this->text['en']),
      ];
    }

    public function book () {
        return $this->belongsTo (Book::Class);
    }

    public function chapter () {
      return $this->belongsTo (Chapter::class, 'container_id');
    }

}
