<?php

namespace BibleNLP\Models;

use Jenssegers\Mongodb\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use ScoutElastic\Searchable;
use BibleNLP\Search\Configurators\VerseIndexConfigurator;

use BibleNLP\Traits\GroupedCrossRefs;
use BibleNLP\Traits\GroupedEntities;

/**
 * Class Verse.
 *
 * @package namespace BibleNLP\Models;
 */
class Verse extends Model implements Transformable
{
    use TransformableTrait, GroupedEntities, GroupedCrossRefs, Searchable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'number',
      'analysis',
      'canonical',
      'extended',
      'crossref_str',
      'crossref_md5',
      'book_num',
      'book_title',
      'book_slug',
      'chap_num',
    ];

    protected $indexConfigurator = VerseIndexConfigurator::class;

    protected $searchRules = [
        //
    ];

    // Here you can specify a mapping for a model fields.
    protected $mapping = [
        'properties' => [
            'number' => [
                'type' => 'integer',
            ],
            'canonical' => [
                'type' => 'keyword',
                "fields" => [
                  "analyzed" => [
                    "type" => "text",
                    "analyzer" => "partial_match_analyzer",
                  ],
                ],
            ],
            'extended'      => [
                'type' => 'text',
                "fields" => [
                  "analyzed" => [
                    "type" => "text",
                    "analyzer" => "partial_match_analyzer",
                  ],
                ],
            ],
            'crossref_str'  => [
                'type' => 'text',
                "fields" => [
                  "analyzed" => [
                    "type" => "text",
                    "analyzer" => "partial_match_analyzer",
                  ],
                ],
            ],
            'book_name'     => [
                'type' => 'keyword',
            ],
            'book_num'       => [
                'type' => 'integer',
            ],
            'chapter'       => [
                'type' => 'integer',
            ],
            /*
            'chap_summary'       => [
                'type' => 'text',
                "fields" => [
                  "analyzed" => [
                    "type" => "text",
                    "analyzer" => "partial_match_analyzer",
                  ],
                ],
            ],
            */
            'content'       => [
                'type' => 'text',
                "fields" => [
                  "analyzed" => [
                    "type" => "text",
                    "analyzer" => "partial_match_analyzer",
                  ],
                ],
            ],
            'words'         => [
                'type' => 'text',
                "fields" => [
                  "analyzed" => [
                    "type" => "text",
                    "analyzer" => "partial_match_analyzer",
                  ],
                ],
            ],
            'entities'      => [
                'type' => 'text',
                "fields" => [
                  "analyzed" => [
                    "type" => "text",
                    "analyzer" => "partial_match_analyzer",
                  ],
                ],
            ],
        ]
    ];

    public function toSearchableArray () : array {
      $this->load (['frequencies']);
      return [
        'number'        => intval ($this->number),
        'canonical'     => $this->canonical,
        'extended'      => $this->extended,
        'crossref_str'  => $this->crossref_str,
        'book_name'     => $this->book_title,
        'book_num'      => $this->book_num,
        'chapter'       => $this->chap_num,
        //'chap_summary'  => $this->chapter->summaries->first()->text['en'],
        'content'       => $this->text['en'],
        'words'         => $this->frequencies->pluck('word')->sort()->implode (', '),
        'entities'      => collect($this->analysis)->pluck ('name')->sort()->implode (', '),
      ];
    }

    public function analyses () {
      return $this->morphMany ( Analysis::class, 'analzyable' );
    }

    public function book () {
      return $this->hasOne (Book::class);
    }

    public function chapter () {
      return $this->belongsTo ( Chapter::class );
    }

    public function cross_refs () {
      return $this->hasMany ( CrossRef::class );
    }

    public function entity_refs () {
      return $this->hasMany ( EntityRef::class, 'reference_id' )->where ('reference_type', get_class ($this));
    }

    public function frequencies () {
      return $this->hasMany ( Frequency::class, 'container_id')->where ('container_type', get_class ($this));
    }

    public function instances () {
      return $this->hasMany ( Instance::class, 'reference_id')->where ('reference_type', get_class ($this));
    }

    public function mlt () {
      return $this->hasOne ( MLT::class, 'container_id' );
    }

    public function topic_refs () {
      return $this->hasMany ( TopicRef::class );
    }


}
