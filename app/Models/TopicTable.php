<?php

namespace BibleNLP\Models;

use Jenssegers\Mongodb\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use ScoutElastic\Searchable;
use BibleNLP\Search\Configurators\TopicTableIndexConfigurator;

/**
 * Class Topic.
 *
 * @package namespace BibleNLP\Models;
 */
class TopicTable extends Model implements Transformable
{
    use TransformableTrait, Searchable;

    protected $collection = "topic_table";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'az',
      'root_title',
      'root_slug',
      'subject_title',
      'subject_slug',
      'root',
    ];

    protected $indexConfigurator = TopicTableIndexConfigurator::class;

    protected $searchRules = [
        //
    ];

    // Here you can specify a mapping for a model fields.
    protected $mapping = [
        'properties' => [
            'az' => [
                'type' => 'keyword',
            ],
            'root_title' => [
                'type' => 'text',
                "fields" => [
                  "analyzed" => [
                    "type" => "text",
                    "analyzer" => "partial_match_analyzer",
                  ],
                ],
            ],
            'root_slug' => [
                'type' => 'text',
                "fields" => [
                  "analyzed" => [
                    "type" => "text",
                    "analyzer" => "partial_match_analyzer",
                  ],
                ],
            ],
            'subject_title' => [
                'type' => 'text',
                "fields" => [
                  "analyzed" => [
                    "type" => "text",
                    "analyzer" => "partial_match_analyzer",
                  ],
                ],
            ],
            'subject_slug' => [
                'type' => 'text',
                "fields" => [
                  "analyzed" => [
                    "type" => "text",
                    "analyzer" => "partial_match_analyzer",
                  ],
                ],
            ],
            'root'     => [
                'type' => 'integer',
            ],
        ]
    ];

    public function toSearchableArray () : array {
      return [
        'az'            => $this->az,
        'root_title'    => $this->root_title,
        'root_slug'     => $this->root_slug,
        'subject_title' => $this->subject_title,
        'subject_slug'  => $this->subject_slug,
        'root'          => $this->root,
      ];
    }

}
