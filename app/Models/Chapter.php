<?php

namespace BibleNLP\Models;

use Jenssegers\Mongodb\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use ScoutElastic\Searchable;
use BibleNLP\Search\Configurators\ChapterIndexConfigurator;

use BibleNLP\Traits\GroupedEntities;

/**
 * Class Chapter.
 *
 * @package namespace BibleNLP\Models;
 */
class Chapter extends Model implements Transformable
{
    use TransformableTrait, Searchable, GroupedEntities;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'number',
      'analysis',
      'canonical',
      'extended',
      'crossref_str',
      'crossref_md5',
      'summary',
    ];

    protected $indexConfigurator = ChapterIndexConfigurator::class;

    protected $searchRules = [
        //
    ];

    // Here you can specify a mapping for a model fields.
    protected $mapping = [
        'properties' => [
          'book_num' => [
              'type' => 'integer',
          ],
          'number' => [
              'type' => 'integer',
          ],
          'book_name' => [
              'type' => 'text',
              "fields" => [
                "analyzed" => [
                  "type" => "text",
                  "analyzer" => "partial_match_analyzer",
                ],
              ],
          ],
          'canonical' => [
              'type' => 'keyword',
              "fields" => [
                "analyzed" => [
                  "type" => "text",
                  "analyzer" => "partial_match_analyzer",
                ],
              ],
          ],
          'extended' => [
              'type' => 'keyword',
              "fields" => [
                "analyzed" => [
                  "type" => "text",
                  "analyzer" => "partial_match_analyzer",
                ],
              ],
          ],
          'crossref_str' => [
              'type' => 'keyword',
              "fields" => [
                "analyzed" => [
                  "type" => "text",
                  "analyzer" => "partial_match_analyzer",
                ],
              ],
          ],
          'content' => [
              'type' => 'keyword',
              "fields" => [
                "analyzed" => [
                  "type" => "text",
                  "analyzer" => "partial_match_analyzer",
                ],
              ],
          ],
          'summary' => [
              'type' => 'keyword',
              "fields" => [
                "analyzed" => [
                  "type" => "text",
                  "analyzer" => "partial_match_analyzer",
                ],
              ],
          ],
          'words'         => [
              'type' => 'text',
              "fields" => [
                "analyzed" => [
                  "type" => "text",
                  "analyzer" => "partial_match_analyzer",
                ],
              ],
          ],
          'entities'      => [
              'type' => 'text',
              "fields" => [
                "analyzed" => [
                  "type" => "text",
                  "analyzer" => "partial_match_analyzer",
                ],
              ],
          ],
        ]
    ];

    public function toSearchableArray () : array {
      $this->load (['book', 'frequencies']);
      return [
        'number'        => intval ($this->number),
        'canonical'     => $this->canonical,
        'extended'      => $this->extended,
        'crossref_str'  => $this->crossref_str,
        'book_name'     => $this->book->title,
        'book_num'      => $this->book->order,
        'content'       => $this->text['en'],
        'summary'       => $this->summary,
        'words'         => $this->frequencies->pluck('word')->sort()->implode (', '),
        'entities'      => collect($this->analysis)->pluck ('name')->sort()->implode (', '),
      ];
    }

    public function analyses () {
      return $this->hasMany ( Analysis::class, 'analyzable_id' )->where ('analyzable_type', get_class ($this));
    }

    public function book () {
      return $this->belongsTo ( Book::class );
    }

    public function entity_refs () {
      return $this->hasMany ( EntityRef::class, 'reference_id' )->where ('reference_type', get_class ($this));
    }

    public function frequencies () {
      return $this->hasMany ( Frequency::class, 'container_id' )->where ('container_type', get_class ($this));
    }

    public function summaries () {
      return $this->hasMany ( Summary::class, 'container_id' );
    }

    public function verses () {
      return $this->hasMany ( Verse::class );
    }

    public function getNextAttribute () {
      if ( $this->number == $this->book->chaps ) {
        return false;
      }
      return $this->number+1;
    }

    public function getPreviousAttribute () {
      if ( $this->number == 1 ) {
        return false;
      }
      return $this->number-1;
    }

    public function getAverageVerseLengthAttribute () {
      return $this->verses->map(function($item, $key) {
          return strlen ($item->text['en']);
      })->avg ();
    }

    public function getLongestVerseAttribute () {
      return $this->verses->sortByDesc(function($item, $key) {
          return strlen ($item->text['en']);
      })->first ();
    }

    public function getShortestVerseAttribute () {
      return $this->verses->sort(function($item, $key) {
          return strlen ($item->text['en']);
      })->first ();
    }

    public function getFullTextAttribute () {
      return $this->verses->pluck ('text.en')->implode (' ');
    }

    public function getFullTextParagraphHTMLAttribute () {
      return $this->verses->pluck ('text.en')->implode ('</p><p>');
    }

}
