<?php

namespace BibleNLP\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ReferenceRepository.
 *
 * @package namespace BibleNLP\Contracts\Repositories;
 */
interface CrossRefRepositoryContract extends RepositoryInterface
{
    //
}
