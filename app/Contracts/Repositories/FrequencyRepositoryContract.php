<?php

namespace BibleNLP\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface FrequencyRepository.
 *
 * @package namespace BibleNLP\Contracts\Repositories;
 */
interface FrequencyRepositoryContract extends RepositoryInterface
{
    //
}
