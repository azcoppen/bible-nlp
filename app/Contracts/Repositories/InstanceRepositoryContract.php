<?php

namespace BibleNLP\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface LexiconRepository.
 *
 * @package namespace BibleNLP\Contracts\Repositories;
 */
interface InstanceRepositoryContract extends RepositoryInterface
{
    //
}
