<?php

namespace BibleNLP\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface EntityRepository.
 *
 * @package namespace BibleNLP\Contracts\Repositories;
 */
interface EntityRepositoryContract extends RepositoryInterface
{
    //
}
