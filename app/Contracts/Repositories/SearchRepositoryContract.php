<?php

namespace BibleNLP\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface TopicRepository.
 *
 * @package namespace BibleNLP\Contracts\Repositories;
 */
interface SearchRepositoryContract extends RepositoryInterface
{
    //
}
