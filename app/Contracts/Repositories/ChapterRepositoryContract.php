<?php

namespace BibleNLP\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ChapterRepository.
 *
 * @package namespace BibleNLP\Contracts\Repositories;
 */
interface ChapterRepositoryContract extends RepositoryInterface
{
    //
}
