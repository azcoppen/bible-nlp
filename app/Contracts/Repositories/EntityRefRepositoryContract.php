<?php

namespace BibleNLP\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface EntityRefRepository.
 *
 * @package namespace BibleNLP\Contracts\Repositories;
 */
interface EntityRefRepositoryContract extends RepositoryInterface
{
    //
}
