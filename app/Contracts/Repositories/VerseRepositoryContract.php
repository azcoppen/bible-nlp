<?php

namespace BibleNLP\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface VerseRepository.
 *
 * @package namespace BibleNLP\Contracts\Repositories;
 */
interface VerseRepositoryContract extends RepositoryInterface
{
    //
}
