<?php

namespace BibleNLP\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AnalysisRepository.
 *
 * @package namespace BibleNLP\Contracts\Repositories;
 */
interface AnalysisRepositoryContract extends RepositoryInterface
{
    //
}
