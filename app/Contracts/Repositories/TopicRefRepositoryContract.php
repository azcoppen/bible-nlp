<?php

namespace BibleNLP\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface TopicRefRepository.
 *
 * @package namespace BibleNLP\Contracts\Repositories;
 */
interface TopicRefRepositoryContract extends RepositoryInterface
{
    //
}
