<?php

namespace BibleNLP\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BookRepository.
 *
 * @package namespace BibleNLP\Contracts\Repositories;
 */
interface BookRepositoryContract extends RepositoryInterface
{
    //
}
