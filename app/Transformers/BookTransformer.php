<?php

namespace BibleNLP\Transformers;

use League\Fractal\TransformerAbstract;
use BibleNLP\Models\Book;

/**
 * Class BookTransformer.
 *
 * @package namespace BibleNLP\Transformers;
 */
class BookTransformer extends TransformerAbstract
{
    /**
     * Transform the Book entity.
     *
     * @param \BibleNLP\Models\Book $model
     *
     * @return array
     */
    public function transform(Book $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
