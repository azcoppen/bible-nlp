<?php

namespace BibleNLP\Transformers;

use League\Fractal\TransformerAbstract;
use BibleNLP\Models\Verse;

/**
 * Class VerseTransformer.
 *
 * @package namespace BibleNLP\Transformers;
 */
class VerseTransformer extends TransformerAbstract
{
    /**
     * Transform the Verse entity.
     *
     * @param \BibleNLP\Models\Verse $model
     *
     * @return array
     */
    public function transform(Verse $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
