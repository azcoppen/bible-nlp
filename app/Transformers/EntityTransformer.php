<?php

namespace BibleNLP\Transformers;

use League\Fractal\TransformerAbstract;
use BibleNLP\Models\Entity;

/**
 * Class EntityTransformer.
 *
 * @package namespace BibleNLP\Transformers;
 */
class EntityTransformer extends TransformerAbstract
{
    /**
     * Transform the Entity entity.
     *
     * @param \BibleNLP\Models\Entity $model
     *
     * @return array
     */
    public function transform(Entity $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
